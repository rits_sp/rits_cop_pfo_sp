﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RITS.COP.PFO.BL.BusinessEntities
{
    public class BottomLink
    {
        #region Props
        public string Key { get; set; }
        public string Menu { get; set; }
        public string Value { get; set; }
        public string Link { get; set; }
        public string Visible { get; set; }
        public string Enable { get; set; }
        #endregion

        #region Const
        public BottomLink()
        {
        }

        public BottomLink(string key, string menu, string value, string link, string visible, string enable)
        {
            Key = key;
            Menu = menu;
            Value = value;
            Link = link;
            Visible = visible;
            Enable = enable;
        }
        #endregion
    }
}
