﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;

namespace RITS.COP.PFO.BR.Utils
{
 
    class COP_BrandingManager
    {
         public SPWeb Web { get; set; }

         public COP_BrandingManager(SPWeb web)
        {
            Web = web;
        }

        public void SetMasterPage(string masterPageUrl, string logoUrl)
        {
            masterPageUrl = GetMasterPageServerRelativeUrl(masterPageUrl);
            Web.MasterUrl = masterPageUrl;
            Web.CustomMasterUrl = masterPageUrl;
            Web.SiteLogoUrl = logoUrl;
            Web.Update();
        }

        private string GetMasterPageServerRelativeUrl(string masterPageUrl)
        {
            var t = masterPageUrl.StartsWith("/") ? masterPageUrl : SPUrlUtility.CombineUrl(Web.ServerRelativeUrl, masterPageUrl);
            return t;
        }

        public void InstallThemes(IEnumerable<string> themeNames, string masterUrl)
        {
            masterUrl = GetMasterPageServerRelativeUrl(masterUrl);
            var designCatalog = Web.GetCatalog(SPListTemplateType.DesignCatalog);

            var displayOrderCounter = 1000;

            foreach (var themeName in themeNames)
            {

                var themeTitle = themeName.Replace(".spcolor", "");
                var themeItem = designCatalog.AddItem();
                themeItem["Name"] = themeTitle;
                themeItem["Title"] = themeTitle;
                themeItem["MasterPageUrl"] = new SPFieldUrlValue(masterUrl); //the master page should have a 'preview' file associated
                themeItem["ThemeUrl"] = new SPFieldUrlValue(Web.Site.ServerRelativeUrl.TrimEnd('/') + "/_catalogs/theme/15/" + themeName);
                themeItem["DisplayOrder"] = displayOrderCounter;
                themeItem.Update();
                displayOrderCounter += 10;
            }
       
        }

        public void InstallThemes(string themeName, string themeColor, string themeFont, string imageUrl, string masterUrl)
        {
           
            masterUrl = GetMasterPageServerRelativeUrl(masterUrl);
  
            var designCatalog = Web.GetCatalog(SPListTemplateType.DesignCatalog);

            if (designCatalog.Items[designCatalog.Items.Count - 1].DisplayName != themeName)
            {
                //var themeTitle = themeColor.Replace(".spcolor", "");
                var themeItem = designCatalog.AddItem();
                themeItem["Name"] = themeName;
                themeItem["Title"] = themeName;
                themeItem["MasterPageUrl"] = new SPFieldUrlValue(masterUrl); //the master page should have a 'preview' file associated
                themeItem["ThemeUrl"] = "/_catalogs/theme/15/" + themeColor;
                themeItem["FontSchemeUrl"] = "/_catalogs/theme/15/" + themeFont;
                themeItem["ImageUrl"] = imageUrl;
                themeItem["DisplayOrder"] = 1000;
                themeItem.Update();
            }
        }

        public void ApplyTheme(SPTheme theme)
        {
            theme.ApplyTo(Web, false);
            Web.Update();
            SetCurrentTheme(theme.Name);
        }

        private void SetCurrentTheme(string name)
        {
            var designCatalog = Web.GetCatalog(SPListTemplateType.DesignCatalog);
            name = name.Replace(".spcolor", "");

            //get Current list item
            var query = new SPQuery
            {
                Query = "<Where><Eq><FieldRef Name='Name'/><Value Type='Text'>Current</Value></Eq></Where>",
                RowLimit = 1
            };
            var items = designCatalog.GetItems(query);

            if (items != null && items.Count == 1)
            {
                //get newly apply theme item
                var query2 = new SPQuery
                {
                    Query =
                        string.Format("<Where><Eq><FieldRef Name='Name'/><Value Type='Text'>{0}</Value></Eq></Where>",
                            name),
                    RowLimit = 1
                };
                var items2 = designCatalog.GetItems(query2);

                if (items2 != null && items2.Count == 1)
                {
                    items[0].Delete();

                    var newCurrentItem = designCatalog.AddItem();
                    newCurrentItem["Title"] = SPResource.GetString(CultureInfo.CurrentUICulture, Strings.DesignGalleryCurrentItemName);
                    newCurrentItem["Name"] = SPResource.GetString(CultureInfo.CurrentUICulture, Strings.DesignGalleryCurrentItemName);
                    newCurrentItem["MasterPageUrl"] = items2[0]["MasterPageUrl"];
                    newCurrentItem["ThemeUrl"] = items2[0]["ThemeUrl"];
                    newCurrentItem["ImageUrl"] = items2[0]["ImageUrl"];
                    newCurrentItem["FontSchemeUrl"] = items2[0]["FontSchemeUrl"];
                    newCurrentItem["DisplayOrder"] = 0;
                    newCurrentItem.Update();
                }
            }
        }

        public void UnInstallThemes(List<string> themes)
        {
            var designCatalog = Web.GetCatalog(SPListTemplateType.DesignCatalog);

            foreach (var theme in themes)
            {
                var themeName = theme.Replace(".spcolor", "");
                var query = new SPQuery();
                query.Query = string.Format("<Where><Eq><FieldRef Name='Name'/><Value Type='Text'>{0}</Value></Eq></Where>", themeName);

                var items = designCatalog.GetItems(query);
                var itemsToDelete = (from SPListItem item in items select item.ID).ToList();

                foreach (var itemId in itemsToDelete)
                {
                    designCatalog.GetItemById(itemId).Delete();
                }
            }
        }

        public void DeleteMasterPage(string masterUrl)
        {
            masterUrl = GetMasterPageServerRelativeUrl(masterUrl);

            var customMasterpage = Web.GetFile(masterUrl);
            if (customMasterpage.Exists)
                customMasterpage.Delete();
        }
    }
}