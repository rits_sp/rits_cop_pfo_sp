//detect mobile device
var isMobile = false; //initiate as false
// device detection
if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0, 4))) isMobile = true;
//

//mobile orientation change reload
window.onorientationchange = function () { window.location.reload(); };
//

// block enter key
$(window).keydown(function (event) {
    if ((event.keyCode == 13) && (event.target.nodeName != 'TEXTAREA') && (event.target.id != 'content_text')) {
        event.preventDefault();
        return false;
    }
});
//

//change main slider position on responsive
function changeMainSlider() {

    if ((window.innerHeight < 704) && (window.innerHeight < window.innerWidth)) {
        $("#header-slideimage-holder").css('height', (window.innerHeight - 205));
        //setTimeout(function() {
        //    $('li .tp-parallax-wrap').each(function() {
        //        $(this).css('top', ($('.tp-parallax-wrap').position().top) - 50);
        //    });
        //}, 200);
    } else {
        $("#header-slideimage-holder").css('height', 500);
    }
    if (($(window).width() < 1024) || (window.innerHeight > window.innerWidth) && (navigator.userAgent.match(/(iPad)/))) {
        $('.slider-container').insertAfter('#nav');
    }
    else {
        $('.slider-container').appendTo('#header-slideimage-holder');
    }
}

//form add content / activities check validation
function valueChanged() {
    if ($('#checkbox-images').is(':checked')) {
        $('#form-images').slideDown('slow');
        //$('#form-images input').each(function(){
        //  $(this).attr('required', 'true');
        //});
    }
    else {
        $('#form-images').slideUp('slow');
        //$('#form-images input').each(function(){
        //  $(this).removeAttr('required');
        //});
    }
    if ($('#checkbox-videos').is(':checked')) {
        $('#form-videos').slideDown('slow');
        //$('#form-videos input').each(function(){
        //  $(this).attr('required', 'true');
        //});
    }
    else {
        $('#form-videos').slideUp('slow');
        //$('#form-videos input').each(function(){
        //  $(this).removeAttr('required');
        //});
    }
    if ($('#checkbox-hangouts').is(':checked')) {
        $('#form-hangouts').slideDown('slow');
        //$('#form-hangouts input').each(function(){
        //  $(this).attr('required', 'true');
        //});
    }
    else {
        $('#form-hangouts').slideUp('slow');
        //$('#form-hangouts input').each(function(){
        //  $(this).removeAttr('required');
        //});
    }
}
//

//vertical text overflow ellispis
(function ($) {
    $.fn.ellipsis = function () {
        return this.each(function () {
            var el = $(this);

            if (el.css("overflow") == "hidden") {
                var text = el.html();
                var multiline = el.hasClass('multiline');
                var t = $(this.cloneNode(true))
					.hide()
					.css('position', 'absolute')
					.css('overflow', 'visible')
					.width(multiline ? el.width() : 'auto')
					.height(multiline ? 'auto' : el.height())
                ;

                el.after(t);

                function height() { return t.height() > el.height(); };
                function width() { return t.width() > el.width(); };

                var func = multiline ? height : width;

                while (text.length > 0 && func()) {
                    text = text.substr(0, text.length - 1);
                    t.html(text + "...");
                }

                el.html(t.html());
                t.remove();
            }
        });
    };
})(jQuery);
//

$(document).ready(function () {

    $('.navbar-topheader').data('size', 'big');
    changeMainSlider();
    if (!$.trim($('#header-slideimage-holder').html()).length) {
        $('#header-slideimage-holder').remove();
    }
    if (isMobile) {
        $('body').addClass('mobile');
        if (window.innerHeight > window.innerWidth) {
            $('body').addClass('portrait');
        } else {
            $('body').addClass('landscape');
        }
        if ((navigator.userAgent.match(/(iPad)/))) {
            $('body').addClass('ipad');
        }
    }
    //replace SP h1 to other
    $('#pageTitle').replaceWith(function () {
        return '<h5>' + $(this).html() + '</h5>';
    });
    //

    //animated call to action icons when scroll to
    var wowanim = 0;
    wow = new WOW({
        callback: function (box) {
            // the callback is fired every time an animation is started
            // the argument that is passed in is the DOM node being animated
            wowanim = 1;
        }
    });
    wow.init();

    //end of animated call to action icons when scroll to

    //scroll animation and header fixing
    var distance = ($('#navbar-collapse-1').offset().top) + 50,
    $window = $(window);

    function scrollCheck() {
        if (($window.scrollTop() >= distance) && ($(window).width() > 768) && (window.innerHeight < window.innerWidth)) {
            if ($('.navbar-topheader').data('size') == 'big') {
                //$('#main-slider').hide();
                $('#menu-collapse, .navbar-topheader-bar').addClass('fixed');
                //$('body').attr('style', 'padding-top:' + ($("#main-slider").height()+$(".head-description").height()) + 'px;');
                $('.navbar-brand img.brand-smaller').show('slow');
                $('.navbar-brand img.brand-normal').hide('normal');
                $('.navbar-brand').addClass('navbar-brand-smaller');
                //$('.slider-container').hide();
                /*$('.navbar-topheader').stop().animate({
                    height:'0px',
                    opacity:'0'
                },600);*/
                //$('.navbar-topheader').hide();
                $("#rev_slider_home").revpause();
                $('.navbar-topheader').data('size', 'small');
            }
        }
        else {
            if (($('.navbar-topheader').data('size') == 'small')) {
                //$('body').removeAttr('style');
                $('.navbar-brand img.brand-smaller').hide('normal');
                $('.navbar-brand img.brand-normal').show('slow');
                $('.navbar-brand').removeClass('navbar-brand-smaller');
                //$('.navbar-topheader').show();
                /*$('.navbar-topheader').stop().animate({
                    height:'123px',
                    opacity:'1'
                      },100);*/
                //$('#main-slider').show();
                $('#menu-collapse, .navbar-topheader-bar').removeClass('fixed');
                $("#rev_slider_home").revresume();
                //$('.slider-container').show();
                $('.navbar-topheader').data('size', 'big');
            }
        }
    }

    $(window).scroll(function () {
        scrollCheck();
    });

    //on window resize check the slider position on the responsive adjustment
    $(window).on('resize', function () {
        if ($(window).width() > 850) {
            $('.navbar-topheader').data('size', 'small');
            changeMainSlider();
            scrollCheck();
        }
        else {
            $('.navbar-topheader').data('size', 'big');
            changeMainSlider();
            scrollCheck();
        }
    });



    $.cookieBar({
        message: 'Cookies no site PEO - Programa de Educação Olímpica<br>Utilizamos cookies para uma melhor experiência de utilizador. Ao navegar no site, estará a consentir a sua utilização.',
        acceptButton: true,
        acceptText: 'Eu percebo. Fechar informação.',
        acceptFunction: null,
        declineButton: false,
        declineText: 'Disable Cookies',
        declineFunction: null,
        policyButton: true,
        policyText: 'Política de uso de Cookies',
        policyURL: '/Pages/PoliticaCookies.aspx',
        autoEnable: true,
        acceptOnContinue: false,
        acceptOnScroll: false,
        acceptAnyClick: false,
        expireDays: 365,
        renewOnVisit: false,
        forceShow: false,
        effect: 'slide',
        element: 'body',
        append: false,
        fixed: false,
        bottom: false
        //,zindex: '',
        //domain: 'www.example.com',
        //referrer: 'www.example.com'
    });

    $('.image-thumbnail').iLightBox(
      { path: 'horizontal' }
    );
    $('.ilightbox.video').iLightBox(
      { path: 'horizontal' }
    );

    $.fn.is_on_screen = function () {
        var win = $(window);
        var viewport = {
            top: win.scrollTop(),
            left: win.scrollLeft()
        };
        viewport.right = viewport.left + win.width();
        viewport.bottom = viewport.top + win.height();

        var bounds = this.offset();
        bounds.right = bounds.left + this.outerWidth();
        bounds.bottom = bounds.top + this.outerHeight();

        return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));
    };

    if ($('.calltoaction').length > 0) { // if target element exists in DOM
        if ($('.calltoaction').is_on_screen()) { // if target element is visible on screen after DOM loaded
            $('.log').html('<div class="alert alert-success">target element is visible on screen</div>'); // log info   
        } else {
            $('.log').html('<div class="alert">target element is not visible on screen</div>'); // log info
        }
    }
    $(window).scroll(function () { // bind window scroll event
        if ($('.calltoaction').length > 0) { // if target element exists in DOM
            if ($('.calltoaction').is_on_screen()) { // if target element is visible on screen after DOM loaded
                //$('.zmdi').slideDown('normal');
                $('.calltoaction .zmdi').addClass('animated bounceOutRight');
            } else {
                $('.calltoaction .zmdi').removeClass('animated bounceOutRight');
            }
        }
    });
    //end of animation





    //
    //social share
    if ($('#share').length) {
        $('#share').jsSocials({
            //url: "http://google.com",
            //text: "Google Search Page",
            showLabel: false,
            showCount: "inside",
            shares: ["twitter", "facebook", "googleplus", "linkedin"]
        });
        $('#share').prepend('<div class="reghere">Partilha aqui</div>');
    }
    //end of social share


    //header notifications if empty hide badge
    if (!$('.notificationsicon .material-icons.mdl-badge').attr('data-badge')) {
        $('#configuration .material-icons.mdl-badge').addClass('emptybadge');
    }
    //

    //email obfuscate
    /*document.write("<n uers=\"znvygb:xvpx@vaprcgvba.pbz\" ery=\"absbyybj\">Fraq n zrffntr</n>".replace(/[a-zA-Z]/g, 
    function(c){return String.fromCharCode((c<="Z"?90:122)>=(c=c.charCodeAt(0)+13)?c:c-26);}));*/
    //end of email obfuscate


    // Login
    /**
               * check if the input has any value (if we've typed into it)
               */

    /**
     * Form validation
     */
    //$('.form').validate({
    //    rules: {
    //        password: {
    //            minlength: 5
    //        }
    //    }
    //});

    /**
     * Form2 validation
     */
    $('.form2').validate();


    /**
     * Simple Modal
     */
    $('.modal__toggle').on('click', function (e) {
        e.preventDefault();
        //$('.modal').toggleClass('modal--open');
        $('.modal-opened').slideToggle();

    });

    //end of Login
    //method of file format validation
    //$.validator.addMethod(
    //    "type_file",
    //    function (value, element) {
    //        var check = true;
    //        var allowedFiles = [".doc", ".docx", ".pdf"];
    //        var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");

    //        if (value == "")
    //            /* campo obrigatorio */
    //            check = false;
    //        else
    //            if (regex.test(value) == false)
    //                check = false;
    //        return check;
    //    },


    //method for youtube URLs
    $.validator.addMethod(
        "youtubeurl",
        //return this.optional(element) || /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/.test(value);
        function (value, element) {
            var check = true;
            var re = /^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/;
            if (value == "")
                /* campo obrigatorio */
                check = false;
            else
                if (re.test(value) == false)
                    check = false;
            return check;
        },
    "Por favor, insira um URL válido de Youtube.");

    //

    //method for checkboxes
    $.validator.addMethod("roles", function (value, elem, param) {
        if ($(".roles:checkbox:checked").length > 0) {
            return true;
        } else {
            return false;
        }
    }, "Tem de escolher pelo menos uma opção.");
    //

    //method of phone number validation
    $.validator.addMethod(
          "type_telefone",
          function (value, element) {
              var check = true;
              /* validar numeros moveis (91x, 92x, 93x, 96x) e fixos (2x) */
              var re = /^(9[1236]\d{7}|2\d{8})$/;

              if (value == "")
                  /* campo obrigatorio */
                  check = false;
              else
                  if (re.test(value) == false)
                      check = false;
              return check;
          },
          "Número invalido");

    //method for zip code validation
    $.validator.addMethod("zipcode", function (value, element) {
        return this.optional(element) || !!value.trim().match(/^\d{4}(-\d{3})?$/);
    }, "Código inválido");
    //

    //method for password pattern
    jQuery.validator.addMethod("type_passw", function (value, element) {
        return this.optional(element) || /((?=.*[a-zA-Z])(?=.*\W).{8,16})/.test(value);
    }, "A sua password deverá ter entre 8 a 16 caracteres, constituída por, pelo menos, um caracter especial.");
    //

    //method for latitude and longitude pattern
    jQuery.validator.addMethod("type_latitude", function (value, element) {
        return this.optional(element) || /^[-+]?([1-8]?\d(\.\d+)?|90(\.0+)?)/.test(value);
    }, "Formato inválido.");
    //
    jQuery.validator.addMethod("type_longitude", function (value, element) {
        return this.optional(element) || /\s*[-+]?(180(\.0+)?|((1[0-7]\d)|([1-9]?\d))(\.\d+)?)$/.test(value);
    }, "Formato inválido.");
    //

    /**
   * Translated default messages for the jQuery validation plugin.
   * Locale: PT_PT
   */
    $.extend($.validator.messages, {
        required: "Campo de preenchimento obrigat&oacute;rio.",
        remote: "Por favor, corrija este campo.",
        email: "Por favor, introduza um endere&ccedil;o eletr&oacute;nico v&aacute;lido.",
        url: "Por favor, introduza um URL v&aacute;lido.",
        date: "Por favor, introduza uma data v&aacute;lida.",
        dateISO: "Por favor, introduza uma data v&aacute;lida (ISO).",
        number: "Por favor, introduza um n&uacute;mero v&aacute;lido.",
        digits: "Por favor, introduza apenas d&iacute;gitos.",
        creditcard: "Por favor, introduza um n&uacute;mero de cart&atilde;o de cr&eacute;dito v&aacute;lido.",
        equalTo: "Por favor, introduza de novo o mesmo valor.",
        extension: "Por favor, introduza um ficheiro com uma extens&atilde;o v&aacute;lida.",
        maxlength: $.validator.format("Por favor, n&atilde;o introduza mais do que {0} caracteres."),
        minlength: $.validator.format("Por favor, introduza pelo menos {0} caracteres."),
        rangelength: $.validator.format("Por favor, introduza entre {0} e {1} caracteres."),
        range: $.validator.format("Por favor, introduza um valor entre {0} e {1}."),
        max: $.validator.format("Por favor, introduza um valor menor ou igual a {0}."),
        min: $.validator.format("Por favor, introduza um valor maior ou igual a {0}."),
        nifES: "Por favor, introduza um NIF v&aacute;lido.",
        nieES: "Por favor, introduza um NIE v&aacute;lido.",
        cifES: "Por favor, introduza um CIF v&aacute;lido."
    });


    //navbar responsive fix for scrolling
    $(".navbar-collapse").css({ maxHeight: $(window).height() - $(".navbar-header").height() + "px" });
    //
    //change login info if mobile portrait
    if ((isMobile) && $('body').hasClass('portrait')) {
        //$('.navbar-nav .responsive-item.login a').hide();
        $('#logged').appendTo('.navbar-nav .responsive-item.login');
        $('ul.navbar-nav').append('<li class="responsive-item logout"></li>');
        $('#ctl00_UC_header_UC_headerLoginArea_btn_Logout').appendTo('.navbar-nav .responsive-item.logout');
    }
    //


    //if ipad landscape change bootstrap lg column to md
    if ((isMobile) && ($('body').hasClass('landscape'))) {
        $('footer .col-lg-8').removeClass('col-lg-8').addClass('col-lg-12');
        $('.col-lg-8').removeClass('col-lg-8').addClass('col-md-8');
        $('.col-lg-4').removeClass('col-lg-4').addClass('col-md-4');
    }
    //

    //if mobile change configiration location
    if ((isMobile) && $('body').hasClass('landscape') && window.innerWidth > 1024) {
        $('#configuration').prependTo('.navbar-topheader');
    }
    if ((isMobile) && $('body').hasClass('portrait') || (isMobile) && ($('body').hasClass('landscape')) && (window.innerWidth < 1024)) {
        $('#configuration').prependTo('.navbar-header .container');
    }
    //


    //if mobile change body to be at least device height
    if ((isMobile) && $('body').hasClass('portrait')) {
        $('#contentBox').attr('style', 'min-height:' + (($(window).height() - $('#rev_slider_inside').height() - $('.navbar-header').height() - $('footer').height()) - 160) + 'px;');
        if ($('.calltoaction').length) {
            $('.calltoaction').insertBefore('footer');
        }
    }
    //
    //rev slider HOME
    if ($('#rev_slider_home').length) {
        if (isMobile) {
            if ((window.innerHeight < window.innerWidth) && (navigator.userAgent.match(/(iPad)/))) {
                var sliderMainHeight = ($("#header-slideimage-holder").height());
            }
            else {
                var sliderMainHeight = $(window).height();
            }
            //var sliderGridHeight = $(window).height();
        }
        else {
            var sliderMainHeight = ($("#header-slideimage-holder").height());
            //var sliderGridHeight = ($("#header-slideimage-holder").height())-50;
        }
        $("#rev_slider_home").revolution({
            sliderType: "standard",
            //jsFileLocation: "../../../../Style%20Library/cop/js/revolution/",
            lazyType: "smart",
            sliderLayout: "fullwidth",
            delay: 9000,
            navigation: {
                arrows: { enable: true }
            },
            minHeight: sliderMainHeight,
            spinner: "false",
            fallbacks: {
                panZoomDisableOnMobile: "on",
                simplifyAll: "on"
            },
            parallax: {
                type: "on",
                levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85],
                origo: "enterpoint",
                speed: 400,
                bgparallax: "on",
                disable_onmobile: "on"
            },
            gridwidth: 1230,
            gridheight: (sliderMainHeight - 10)
        });
    }
    //end of rev slider

    //rev slider for pages with one slide
    if ($('#rev_slider_inside').length) {
        if (isMobile) {
            if ((window.innerHeight < window.innerWidth) && (navigator.userAgent.match(/(iPad)/))) {
                var sliderMainHeight = ($("#header-slideimage-holder").height());
            }
            else {
                var sliderMainHeight = $(window).height();
            }
            //var sliderGridHeight = $(window).height();
        }
        else {
            var sliderMainHeight = ($("#header-slideimage-holder").height());
            //var sliderGridHeight = ($("#header-slideimage-holder").height())-50;
        }
        $("#rev_slider_inside").revolution({
            sliderType: "standard",
            //jsFileLocation: "../../../../Style%20Library/cop/js/revolution/",
            lazyType: "smart",
            sliderLayout: "fullwidth",
            delay: 9000,
            navigation: {
                arrows: { enable: false }
            },
            minHeight: sliderMainHeight,
            spinner: "false",
            fallbacks: {
                panZoomDisableOnMobile: "on",
                simplifyAll: "on"
            },
            parallax: {
                type: "on",
                levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85],
                origo: "enterpoint",
                speed: 400,
                bgparallax: "on",
                disable_onmobile: "on"
            },
            gridwidth: 1230,
            gridheight: (sliderMainHeight - 10)
        });
    }
    //end of rev slider

    //rev Slider HOME Hangouts
    if ($('#rev_slider_home_hangouts').length) {
        $("#rev_slider_home_hangouts").show().revolution({
            sliderType: "standard",
            //jsFileLocation:"../../revolution/js/",
            sliderLayout: "auto",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                arrows: {
                    style: "uranus",
                    enable: true,
                    hide_onmobile: true,
                    hide_under: 778,
                    hide_onleave: true,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    tmp: '',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 20,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 20,
                        v_offset: 0
                    }
                }
              ,
                thumbnails: {
                    style: "erinyen",
                    enable: true,
                    width: 200,
                    height: 113,
                    min_width: 170,
                    wrapper_padding: 30,
                    wrapper_color: "#333333",
                    wrapper_opacity: "1",
                    tmp: '<span class="tp-thumb-over"></span><span class="tp-thumb-image"></span><span class="tp-thumb-title">{{title}}</span><span class="tp-thumb-more"></span>',
                    visibleAmount: 10,
                    hide_onmobile: false,
                    hide_onleave: false,
                    direction: "horizontal",
                    span: true,
                    position: "outer-bottom",
                    space: 20,
                    h_align: "center",
                    v_align: "bottom",
                    h_offset: 0,
                    v_offset: 0
                }
            },
            gridwidth: 1230,
            gridheight: 692,
            lazyType: "none",
            shadow: 0,
            spinner: "spinner2",
            stopLoop: "on",
            stopAfterLoops: 0,
            stopAtSlide: 1,
            shuffle: "off",
            autoHeight: "off",
            disableProgressBar: "on",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });
    }
    //

    //rev Slider HOME Activities
    if ($('#rev_slider_home_activities').length) {
        $("#rev_slider_home_activities").show().revolution({
            sliderType: "standard",
            //jsFileLocation:"../../revolution/js/",
            sliderLayout: "auto",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "on",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "on",
                touch: {
                    touchenabled: "on",
                    swipe_threshold: 75,
                    swipe_min_touches: 1,
                    swipe_direction: "horizontal",
                    drag_block_vertical: false
                }
              ,
                arrows: {
                    style: "gyges",
                    enable: true,
                    hide_onmobile: false,
                    hide_over: 778,
                    hide_onleave: false,
                    tmp: '',
                    left: {
                        h_align: "right",
                        v_align: "bottom",
                        h_offset: 40,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "bottom",
                        h_offset: 0,
                        v_offset: 0
                    }
                }
              ,
                tabs: {
                    style: "erinyen",
                    enable: true,
                    width: 250,
                    height: 100,
                    min_width: 250,
                    wrapper_padding: 0,
                    wrapper_color: "transparent",
                    wrapper_opacity: "0",
                    tmp: '<div class="tp-tab-title">{{title}}</div><div class="tp-tab-desc">{{description}}</div>',
                    visibleAmount: 3,
                    hide_onmobile: true,
                    hide_under: 778,
                    hide_onleave: false,
                    hide_delay: 200,
                    direction: "vertical",
                    span: false,
                    position: "inner",
                    space: 10,
                    h_align: "right",
                    v_align: "center",
                    h_offset: 30,
                    v_offset: 0
                }
            },
            viewPort: {
                enable: true,
                outof: "pause",
                visible_area: "80%"
            },
            responsiveLevels: [1240, 1024, 778, 480],
            gridwidth: [1240, 1024, 778, 480],
            gridheight: [500, 450, 400, 350],
            lazyType: "none",
            parallax: {
                type: "scroll",
                origo: "enterpoint",
                speed: 400,
                levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50],
            },
            shadow: 0,
            spinner: "off",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });
    }
    //
    //rev Slider ABOUT
    if ($('#rev_slider_about').length) {
        $("#rev_slider_about").show().revolution({
            sliderType: "carousel",
            lazyType: "smart",
            //jsFileLocation:"../../revolution/js/",
            sliderLayout: "fullwidth",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                arrows: {
                    style: "erinyen",
                    enable: true,
                    hide_onmobile: true,
                    hide_under: 600,
                    hide_onleave: true,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div>    <div class="tp-arr-img-over"></div> <span class="tp-arr-titleholder">{{title}}</span> </div>',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    }
                }
              ,
                thumbnails: {
                    style: "gyges",
                    enable: true,
                    width: 60,
                    height: 60,
                    min_width: 60,
                    wrapper_padding: 0,
                    wrapper_color: "transparent",
                    wrapper_opacity: "1",
                    tmp: '<span class="tp-thumb-img-wrap">  <span class="tp-thumb-image"></span></span>',
                    visibleAmount: 5,
                    hide_onmobile: true,
                    hide_under: 800,
                    hide_onleave: false,
                    direction: "horizontal",
                    span: false,
                    position: "inner",
                    space: 5,
                    h_align: "center",
                    v_align: "bottom",
                    h_offset: 0,
                    v_offset: 20
                }
            },
            carousel: {
                horizontal_align: "center",
                vertical_align: "center",
                fadeout: "off",
                maxVisibleItems: 3,
                infinity: "on",
                space: 0,
                stretch: "off"
            },
            viewPort: {
                enable: true,
                outof: "pause",
                visible_area: "80%"
            },
            responsiveLevels: [1240, 1024, 778, 480],
            gridwidth: [1240, 1024, 778, 480],
            gridheight: [600, 600, 500, 400],
            lazyType: "none",
            parallax: {
                type: "mouse",
                origo: "slidercenter",
                speed: 2000,
                levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
            },
            shadow: 5,
            spinner: "off",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            hideThumbsOnMobile: "on",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });
    }
    //

    //rev Slider ABOUT
    if ($('#rev_slider_news').length) {
        $("#rev_slider_news").show().revolution({
            sliderType: "carousel",
            //jsFileLocation:"../../revolution/js/",
            sliderLayout: "fullwidth",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                arrows: {
                    style: "erinyen",
                    enable: true,
                    hide_onmobile: true,
                    hide_under: 600,
                    hide_onleave: true,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    //tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div>    <div class="tp-arr-img-over"></div> <span class="tp-arr-titleholder">{{title}}</span> </div>',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    }
                }
              ,
                thumbnails: {
                    style: "gyges",
                    enable: true,
                    width: 60,
                    height: 60,
                    min_width: 60,
                    wrapper_padding: 0,
                    wrapper_color: "transparent",
                    wrapper_opacity: "1",
                    tmp: '<span class="tp-thumb-img-wrap">  <span class="tp-thumb-image"></span></span>',
                    visibleAmount: 5,
                    hide_onmobile: true,
                    hide_under: 800,
                    hide_onleave: false,
                    direction: "horizontal",
                    span: false,
                    position: "inner",
                    space: 5,
                    h_align: "center",
                    v_align: "bottom",
                    h_offset: 0,
                    v_offset: 10
                }
            },
            carousel: {
                horizontal_align: "center",
                vertical_align: "center",
                fadeout: "off",
                maxVisibleItems: 3,
                infinity: "on",
                space: 0,
                stretch: "off"
            },
            viewPort: {
                enable: true,
                outof: "pause",
                visible_area: "80%"
            },
            responsiveLevels: [1240, 1024, 778, 480],
            gridwidth: [1240, 1024, 778, 480],
            gridheight: [350, 350, 250, 350],
            lazyType: "none",
            parallax: {
                type: "mouse",
                origo: "slidercenter",
                speed: 2000,
                levels: [2, 3, 4, 5, 6, 7, 12, 16, 10, 50],
            },
            shadow: 5,
            spinner: "off",
            stopLoop: "off",
            stopAfterLoops: -1,
            stopAtSlide: -1,
            shuffle: "off",
            autoHeight: "off",
            hideThumbsOnMobile: "on",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });
    }
    //

    //rev Slider DESAFIOS
    if ($('#rev_slider_challenges').length) {
        $("#rev_slider_challenges").show().revolution({
            sliderType: "carousel",
            sliderLayout: "fullwidth",
            gridwidth: 800,
            gridheight: 700
        });
    }
    //

    //rev Slider HOME Hangouts
    if ($('#rev_slider_values').length) {
        $("#rev_slider_values").show().revolution({
            sliderType: "carousel",
            //jsFileLocation: "../../revolution/js/",
            sliderLayout: "auto",
            dottedOverlay: "none",
            delay: 9000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                onHoverStop: "off",
                arrows: {
                    style: "erinyen",
                    enable: true,
                    hide_onmobile: true,
                    hide_under: 600,
                    hide_onleave: true,
                    hide_delay: 200,
                    hide_delay_mobile: 1200,
                    tmp: '<div class="tp-title-wrap">    <div class="tp-arr-imgholder"></div>    <div class="tp-arr-img-over"></div> <span class="tp-arr-titleholder">{{title}}</span> </div>',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    }
                }
              ,
                thumbnails: {
                    style: "gyges",
                    enable: true,
                    width: 60,
                    height: 60,
                    min_width: 60,
                    wrapper_padding: 0,
                    wrapper_color: "transparent",
                    wrapper_opacity: "1",
                    tmp: '<span class="tp-thumb-img-wrap">  <span class="tp-thumb-image"></span></span>',
                    visibleAmount: 5,
                    hide_onmobile: true,
                    hide_under: 800,
                    hide_onleave: false,
                    direction: "horizontal",
                    span: false,
                    position: "inner",
                    space: 5,
                    h_align: "center",
                    v_align: "bottom",
                    h_offset: 0,
                    v_offset: 20
                }
            },
            carousel: {
                horizontal_align: "center",
                vertical_align: "center",
                fadeout: "on",
                vary_fade: "off",
                maxVisibleItems: 3,
                infinity: "on",
                space: 0,
                stretch: "off"
            },
            responsiveLevels: [1240, 1024, 778, 480],
            visibilityLevels: [1240, 1024, 778, 480],
            gridwidth: [1000, 800, 700, 480],
            gridheight: [500, 500, 400, 300],
            lazyType: "none",
            shadow: 0,
            spinner: "off",
            stopLoop: "on",
            stopAfterLoops: 0,
            stopAtSlide: 1,
            shuffle: "off",
            autoHeight: "off",
            disableProgressBar: "on",
            hideThumbsOnMobile: "off",
            hideSliderAtLimit: 0,
            hideCaptionAtLimit: 0,
            hideAllCaptionAtLilmit: 0,
            debugMode: false,
            fallbacks: {
                simplifyAll: "off",
                nextSlideOnWindowFocus: "off",
                disableFocusListener: false,
            }
        });
    }

});