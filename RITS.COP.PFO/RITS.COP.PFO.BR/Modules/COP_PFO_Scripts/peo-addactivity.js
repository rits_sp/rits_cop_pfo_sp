//form field add
    var max_fields      = 10; //maximum input boxes allowed
    var add_links_button      = $(".add_links_button");
    var add_hangouts_button = $(".add_hangouts_button");
    var add_videos_button      = $(".add_videos_button");
    var x = 1;
    var y = 1;
    var z = 1;

    $(add_links_button).click(function (e) { //on add input button click
        var wrapper         = $(this).parent(".input_fields_wrap"); //Fields wrapper
        var idname = $(this).closest('.input_fields_wrap').attr('id');
        var clone = $(this).parent().find('.form-element:first-child').clone().removeClass('is-invalid is-upgraded is-dirty').removeAttr('data-upgraded').append('<div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray remove_field">Remover</div>');
        clone.find('input').val('').removeClass('error').removeAttr('aria-invalid').attr('id', idname + (x+1));
        clone.find('label.error').remove();
        clone.find('label').attr('for', idname + (x+1));
        var clonedfield = clone.find('input');
        //clone.find('label').remove();
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
           // $(wrapper).append('<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"><input class="mdl-textfield__input" type="text" id="'+ idname + x + '" required><label class="mdl-textfield__label" for="ename">URL do vídeo</label><a href="#" class="remove_field">Remove</a></div></div>'); //add input box
           $(wrapper).append(clone);
           componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');
        }
    });
    $(add_hangouts_button).click(function (e) { //on add input button click
        var wrapper = $(this).parent(".input_fields_wrap"); //Fields wrapper
        var idname = $(this).closest('.input_fields_wrap').attr('id');
        var clone = $(this).parent().find('.form-element:first-child').clone().removeClass('is-invalid is-upgraded is-dirty').removeAttr('data-upgraded').append('<div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray remove_field">Remover</div>');
        clone.find('input').val('').removeClass('error').removeAttr('aria-invalid').attr('id', idname + (y + 1));
        clone.find('label.error').remove();
        clone.find('label').attr('for', idname + (y + 1));
        var clonedfield = clone.find('input');
        //clone.find('label').remove();
        e.preventDefault();
        if (y < max_fields) { //max input box allowed
            y++; //text box increment
            // $(wrapper).append('<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"><input class="mdl-textfield__input" type="text" id="'+ idname + x + '" required><label class="mdl-textfield__label" for="ename">URL do vídeo</label><a href="#" class="remove_field">Remove</a></div></div>'); //add input box
            $(wrapper).append(clone);
            componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');
        }
    });
    $(add_videos_button).click(function (e) { //on add input button click
        var wrapper = $(this).parent(".input_fields_wrap"); //Fields wrapper
        var idname = $(this).closest('.input_fields_wrap').attr('id');
        var clone = $(this).parent().find('.form-element:first-child').clone().removeClass('is-invalid is-upgraded is-dirty').removeAttr('data-upgraded').append('<div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray remove_field">Remover</div>');
        clone.find('input').val('').removeClass('error').removeAttr('aria-invalid').attr('id', idname + (z + 1));
        clone.find('label.error').remove();
        clone.find('label').attr('for', idname + (z + 1));
        var clonedfield = clone.find('input');
        //clone.find('label').remove();
        e.preventDefault();
        if (z < max_fields) { //max input box allowed
            z++; //text box increment
            // $(wrapper).append('<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label"><input class="mdl-textfield__input" type="text" id="'+ idname + x + '" required><label class="mdl-textfield__label" for="ename">URL do vídeo</label><a href="#" class="remove_field">Remove</a></div></div>'); //add input box
            $(wrapper).append(clone);
            componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');
        }
    });


    

    $('.bt-addmore').click(function() {
        $(this).closest('.input_fields_wrap').find('.fileup.hide').first().removeClass('hide');
        componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');
    });

    $('.bt-removethis').click(function () {
        $(this).closest('.fileup').find('input').val('');
        $(this).closest('.fileup').find('.label.error').text('');
        $(this).closest('.fileup').find('input.displayName').removeAttr('required');
        componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');
        $(this).closest('.fileup').addClass('hide');
    });
    $('.bt-clearthis').click(function () {
        $(this).closest('.fileup').find('input').val('');
        $(this).closest('.fileup').find('.label.error').hide();
        $(this).closest('.fileup').find('input.displayName').removeAttr('required');
        componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');
    });
    $('.bt-clearthislink').click(function () {
        $(this).closest('.form-element').find('input').val('');
        componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');
    });

    $('input[type="file"]').change(function () {
        var fileName = $(this).val();
        $(this).prev().find('input.displayName').attr('required','true')/*.val(fileName)*/;
        componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');
    });

    $('#links.input_fields_wrap').on("click", ".remove_field", function(e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        x--;
    });
    $('#hangout.input_fields_wrap').on("click", ".remove_field", function(e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        y--;
    });
    $('#video.input_fields_wrap').on("click", ".remove_field", function(e) { //user click on remove text
        e.preventDefault();
        $(this).parent('div').remove();
        z--;
    });
  //
    $(document).ready(function () {

     //if mobile portrait change submit buttons position
        if ((isMobile) && $('body').hasClass('portrait')) {
            $('.actionbuttons').appendTo('.createstuff');
        }
     //

     //change search function on add pages
     $('.searchsubmitbt').removeAttr('onclick');
     $('.searchsubmitbt').click(function() {
         __doPostBack('ctl00$UC_header$ctl00', '');
     });
     //
   
    //ask to confirm before leaving the page, if this is changed.
    //    function askConfirm() {
    //        return "Irá perder todos os dados não gravados!"; 
    //    }

    //var needToConfirm = false;
    //    $("select,input,textarea").change(function () {
    //        needToConfirm = true;
    //    });

    //    $('#searchsubmit input').change(function () {
    //        needToConfirm = false;
    //    });

    //    $('.textarea').wysihtml5({
    //        "events": {
    //            change: function () {
    //                needToConfirm = true;
    //            }
    //        }
        //    });

        $('.textarea').wysihtml5();

        //ask to confirm before leaving the page, if something is changed.
        //if (needToConfirm) {
        //    $('body').click(function(e) {
        //        //console.log(e);
        //        if ($(e.target).hasClass('bt-save') || $(e.target).hasClass('searchsubmitbt')) {
        //            window.onbeforeunload = null;
        //        } else {
        //            window.onbeforeunload = askConfirm;
        //        }
        //    });
        //}


     //Rich Text Validator
        $("#content_text").bind('paste', function () {
            $('#content_text').find('href').remove();
            $('#content_text').find('img').remove();
            $('#content_text').find('pre').remove();
            $('#content_text').find('code').remove();
            $('#content_text').find('h1').replaceWith(function () {
                return '<h2>' + $(this).text() + '</h2>';
            });
        });
        $('#content_text').bind("DOMFocusOut", function () {
            $('#content_text').find('a').remove();
            $('#content_text').find('img').remove();
            $('#content_text').find('pre').remove();
            $('#content_text').find('code').remove();
            $('#content_text').find('h1').replaceWith(function () {
                return '<h2>' + $(this).text() + '</h2>';
            });
            var max = 1000;
            $('#content_text_counter').show();
            $('#content_text_counter').val(max - $("#content_text").text().length);
            $('#content_text').keypress(function (e) {
                if (e.which < 0x20) {
                    // e.which < 0x20, then it's not a printable character
                    // e.which === 0 - Not a character
                    return; // Do nothing
                }
                if ($("#content_text").text().length == max) {
                    e.preventDefault();
                } else if ($("#content_text").text().length.length > max) {
                    // Maximum exceeded
                    $("#content_text").text().length = this.value.substring(0, max);
                    $('#content_text_counter').css('color', 'red');
                } else if ($("#content_text").text().length.length < 0) {
                    $('#content_text').html($('#content_text').html().slice(0, max));
                }
            });

            

            //if ($('#content_text_counter').val() >= 10) {
            //    $('#content_text').attr('contenteditable', 'false');

            //}

        });

    $('#content_text').bind("DOMFocusIn", function() {
        $('#content_text_counter').hide();
        $('#content_text_counter-error').hide();
        $('#content_text_counter').css('color', 'gray');
    });
       
        
    //alerts
        if ($('.alert span').hasClass('success')) {
            $('.alert').addClass('alert-success');
        } else {
            $('.alert').addClass('alert-warning');
        }
        if (!$.trim($('.alert span').html()).length) {
            $('.alert').remove();
        }
     //

     //block letters in numeric fields
        $("#activityHours, #numberAttendes").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode == 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
     //
});
//