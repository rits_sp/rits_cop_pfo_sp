$('.nav').find('.item.publications').addClass('selected');

//responsive filtering tab
function closepanel() {
    $('.filterbox').animate({ "left": "-100%" }, "slow");
    setTimeout(function () {
        $('#resources-close-panel').remove();
    }, 500);
}

$('document').ready(function () {
    $('footer li a.publications').addClass('selected');
    $('#filtering').jplist({


        //enable/disable logging information: if firebug is installed the debuging information will be displayed in the firebug console
        debug: false

        //main options
        , itemsBox: '.list' //items container jQuery path
        , itemPath: '.list-item' //jQuery path to the item within the items container
        , panelPath: '.jplist-panel' //panel jQuery path
        , noResults: '.jplist-no-results' //'no reaults' section jQuery path
        , redrawCallback: function (collection, $dataview, statuses) {
            //this code occurs on every jplist action
            if ($('body').hasClass('mobile')) {
                $('#resources-close-panel i').removeClass('.zmdi-close-circle-o').addClass('zmdi-eye');
            }
        }
        , iosBtnPath: '.jplist-ios-button'

        //animate to top - enabled by data-control-animate-to-top="true" attribute in control
        , animateToTop: 'html, body'
        , animateToTopDuration: 0 //in milliseconds (1000 ms = 1 sec)

        //animation effects
        , effect: '' //'', 'fade'
        , duration: 300
        , fps: 24

        //save plugin state with storage
        , storage: '' //'', 'cookies', 'localstorage'      
        , storageName: 'jplist'
        , cookiesExpiration: -1 //cookies expiration in minutes (-1 = cookies expire when browser is closed)

        //deep linking
        , deepLinking: false
        , delimiter0: ':' //this delimiter is placed after the control name 
        , delimiter1: '|' //this delimiter is placed between key-value pairs
        , delimiter2: '~' //this delimiter is placed between multiple value of the same key
        , delimiter3: '!' //additional delimiter

        //history
        , historyLength: 10

        //data source
        , dataSource: {

            type: 'html' //'html', 'server'

            //data source server side
            //,server: {

            //    //ajax settings
            //    ajax:{
            //        //url: 'server.php',
            //    dataType: 'html'
            //    ,type: 'POST'
            //        //,cache: false
            //    }
            // ,serverOkCallback: null
            // ,serverErrorCallback: null
            //}
        }

        //panel controls
        , controlTypes: {

            'default-sort': {
                className: 'DefaultSort'
                , options: {}
            }

        , 'drop-down': {
            className: 'Dropdown'
            , options: {}
        }

        , 'pagination-info': {
            className: 'PaginationInfo'
            , options: {}
        }

        , 'counter': {
            className: 'Counter'
            , options: {
                ignore: '[~!@#$%^&*()+=`\'"\/\\_]+' //[^a-zA-Z0-9]+ not letters/numbers: [~!@#$%^&*\(\)+=`\'"\/\\_]+
            }
        }

        , 'pagination': {
            className: 'Pagination'
            , options: {

                //paging
                range: 4
            , jumpToStart: false

                //arrows
            , prevArrow: '�'
            , nextArrow: '�'
            , firstArrow: '�'
            , lastArrow: '�'
            }
        }

        , 'reset': {
            className: 'Reset'
            , options: {}
        }

        , 'select': {
            className: 'Select'
            , options: {}
        }

        , 'textbox': {
            className: 'Textbox'
            , options: {
                eventName: 'keyup'
            , ignore: '[~!@#$%^&*()+=`\'"\/\\_]+' //[^a-zA-Z0-9]+ not letters/numbers: [~!@#$%^&*\(\)+=`\'"\/\\_]+              
            }
        }

        , 'views': {
            className: 'Views'
            , options: {}
        }

        , 'checkbox-group-filter': {
            className: 'CheckboxGroupFilter'
            , options: {}
        }

        , 'checkbox-text-filter': {
            className: 'CheckboxTextFilter'
            , options: {
                ignore: '' //regex for the characters to ignore, for example: [^a-zA-Z0-9]+
            }
        }

        , 'button-filter': {
            className: 'ButtonFilter'
            , options: {}
        }

        , 'button-filter-group': {
            className: 'ButtonFilterGroup'
            , options: {}
        }

        , 'button-text-filter': {
            className: 'ButtonTextFilter'
            , options: {
                ignore: '[~!@#$%^&*()+=`\'"\/\\_]+' //[^a-zA-Z0-9]+ not letters/numbers: [~!@#$%^&*\(\)+=`\'"\/\\_]+
            }
        }

        , 'button-text-filter-group': {
            className: 'ButtonTextFilterGroup'
            , options: {
                ignore: '[~!@#$%^&*()+=`\'"\/\\_]+' //[^a-zA-Z0-9]+ not letters/numbers: [~!@#$%^&*\(\)+=`\'"\/\\_]+
            }
        }

        , 'radio-buttons-filters': {
            className: 'RadioButtonsFilter'
            , options: {}
        }

            , 'range-filter': {
                className: 'Range<a href="http://www.jqueryscript.net/slider/">Slider</a>ToggleFilter'
            , options: {}
            }

        , 'back-button': {
            className: 'BackButton'
            , options: {}
        }

        , 'preloader': {
            className: 'Preloader'
            , options: {}
        }
        }
    });



    $('#resources-responsive-panel').click(function () {
        $('.filterbox').prepend('<a onclick="closepanel();" id="resources-close-panel" class="responsive-item mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"><i class="zmdi zmdi-close-circle-o"></i></a>');
        $('.filterbox').animate({ "left": "0px" }, "slow");
    });

    //resources-responsive-panel responsive fix for scrolling
    $(".portrait #resources-responsive-panel .filterbox").css({ maxHeight: $(window).height() - $(".navbar-header").height() + "px" });
    //

    componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');

    //if ipad landscape change bootstrap lg column to md
    if ((window.innerHeight < window.innerWidth) && (navigator.userAgent.match(/(iPad)/))) {
        $('#filtering .col-md-9').removeClass('col-md-9').addClass('col-md-8');
        $('#filtering .col-md-3').removeClass('col-md-3').addClass('col-md-4');
        $('.jplist-drop-down li span[data-number="3"]').attr('data-number', '2').text('2 por p�gina');
        $('.jplist-drop-down li span[data-number="6"]').attr('data-number', '4').text('4 por p�gina').attr('data-default', 'true').click();
        $('.jplist-drop-down li span[data-number="9"]').attr('data-number', '8').text('8 por p�gina');
    }
});


$(window).load(function () {

        
    $("#revisors").flexisel({
        visibleItems: 3,
        itemsToScroll: 3,
        animationSpeed: 1000,
        infinite: true,
        navigationTargetSelector: null,
        autoPlay: {
            enable: true,
            interval: 5000,
            pauseOnHover: true
        },
        responsiveBreakpoints: {
            portrait: {
                changePoint: 480,
                visibleItems: 1,
                itemsToScroll: 1
            },
            landscape: {
                changePoint: 640,
                visibleItems: 2,
                itemsToScroll: 2
            },
            tablet: {
                changePoint: 768,
                visibleItems: 3,
                itemsToScroll: 3
            }
        }
    });

});