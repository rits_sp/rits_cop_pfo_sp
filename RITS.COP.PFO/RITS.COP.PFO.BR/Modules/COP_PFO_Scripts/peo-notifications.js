//notifications datatable
function trim(str) {
    str = str.replace(/^\s+/, '');
    for (var i = str.length - 1; i >= 0; i--) {
        if (/\S/.test(str.charAt(i))) {
            str = str.substring(0, i + 1);
            break;
        }
    }
    return str;
}

function dateHeight(dateStr) {
    if (trim(dateStr) != '') {
        var frDate = trim(dateStr).split(' ');
        var frTime = frDate[1].split(':');
        var frDateParts = frDate[0].split('/');
        var day = frDateParts[0] * 60 * 24;
        var month = frDateParts[1] * 60 * 24 * 31;
        var year = frDateParts[2] * 60 * 24 * 366;
        var hour = frTime[0] * 60;
        var minutes = frTime[1];
        var x = day + month + year + hour + minutes;
    } else {
        var x = 99999999999999999; //GoHorse!
    }
    return x;
}

jQuery.fn.dataTableExt.oSort['date-euro-asc'] = function (a, b) {
    var x = dateHeight(a);
    var y = dateHeight(b);
    var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
    return z;
};

jQuery.fn.dataTableExt.oSort['date-euro-desc'] = function (a, b) {
    var x = dateHeight(a);
    var y = dateHeight(b);
    var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
    return z;
};


if (($(window).width() < 1025) || (navigator.userAgent.match(/(iPad)/))) {
    $.extend($.fn.dataTable.defaults, {
        paging: true
    });
}
else {
    $.extend($.fn.dataTable.defaults, {
        scrollY: "400px",
        paging: false
    });
}

if ($('#status-comment')) {

    var table = $('#status-comment').DataTable({
        scrollCollapse: true,
        "aoColumns": [
                null,
                null,
                null,
                null,
                { "sType": "date-euro" }
        ],
        columnDefs: [
            {
                targets: 0,
                width: "20%"
            },
            {
                targets: 1,
                width: "30%"
            },
            {
                targets: 2,
                width: "15%"
            },
            {
                targets: 3,
                width: "15%"
            },
            {
                targets: 4,
                width: "10%"
            },
            {
                orderable: false,
                //className: 'select-checkbox',
                targets: 5,
                width: "10%"
            }
        ],
        /*select: {
            style:    'os',
            selector: 'td:first-child'
        },*/
        order: [[4, 'desc']],
        /*responsive: {
              details: {
                type:"column",
                target:"tr"
              }
        },*/
        responsive: "true",
        /*language: {
                url: "js/datatable/Portuguese.json"
            },*/
        language: {
            sProcessing: "A processar...",
            sLengthMenu: "Mostrar _MENU_ registos",
            sZeroRecords: "Sem resultados",
            sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registos",
            sInfoEmpty: "Mostrando de 0 até 0 de 0 registos",
            sInfoFiltered: "(filtrado de _MAX_ registos no total)",
            sInfoPostFix: "",
            sSearch: "",
            sUrl: "",
            oPaginate: {
                sFirst: "Primeiro",
                sPrevious: "Anterior",
                sNext: "Seguinte",
                sLast: "Último"
            }
        },
        "fnInitComplete": function (oSettings) {
            $('#status-comment_filter').prepend('<div id="datatable-searchfield"><div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable"><label id="datatable-searchfield-button-comments" class="mdl-button mdl-js-button mdl-button--icon" for="serchtable-comment"><i class="zmdi zmdi-search"></i></label><div id="tablesearch-container-comment" class="mdl-textfield__expandable-holder"><label class="mdl-textfield__label" for="sample-expandable">Expandable Input</label></div></div></div><div class="mdl-tooltip" for="datatable-searchfield-button-comments">Procurar por notificações</div>');
            $('#status-comment_filter input').attr('id', 'serchtable-comment').addClass('mdl-textfield__input').prependTo($('#tablesearch-container-comment'));
        }
    });

    var tableComments = $('#status-comment').DataTable();
    var infoComments = tableComments.page.info();

    /*$('#tableInfo-comment-info').html(
      infoRequests.recordsTotal
    );*/


}

var currentdate = new Date();
var datenow = currentdate.getDate() + "/"
            + (currentdate.getMonth() + 1) + "/"
            + currentdate.getFullYear();
var datetime = currentdate.getHours() + ":"
            + currentdate.getMinutes() + ":"
            + currentdate.getSeconds();

//$('table.display').on( 'click', 'button.checkthis', function () {
//    table
//        .row( $(this).parents('tr').addClass('selected') )
//        .row( $(this).children('i').removeClass('fa-square-o'))
//        .row( $(this).children('i').addClass('fa-check-square-o'))
//        .row( $(this).parent().next('.seenby').removeClass('hide'))
//        .row( $(this).parent().next('.seenby').children('.seen-date').html(datenow + '<br>' + datetime) );
//});

//$('table.display').on( 'click', 'a.checkthis-link', function () {
//    table
//        .row( $(this).parents('tr').addClass('selected') );
//});

///*$('#status tbody').on( 'click', 'button.deletethis', function () {
//    var deletebt = $(this).data('id');
//    $('.modal-footer .delete').
//    table
//        .row( $(this).parents('tr'))
//        .remove()
//        .draw();
//});*/

//$('button.deletethis').on('click', function (e) {
//    e.preventDefault();
//    var id = $(this).closest('tr').data('id');
//    $('#ConfirmDialog').data('id', id).modal('show');
//});

//$('#btnDelteYes').click(function () {
//    var id = $('#ConfirmDialog').data('id');
//    $('[data-id=' + id + ']').remove();   // example: http://jsfiddle.net/Se52D/5/
//    $('#ConfirmDialog').modal('hide');  
//});


// end of notifications datatable

//requests datatable
if ($('#status-requests')) {

    var table = $('#status-requests').DataTable({
        scrollCollapse: true,
        "aoColumns": [
                null,
                null,
                null,
                null,
                null,
                null,
                { "sType": "date-euro" }
        ],
        columnDefs: [ 
       
        {
            orderable: false,
             targets:   7
          }
        ],
        order: [[6, 'desc']],
        /*responsive: {
              details: {
                type:"column",
                target:"tr"
              }
        },*/
        responsive: "true",
        /*language: {
                url: "js/datatable/Portuguese.json"
            },*/
        language: {
            sProcessing: "A processar...",
            sLengthMenu: "Mostrar _MENU_ registos",
            sZeroRecords: "Sem resultados",
            sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registos",
            sInfoEmpty: "Mostrando de 0 até 0 de 0 registos",
            sInfoFiltered: "(filtrado de _MAX_ registos no total)",
            sInfoPostFix: "",
            sSearch: "",
            sUrl: "",
            oPaginate: {
                sFirst: "Primeiro",
                sPrevious: "Anterior",
                sNext: "Seguinte",
                sLast: "Último"
            }
        },
        "fnInitComplete": function (oSettings) {
            $('#status-requests_filter').prepend('<div id="datatable-searchfield"><div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable"><label id="datatable-searchfield-button-requests" class="mdl-button mdl-js-button mdl-button--icon" for="serchtable-requests"><i class="zmdi zmdi-search"></i></label><div id="tablesearch-container-requests" class="mdl-textfield__expandable-holder"><label class="mdl-textfield__label" for="sample-expandable">Expandable Input</label></div></div></div><div class="mdl-tooltip" for="datatable-searchfield-button-requests">Procurar por notificações</div>');
            $('#status-requests_filter input').attr('id', 'serchtable-requests').addClass('mdl-textfield__input').prependTo($('#tablesearch-container-requests'));
        }
    });
    var tableRequests = $('#status-requests').DataTable();
    var infoRequests = tableRequests.page.info();

    /* $('#tableInfo-requests-info').html(
       infoRequests.recordsTotal
     );*/
}



/* 
$('#tableInfo').html(
    'Currently showing page '+(info.page+1)+' of '+info.pages+' pages.'
);*/


// end of requests datatable

//activities approval datatable
if ($('#status-approval')) {

    var table = $('#status-activities-approval').DataTable({
        scrollCollapse: true,
        "aoColumns": [
                null,
                null,
                null,
                { "sType": "date-euro" }
        ],
        columnDefs: [
            {
                targets: 3,
                width: "10%"
            }
        ],
        order: [[3, 'desc']],
        responsive: "true",
        language: {
            sProcessing: "A processar...",
            sLengthMenu: "Mostrar _MENU_ registos",
            sZeroRecords: "Sem resultados",
            sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registos",
            sInfoEmpty: "Mostrando de 0 até 0 de 0 registos",
            sInfoFiltered: "(filtrado de _MAX_ registos no total)",
            sInfoPostFix: "",
            sSearch: "",
            sUrl: "",
            oPaginate: {
                sFirst: "Primeiro",
                sPrevious: "Anterior",
                sNext: "Seguinte",
                sLast: "Último"
            }
        },
        "fnInitComplete": function (oSettings) {
            $('#status-activities-approval_filter').prepend('<div id="datatable-searchfield"><div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable"><label id="datatable-searchfield-button-activities-approval" class="mdl-button mdl-js-button mdl-button--icon" for="serchtable-activities-approval"><i class="zmdi zmdi-search"></i></label><div id="tablesearch-container-activities-approval" class="mdl-textfield__expandable-holder"><label class="mdl-textfield__label" for="sample-expandable">Expandable Input</label></div></div></div><div class="mdl-tooltip" for="datatable-searchfield-button-activities-approval">Procurar por notificações</div>');
            $('#status-activities-approval_filter input').attr('id', 'serchtable-activities-approval').addClass('mdl-textfield__input').prependTo($('#tablesearch-container-activities-approval'));
        }
    });

    var tableActivities = $('#status-activities').DataTable();
    var infoActivities = tableActivities.page.info();

    /*$('#tableInfo-comment-info').html(
      infoRequests.recordsTotal
    );*/
}


//activities approval datatable
if ($('#status-profschool')) {

    var table = $('#status-profschool').DataTable({
        scrollCollapse: true,
        responsive: "true",
        language: {
            sProcessing: "A processar...",
            sLengthMenu: "Mostrar _MENU_ registos",
            sZeroRecords: "Sem resultados",
            sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registos",
            sInfoEmpty: "Mostrando de 0 até 0 de 0 registos",
            sInfoFiltered: "(filtrado de _MAX_ registos no total)",
            sInfoPostFix: "",
            sSearch: "",
            sUrl: "",
            oPaginate: {
                sFirst: "Primeiro",
                sPrevious: "Anterior",
                sNext: "Seguinte",
                sLast: "Último"
            }
        },
        "fnInitComplete": function (oSettings) {
            $('#status-profschool_filter').prepend('<div id="datatable-searchfield"><div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable"><label id="datatable-searchfield-button-activities-approval" class="mdl-button mdl-js-button mdl-button--icon" for="serchtable-activities-approval"><i class="zmdi zmdi-search"></i></label><div id="tablesearch-container-activities-approval" class="mdl-textfield__expandable-holder"><label class="mdl-textfield__label" for="sample-expandable">Expandable Input</label></div></div></div><div class="mdl-tooltip" for="datatable-searchfield-button-activities-approval">Procurar por notificações</div>');
            $('#status-profschool_filter input').attr('id', 'serchtable-activities-approval').addClass('mdl-textfield__input').prependTo($('#tablesearch-container-activities-approval'));
        }
    });

    /*$('#tableInfo-comment-info').html(
      infoRequests.recordsTotal
    );*/
}

//

// end of requests datatable

$(document).ready(function () {
    //redraw datatables on window resize
    $(window).on('resize', function () {
        var table1 = $('#status-comment').DataTable();
        var table2 = $('#status-requests').DataTable();
        var table3 = $('#status-approval').DataTable();
        table1.draw();
        table2.draw();
        table3.draw();
    });

    //
});