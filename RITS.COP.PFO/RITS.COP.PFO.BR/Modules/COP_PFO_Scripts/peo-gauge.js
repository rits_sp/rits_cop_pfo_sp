//Animations Infographics
var gageValue = 0;
var gageValueMax1 = $('#gauge-one-value-max').text();
var gageValueMax2 = $('#gauge-two-value-max').text();
var gageValueMax3 = $('#gauge-three-value-max').text();
var gaugeTypeValue1 = $('#gauge-one-value').text();
var gaugeTypeValue2 = $('#gauge-two-value').text();
var gaugeTypeValue3 = $('#gauge-three-value').text();
if ($('.gauge-anim')) {
    var g1 = new JustGage({
        id: "gauge-one",
        value: gageValue,
        min: 0,
        max: gageValueMax1,
        relativeGaugeSize: true,
        donut: true
        //,title: "Visitors"
    });
    var g2 = new JustGage({
        id: "gauge-two",
        value: gageValue,
        min: 0,
        max: gageValueMax2,
        relativeGaugeSize: true,
        donut: true
        //,title: "Visitors"
    });
    var g3 = new JustGage({
        id: "gauge-three",
        value: gageValue,
        min: 0,
        max: gageValueMax3,
        relativeGaugeSize: true,
        donut: true
        //,title: "Visitors"
    });
}


$('.gauge-anim').scrolling();
function gaugechange() {
    setTimeout(function () {
        g1.refresh(gageValue + gaugeTypeValue1);
        g2.refresh(gageValue + gaugeTypeValue2);
        g3.refresh(gageValue + gaugeTypeValue3);
        jQuery('svg tspan').attr('style', 'font-size:17px;').parent().attr('transform', 'translate(0,-5)');
    }, 2000);
}
$('.gauge-anim').on('scrollin', function (event, $all_elements) {
    gaugechange();
});

if ((!$('body').scrollTop() == 0) && $('.gauge-anim').is(':visible')) {
    gaugechange();
}


//end of about page Animations Infographic

jQuery(document).ready(function () {
    jQuery('svg tspan').attr('style', 'font-size:17px;').parent().attr('transform', 'translate(0,-5)');
});



