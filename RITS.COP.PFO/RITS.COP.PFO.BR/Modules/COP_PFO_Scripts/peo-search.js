function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var prodId = getParameterByName('k');

$('#searchsite').val(prodId);
$('#searchsite').parents('#search').find('.mdl-textfield').addClass('is-focused');

if (($(window).width() < 1025) || (navigator.userAgent.match(/(iPad)/))) {
    $.extend($.fn.dataTable.defaults, {
        paging: true
    });
}
else {
    $.extend($.fn.dataTable.defaults, {
        scrollY: "600px",
        paging: false,
    });
}

if ($('#search-table')) {

    var table = $('#search-table').DataTable({
        scrollCollapse: true,
        language: {
            sProcessing: "A processar...",
            sLengthMenu: "Mostrar _MENU_ registos",
            sZeroRecords: "Sem resultados",
            sInfo: "Mostrando de _START_ at� _END_ de _TOTAL_ registos",
            sInfoEmpty: "Mostrando de 0 at� 0 de 0 registos",
            sInfoFiltered: "(filtrado de _MAX_ registos no total)",
            sInfoPostFix: "",
            sSearch: "",
            sUrl: "",
            oPaginate: {
                sFirst: "Primeiro",
                sPrevious: "Anterior",
                sNext: "Seguinte",
                sLast: "�ltimo"
            }
        },
        responsive: "true"
        //,"bFilter": false
    });

}