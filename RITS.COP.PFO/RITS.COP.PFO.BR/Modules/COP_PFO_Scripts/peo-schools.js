 //map locator
  if('#jlocator'){
    $('#jlocator').jlocator({
         
         startZoom: 9 //initial map zoom (on page load)
         ,storeZoom: 16 //map zoom on store click
         ,latitude: 38.7436883 //initial map latitude (on page load)
         ,longitude: -9.1953089 //initial map longitude (on page load)
         
         ,geolocation: true
         , markerIcon: '../../../../_layouts/15/images/RITS.COP.BR/icons/map-pointer.png' //marker icon path ('' for default google icon)
         ,markerText: 'Click to Zoom'
         ,directionsType: 'DRIVING' //DRIVING, BICYCLING, TRANSIT, WALKING
         ,mapTypeId: 'ROADMAP' //ROADMAP, SATELLITE, HYBRID, TERRAIN
         
         //if info window should be opened on store click
         ,openInfoWindowOnStoreClick: true 
         
         //map popup window content
         ,infoWindow: function(html, title, address, city, state, zipcode, country){
            return '<div class="info-window">' + html + '</div>';
         }
         
         //ADVANCED OPTIONS ----------------------------------------------------------------------
         
         //jplist options: http://jplist.no81no.com/
         ,jplist: {
            
            items_box: '[data-type="stores"]' 
            ,item_path: '[data-type="store"]'
            ,panel_path: '[data-type="controls"]'
            ,no_results: '[data-type="no-results"]'
            ,redraw_callback: function(){
               
            }
            
            //panel controls
            ,control_types: {
               
               'autocomplete':{
                  class_name: 'Autocomplete' 
                  ,options: {}
               }
               
               ,'autocomplete-radius':{
                  class_name: 'AutocompleteRadius' 
                  ,options: {}
               }
               
            }
         }      

         //events
         ,storeClickEvent: 'storeClickEvent'
         ,pinClickEvent: 'pinClickEvent'
         ,sendStoreListEvent: 'sendStoreListEvent'
         ,getStoresListEvent: 'getStoresListEvent'
         ,setDirectionsEvent: 'setDirectionsEvent'
         ,jumpEvent: 'jumpEvent'
         ,initZoomEvent: 'initZoomEvent'  
      });
  }
//end of map locator

  function closepanel() {
      $('#jlocator .panel').animate({ "left": "-100%" }, "slow");
      setTimeout(function () {
          $('#jlocator-close-panel').remove();
      }, 500);
  }

  $('#jlocator-responsive-panel').click(function () {
      $('#jlocator .panel').prepend('<a onclick="closepanel();" id="jlocator-close-panel" class="responsive-item mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"><i class="zmdi zmdi-close-circle-o"></i></a>');
      $('#jlocator .panel').animate({ "left": "0px" }, "slow");
  });
  $('div.store').click(function () {
      if ($('#jlocator-close-panel')) {
          closepanel();
      }
  });