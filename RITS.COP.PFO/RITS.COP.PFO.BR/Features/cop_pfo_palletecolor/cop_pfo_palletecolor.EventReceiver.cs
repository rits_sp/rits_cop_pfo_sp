using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Utilities;
using RITS.COP.PFO.BR.Utils;

namespace RITS.COP.PFO.BR.Features.cop_peo_palletecolor
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("525219a3-01ce-446a-8d4d-6157f4d6f80f")]
    public class cop_peo_palletecolorEventReceiver : SPFeatureReceiver
    {

        #region Variables
        public const string CustomTheme = "PFO_PalleteColor";
        public const string CustomPalette = "/_catalogs/theme/15/PFO_PalleteColor.spcolor";

        public const string DefaultTheme = "Office";
        public const string DefaultPalette = "/_catalogs/theme/15/palette001.spcolor";
        #endregion

        #region Events
        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            try
            {
                SPSite site = properties.Feature.Parent as SPSite;
                if (site != null)
                {
                    var _customTheme = SPTheme.Open(CustomTheme, site.RootWeb.GetFile(CustomPalette));

                    var brandingManager = new COP_BrandingManager(site.RootWeb);
                    brandingManager.ApplyTheme(_customTheme);
                }
            }
            catch (Exception ex)
            {

            }
        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            try
            {
                SPSite site = properties.Feature.Parent as SPSite;
                if (site != null)
                {
                    var _defaultTheme = SPTheme.Open(DefaultTheme, site.RootWeb.GetFile(DefaultPalette));

                    var brandingManager = new COP_BrandingManager(site.RootWeb);
                    brandingManager.ApplyTheme(_defaultTheme);
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}
