using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.BR.Features.cop_peo_masterpage
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("b070af9f-e5cd-40f0-955b-c21987801a27")]
    public class cop_peo_masterpageEventReceiver : SPFeatureReceiver
    {
        #region Variables
        private const string DefaultMasterPageUrl = "/_catalogs/masterpage/seattle.master";
        private const string CustomMasterPageUrl = "/_catalogs/masterpage/COP_PFO_MasterPage.master";
        private const string CustomLogo = "../../Style%20Library/cop/imgs/icons/ms-icon-144x144.png";
        private const string DefaultLogo = "_layouts/images/titlegraphic.gif";
        private const string LogoDescription = "PFO";
        #endregion

        #region Events

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            try
            {
                using (var site = properties.Feature.Parent as SPSite)
                {
                    if (site == null) return;
                    using (var web = site.OpenWeb())
                    {
                        var masterUrl = web.Site.ServerRelativeUrl == "/" ? CustomMasterPageUrl : web.Site.ServerRelativeUrl + CustomMasterPageUrl;
                        var customMasterUrl = web.Site.ServerRelativeUrl == "/" ? DefaultMasterPageUrl : web.Site.ServerRelativeUrl + DefaultMasterPageUrl;

                        // Define Master page for FO and BO
                        web.CustomMasterUrl = masterUrl;
                        web.MasterUrl = customMasterUrl;

                        // Inheritance Settings
                        web.AllProperties["__InheritsMasterUrl"] = "True";
                        web.AllProperties["__InheritsCustomMasterUrl"] = "True";

                        //Apply Logo and LogoDescription
                        web.SiteLogoUrl = web.ServerRelativeUrl + CustomLogo;
                        web.SiteLogoDescription = LogoDescription;
                        web.Update();

                        //Process All Webs
                        foreach (SPWeb subWeb in web.Webs)
                        {
                            try
                            {
                                subWeb.SiteLogoUrl = web.ServerRelativeUrl + CustomLogo;
                                subWeb.SiteLogoDescription = LogoDescription;
                                ProcessSubWebs(subWeb);
                            }
                            finally
                            {
                                if (subWeb != null) subWeb.Dispose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "COP - Error Activating Feature - cop_masterpages", ex.Message);
            }
        }

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {

            try
            {
                using (var site = properties.Feature.Parent as SPSite)
                {
                    if (site == null) return;
                    using (var web = site.OpenWeb())
                    {

                        var customMasterUrl = web.Site.ServerRelativeUrl == "/" ? DefaultMasterPageUrl : web.Site.ServerRelativeUrl + DefaultMasterPageUrl;

                        // Define Master page for FO and BO
                        web.CustomMasterUrl = customMasterUrl;
                        web.MasterUrl = customMasterUrl;

                        // inheritance settings
                        web.AllProperties["__InheritsMasterUrl"] = "True";
                        web.AllProperties["__InheritsCustomMasterUrl"] = "True";

                        //Apply Logo and LogoDescription
                        web.SiteLogoUrl = web.ServerRelativeUrl + DefaultLogo;
                        web.SiteLogoDescription = LogoDescription;
                        web.Update();

                        //Process All Webs
                        foreach (SPWeb subWeb in web.Webs)
                        {
                            try
                            {
                                subWeb.SiteLogoUrl = web.ServerRelativeUrl + DefaultLogo;
                                subWeb.SiteLogoDescription = LogoDescription;
                                ProcessSubWebs(subWeb);
                            }
                            finally
                            {
                                if (subWeb != null) subWeb.Dispose();
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "000", "COP - FeatureDeactivating - cop_masterpages", ex.Message);
            }
        }

        #endregion

        #region Methods
        private static void ProcessSubWebs(SPWeb web)
        {
            try
            {
                web.CustomMasterUrl = web.ParentWeb.CustomMasterUrl;
                web.MasterUrl = web.ParentWeb.MasterUrl;
                web.AlternateCssUrl = web.ParentWeb.AlternateCssUrl;

                web.Update();

                foreach (SPWeb subWeb in web.Webs)
                {
                    try
                    {
                        ProcessSubWebs(subWeb);
                    }
                    finally
                    {
                        if (subWeb != null)
                            subWeb.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "000", "COP - Error ProcessSubWebs - cop_masterpages", ex.Message);
            }
        }
        #endregion
    }
}
