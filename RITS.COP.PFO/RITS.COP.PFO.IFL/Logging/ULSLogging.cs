﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Administration;

namespace RITS.COP.PFO.IFL.Logging
{
    public class ULSLogging : SPDiagnosticsServiceBase
    {

        public static string DiagnosticAreaName = "Fishbone";
        private static ULSLogging _current;
        public static ULSLogging Current
        {
            get { return _current ?? (_current = new ULSLogging()); }
        }

        public ULSLogging()
            : base("Fishbone Logging Service", SPFarm.Local)
        { }


        public enum Category
        {
            Unexpected,
            High,
            Medium,
            Information
        }

        protected override IEnumerable<SPDiagnosticsArea> ProvideAreas()
        {
            var areas = new List<SPDiagnosticsArea>
        {
            new SPDiagnosticsArea(DiagnosticAreaName, new List<SPDiagnosticsCategory>
            {
                new SPDiagnosticsCategory("Unexpected", TraceSeverity.Unexpected, EventSeverity.Error),
                new SPDiagnosticsCategory("High", TraceSeverity.High, EventSeverity.Warning),
                new SPDiagnosticsCategory("Medium", TraceSeverity.Medium, EventSeverity.Information),
                new SPDiagnosticsCategory("Information", TraceSeverity.Verbose, EventSeverity.Information)
            })
        };

            return areas;
        }

        public static void WriteLog(Category categoryName, string codeError, string source, string errorMessage)
        {
            var category = Current.Areas[DiagnosticAreaName].Categories[categoryName.ToString()];
            Current.WriteTrace(0, category, category.TraceSeverity, string.Concat(codeError, " - ", source, " : ", errorMessage));
        }
    }

}
