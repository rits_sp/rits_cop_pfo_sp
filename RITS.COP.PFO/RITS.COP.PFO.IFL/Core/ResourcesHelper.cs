﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint.Utilities;

namespace RITS.COP.PFO.IFL.Core
{

    public static class ResourcesHelper
    {
        public static String ResourceLabel { get; set; }

        public static string GetStringFromResource(string resourceFileName, string resourceKey, string lang)
        {
            try
            {
                uint licd;

                if (lang == "PT")
                    licd = 2070;
                else
                    licd = 1033;

                if (!String.IsNullOrEmpty(resourceFileName) && !String.IsNullOrEmpty(resourceKey))
                {
                  ResourceLabel = SPUtility.GetLocalizedString("$Resources:" + resourceFileName + "," + resourceKey + "'", "'" + resourceKey + "'", licd).Replace("'","");
                
                }
            }
            catch (Exception)
            {
                return string.Empty; 
            }
            return ResourceLabel.Replace("'",""); 
        }
    }
}
