﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;

namespace RITS.COP.PFO.IFL.Core
{
    public class SpLists
    {
        /// <summary>
        /// Creates a new SPList.
        /// </summary>
        /// <param name="web">Represents the SPWeb.</param>
        /// <param name="listName">Represnets the SPList name.</param>
        /// <param name="listDescription">Represents the SPList description.</param>
        /// <param name="template">Represents the SPList template.</param>
        /// <param name="list">Represents the SPList as output parameter.</param>
        public static void CreateList(SPWeb web, string listName, string listDescription, SPListTemplateType template,
            out SPList list)
        {
            try
            {
                list = null;

                var listExists = Exists(web, listName);

                if (listExists) return;
                web.Lists.Add(listName, listDescription, template);
                web.Update();
                list = web.Lists.TryGetList(listName);
                // list = web.GetList(string.Format("{0}/Lists/{1}/AllItems.aspx", web.Url, listName));
            }
            catch (Exception ex)
            {
                list = null;
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Erro a criar lista",
                    TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Creates a generic list item.
        /// </summary>
        /// <param name="webUri">Represents the SPWeb uri.</param>
        /// <param name="listName"></param>
        /// <param name="title">Represents the SPListItem's title.</param>
        public static void CreateGenericItem(string webUri, string listName, string title)
        {
            try
            {
                using (var site = new SPSite(webUri))
                {
                    site.AllowUnsafeUpdates = true;
                    using (var web = site.OpenWeb())
                    {
                        web.AllowUnsafeUpdates = true;
                        //   var list = web.GetList(string.Format("{0}/Lists/{1}/AllItems.aspx", web.Url, listName));
                        var list = web.Lists.TryGetList(listName);
                        if (list != null)
                        {
                            var item = list.Items.Add();
                            item["Title"] = title;
                            item.Update();
                        }
                        web.AllowUnsafeUpdates = false;
                    }
                    site.AllowUnsafeUpdates = false;
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Erro a criar item genérico",
                    TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Creates a generic list item.
        /// </summary>
        /// <param name="web">Represents the SPWeb.</param>
        /// <param name="listName"></param>
        /// <param name="title">Represents the SPListItem's title.</param>
        public static void CreateGenericItem(SPWeb web, string listName, string title)
        {
            try
            {
                //SPList list = web.Lists[listName];
                //   var list = web.GetList(string.Format("{0}/Lists/{1}/AllItems.aspx", web.Url, listName));
                var list = web.Lists.TryGetList(listName);
                if (list == null) return;
                var item = list.Items.Add();
                item["Title"] = title;
                item.Update();
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Erro a criar item genérico",
                    TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Verifies if a SPList exists on a specific web.
        /// </summary>
        /// <param name="web">Represents the SPWeb.</param>
        /// <param name="listName">Represents the SPList name.</param>
        /// <returns>True if the SPList exists, false otherwise.</returns>
        public static bool Exists(SPWeb web, string listName)
        {
            try
            {
                var list = web.Lists.TryGetList(listName);
                return list != null;
                //return web.GetList(string.Format("{0}/Lists/{1}/AllItems.aspx", web.Url, listName)) != null;
            }
            catch
            {
                return false;
            }
        }


        /// <summary>
        /// Adds a Specific Content Type to List.
        /// </summary>
        /// <param name="web"></param>
        /// <param name="listName">Represents the SPList name.</param>
        /// <param name="contentTypeName">Represents the Content Type Name.</param>
        public static void AddContentTypeToList(SPWeb web, string listName, string contentTypeName)
        {
            try
            {
                // var list = web.GetList(string.Format("{0}/Lists/{1}/AllItems.aspx", web.Url, listName));
                var list = web.Lists.TryGetList(listName);

                if (list == null) return;
                foreach (SPContentType ct in list.ContentTypes)
                {
                    list.ContentTypes.Delete(ct.Id);
                    list.Update();
                }

                var contentType = web.AvailableContentTypes[contentTypeName];

                if (contentType == null) return;
                list.ContentTypesEnabled = true;
                list.ContentTypes.Add(contentType);
                list.Update();
            }
            catch (SPException sPexception)
            {
                SPDiagnosticsService.Local.WriteTrace(0,
                    new SPDiagnosticsCategory("Erro ao adicionar content type à lista, sPexception:",
                        TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, sPexception.Message,
                    sPexception.StackTrace);
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0,
                    new SPDiagnosticsCategory("Erro ao adicionar content type à lista, sPexception:",
                        TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message,
                    ex.StackTrace);
            }
        }

        /// <summary>
        /// Adds a Specific Content Type to List.
        /// </summary>
        /// <param name="web"></param>
        /// <param name="listName">Represents the SPList name.</param>
        /// <param name="contentTypeName">Represents the Content Type Name.</param>
        public static void AddContentTypeToDocLib(SPWeb web, string listName, string contentTypeName)
        {
            try
            {
                var list = (SPDocumentLibrary) web.Lists[listName];

                if (list == null) return;
                foreach (SPContentType ct in list.ContentTypes)
                {
                    list.ContentTypes.Delete(ct.Id);
                    list.Update();
                }

                var contentType = web.AvailableContentTypes[contentTypeName];

                if (contentType == null) return;
                list.ContentTypesEnabled = true;
                list.ContentTypes.Add(contentType);
                list.Update();
            }
            catch (SPException sPexception)
            {
                SPDiagnosticsService.Local.WriteTrace(0,
                    new SPDiagnosticsCategory("Erro ao adicionar content type à lista, sPexception:",
                        TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, sPexception.Message,
                    sPexception.StackTrace);
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0,
                    new SPDiagnosticsCategory("Erro ao adicionar content type à lista, sPexception:",
                        TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message,
                    ex.StackTrace);
            }
        }

        /// <summary>
        /// Adds the content type fields to a view.
        /// </summary>
        /// <param name="webUri">The web URI.</param>
        /// <param name="listName">Name of the list.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="contentTypeName">Name of the content type.</param>
        /// <param name="includeTitleField"></param>
        public static void AddContentTypeFieldsToView(string webUri, string listName,
            string viewName, string contentTypeName, bool includeTitleField)
        {
            try
            {
                using (var site = new SPSite(webUri))
                {
                    using (var web = site.OpenWeb())
                    {
                        //  var list = web.GetList(string.Format("{0}/Lists/{1}/AllItems.aspx", web.Url, listName));
                        var list = web.Lists.TryGetList(listName);
                        if (list == null) return;
                        SPContentType contentType = null;
                        try
                        {
                            contentType = list.ContentTypes[contentTypeName];
                        }
                        catch (Exception ex)
                        {
                            SPDiagnosticsService.Local.WriteTrace(0,
                                new SPDiagnosticsCategory("Erro a obter content type da lista",
                                    TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message,
                                ex.StackTrace);
                        }

                        if (contentType == null) return;
                        var fieldsArrayList = new ArrayList(contentType.Fields.Count);
                        for (var i = 0; i < contentType.Fields.Count; i++)
                        {
                            var field = contentType.Fields[i];
                            if (includeTitleField)
                            {
                                if (!"|ContentType|".Contains(field.StaticName))
                                {
                                    fieldsArrayList.Add(field);
                                }
                            }
                            else if (!"|ContentType|Title|".Contains(field.StaticName))
                            {
                                fieldsArrayList.Add(field);
                            }
                        }

                        //Delete all existing fields on the view
                        var view = list.Views[viewName];
                        if (view != null && view.ViewFields.Count > 0)
                            view.ViewFields.DeleteAll();
                        if (view != null) view.Update();

                        AddColumnsToView(webUri, listName, viewName, fieldsArrayList);
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0,
                    new SPDiagnosticsCategory("Erro ao adicionar content type à lista",
                        TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message,
                    ex.StackTrace);
            }
        }

        /// <summary>
        /// Adds the content type fields to a view.
        /// </summary>
        /// <param name="webUri">The web URI.</param>
        /// <param name="listName">Name of the list.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="contentTypeName">Name of the content type.</param>
        /// <param name="includeTitleField"></param>
        public static void AddContentTypeFieldsToViewDocLib(string webUri, string listName,
            string viewName, string contentTypeName, bool includeTitleField)
        {
            try
            {
                using (var site = new SPSite(webUri))
                {
                    using (var web = site.OpenWeb())
                    {
                        var list = web.Lists.TryGetList(listName);
                        //var list = (SPDocumentLibrary)web.Lists[listName];

                        if (list == null) return;
                        SPContentType contentType = null;
                        try
                        {
                            contentType = list.ContentTypes[contentTypeName];
                        }
                        catch (Exception ex)
                        {
                            SPDiagnosticsService.Local.WriteTrace(0,
                                new SPDiagnosticsCategory("Erro in AddContentTypeFieldsToViewDocLib",
                                    TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message,
                                ex.StackTrace);
                        }

                        if (contentType == null) return;
                        var fieldsArrayList = new ArrayList(contentType.Fields.Count);
                        for (var i = 0; i < contentType.Fields.Count; i++)
                        {
                            var field = contentType.Fields[i];
                            if (includeTitleField)
                            {
                                if (!"|ContentType|".Contains(field.StaticName))
                                {
                                    fieldsArrayList.Add(field);
                                }
                            }
                            else if (!"|ContentType|Title|".Contains(field.StaticName))
                            {
                                fieldsArrayList.Add(field);
                            }
                        }

                        //Delete all existing fields on the view
                        var view = list.Views[viewName];
                        if (view != null && view.ViewFields.Count > 0)
                            view.ViewFields.DeleteAll();
                        if (view != null) view.Update();

                        AddColumnsToViewDocLib(webUri, listName, viewName, fieldsArrayList);
                    }
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0,
                    new SPDiagnosticsCategory("Erro in AddContentTypeFieldsToViewDocLib",
                        TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message,
                    ex.StackTrace);
            }
        }

        /// <summary>
        /// Adds the columns to view.
        /// </summary>
        /// <param name="webUri">The web URI.</param>
        /// <param name="listName">Name of the list.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="fieldsArrayList">The fields array list.</param>
        public static void AddColumnsToView(string webUri, string listName, string viewName, ArrayList fieldsArrayList)
        {
            try
            {
                using (var site = new SPSite(webUri))
                {
                    site.AllowUnsafeUpdates = true;
                    using (var web = site.OpenWeb())
                    {
                        web.AllowUnsafeUpdates = true;
                        var list = web.Lists.TryGetList(listName);
                        //var list = web.GetList(string.Format("{0}/Lists/{1}/AllItems.aspx", web.Url, listName));

                        if (fieldsArrayList.Count == 0)
                        {
                            return;
                        }

                        if (list != null)
                        {
                            var view = list.Views[viewName];
                            if (view != null)
                            {
                                if (!view.Hidden && view.Title != "Explorer View")
                                {
                                    foreach (var column in fieldsArrayList.Cast<SPField>().Where(column =>
                                        !view.ViewFields.Exists(column.StaticName)))
                                    {
                                        view.ViewFields.Add(column);
                                    }
                                }
                            }

                            if (view != null) view.Update();
                            list.Update();
                        }
                        web.AllowUnsafeUpdates = false;
                    }
                    site.AllowUnsafeUpdates = false;
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Erro in AddColumnsToView",
                    TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Adds fields to a specific view.
        /// </summary>
        /// <param name="webUri">Represents the SPWeb uri.</param>
        /// <param name="listName">Represents the SPList name.</param>
        /// <param name="viewName">Represents the SPView name.</param>
        /// <param name="columnNames">Represents the SPFields to add to the SPView.</param>
        public static void AddColumnsToView(string webUri, string listName, string viewName, string[] columnNames)
        {
            try
            {
                using (var site = new SPSite(webUri))
                {
                    site.AllowUnsafeUpdates = true;
                    using (var web = site.OpenWeb())
                    {
                        web.AllowUnsafeUpdates = true;
                        var list = web.Lists.TryGetList(listName);
                        //var list = web.GetList(string.Format("{0}/Lists/{1}/AllItems.aspx", web.Url, listName));

                        if (columnNames.Length == 0)
                            return;

                        if (list != null)
                        {
                            var view = list.Views[viewName];

                            if (view != null)
                            {
                                if (!view.Hidden && view.Title != "Explorer View")
                                {
                                    foreach (var column in columnNames.Where(column =>
                                        !string.IsNullOrEmpty(column)).Where(column => !view.ViewFields.Exists(column)))
                                    {
                                        view.ViewFields.Add(column);
                                    }
                                }
                            }

                            if (view != null) view.Update();
                            list.Update();
                        }
                        web.AllowUnsafeUpdates = false;
                    }
                    site.AllowUnsafeUpdates = false;
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Erro in AddColumnsToView",
                    TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Adds the columns to view.
        /// </summary>
        /// <param name="webUri">The web URI.</param>
        /// <param name="listName">Name of the list.</param>
        /// <param name="viewName">Name of the view.</param>
        /// <param name="fieldsArrayList">The fields array list.</param>
        public static void AddColumnsToViewDocLib(string webUri, string listName, string viewName,
            ArrayList fieldsArrayList)
        {
            try
            {
                using (var site = new SPSite(webUri))
                {
                    site.AllowUnsafeUpdates = true;
                    using (var web = site.OpenWeb())
                    {
                        web.AllowUnsafeUpdates = true;
                        var list = (SPDocumentLibrary) web.Lists[listName];

                        if (fieldsArrayList.Count == 0)
                        {
                            return;
                        }

                        if (list != null)
                        {
                            var view = list.Views[viewName];
                            if (view != null)
                            {
                                if (!view.Hidden && view.Title != "Explorer View")
                                {
                                    foreach (var column in fieldsArrayList.Cast<SPField>().Where(column =>
                                        !view.ViewFields.Exists(column.StaticName)))
                                    {
                                        view.ViewFields.Add(column);
                                    }
                                }
                            }

                            if (view != null) view.Update();
                            list.Update();
                        }
                        web.AllowUnsafeUpdates = false;
                    }
                    site.AllowUnsafeUpdates = false;
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Erro in AddColumnsToViewDocLib",
                    TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        /// <summary>
        /// Adds fields to a specific view.
        /// </summary>
        /// <param name="webUri">Represents the SPWeb uri.</param>
        /// <param name="listName">Represents the SPList name.</param>
        /// <param name="viewName">Represents the SPView name.</param>
        /// <param name="columnNames">Represents the SPFields to add to the SPView.</param>
        public static void AddColumnsToViewDocLib(string webUri, string listName, string viewName, string[] columnNames)
        {
            try
            {
                using (var site = new SPSite(webUri))
                {
                    site.AllowUnsafeUpdates = true;
                    using (var web = site.OpenWeb())
                    {
                        web.AllowUnsafeUpdates = true;
                        var list = (SPDocumentLibrary) web.Lists[listName];

                        if (columnNames.Length == 0)
                            return;

                        if (list != null)
                        {
                            var view = list.Views[viewName];

                            if (view != null)
                            {
                                if (!view.Hidden && view.Title != "Explorer View")
                                {
                                    foreach (var column in columnNames.Where(column =>
                                        !string.IsNullOrEmpty(column)).Where(column => !view.ViewFields.Exists(column)))
                                    {
                                        view.ViewFields.Add(column);
                                    }
                                }
                            }

                            try
                            {
                                if (view != null)
                                {
                                    view.ViewFields.MoveFieldTo("DocIcon", 0);
                                    view.ViewFields.MoveFieldTo("Title", 1);
                                    view.ViewFields.MoveFieldTo("BaseSynopsis", 2);
                                }
                            }
                            catch (Exception exViewSort)
                            {
                                SPDiagnosticsService.Local.WriteTrace(0,
                                    new SPDiagnosticsCategory("Erro in AddColumnsToViewDocLib",
                                        TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected,
                                    exViewSort.Message, exViewSort.StackTrace);
                            }

                            if (view != null) view.Update();
                            list.Update();
                        }
                        web.AllowUnsafeUpdates = false;
                    }
                    site.AllowUnsafeUpdates = false;
                }
            }
            catch (Exception ex)
            {
                SPDiagnosticsService.Local.WriteTrace(0, new SPDiagnosticsCategory("Erro in AddColumnsToViewDocLib",
                    TraceSeverity.Unexpected, EventSeverity.Error), TraceSeverity.Unexpected, ex.Message, ex.StackTrace);
            }
        }

        public static bool UploadFileToDocLibray(string site, string fileToUpload,
            string library, string folder, bool replace)
        {
            //String fileToUpload = @"C:\YourFile.txt";
            //String sharePointSite = "http://yoursite.com/sites/Research/";
            //String documentLibraryName = "Shared Documents";

            using (var oSite = new SPSite(site))
            {
                var replaceExistingFiles = replace;
                using (var oWeb = oSite.OpenWeb())
                {
                    if (!File.Exists(fileToUpload))
                        throw new FileNotFoundException("File not found.", fileToUpload);

                    var myLibrary = oWeb.Folders[library + "/" + folder];

                    // Prepare to upload
                    var fileName = Path.GetFileName(fileToUpload);
                    var fileStream = File.OpenRead(fileToUpload);

                    // Upload document
                    myLibrary.Files.Add(fileName, fileStream, replaceExistingFiles);

                    // Commit 
                    myLibrary.Update();
                    return true;
                }
            }
        }

        public static bool CreateFolders(SPList list, string year)
        {
            var folder = list.Items.Add(list.RootFolder.ServerRelativeUrl, SPFileSystemObjectType.Folder, null);

            folder["Title"] = year + DateTime.Now.Month.ToString().PadLeft(2, '0');
            folder.Update();
            list.Update();
            return true;
        }
    }
}


   
