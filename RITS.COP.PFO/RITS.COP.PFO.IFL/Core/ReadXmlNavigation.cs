﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.SharePoint;
using RITS.COP.PFO.BL.BusinessEntities;

namespace RITS.COP.PFO.IFL.Core
{
    public static class ReadXmlNavigation
    {
        public static DataTable ReadXml(string path)
        {
            var dt = new DataTable();

            dt.Columns.AddRange(new[] { 
                new DataColumn("Key",typeof(string)),
                new DataColumn("Value", typeof(string)), 
                new DataColumn("Menu", typeof(string)), 
                new DataColumn("Link", typeof(string)), 
                new DataColumn("Visible",typeof(string)),
                new DataColumn("Enable", typeof (string)),
                new DataColumn("Title",typeof(string))});


            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                Bottom btn;

                XmlNodeList xmlnode;
                int i = 0;
                int z = 0;
                string str = null;

                // Supply the credentials necessary to access the DTD file stored on the network.
                XmlUrlResolver resolver = new XmlUrlResolver();
                resolver.Credentials = CredentialCache.DefaultCredentials;

                // Create and load the XmlDocument.
                XmlDocument xmldoc = new XmlDocument();
                xmldoc.XmlResolver = resolver;  // Set the resolver.
               

                xmldoc.Load(path);
                
                xmlnode = xmldoc.GetElementsByTagName("Item");
                for (i = 0; i <= xmlnode.Count - 1; i++)
                {
                    btn = new Bottom();
                    for (z = 0; z <= xmlnode[i].ChildNodes.Count - 1; z++)
                    {
                        if (xmlnode[i].ChildNodes.Item(z).Name == "cop_pfo_key")
                            btn.Key = xmlnode[i].ChildNodes.Item(z).InnerText.Trim();
                        if (xmlnode[i].ChildNodes.Item(z).Name == "cop_pfo_value")
                            btn.Value = xmlnode[i].ChildNodes.Item(z).InnerText.Trim();
                        if (xmlnode[i].ChildNodes.Item(z).Name == "cop_pfo_menu")
                            btn.Menu = xmlnode[i].ChildNodes.Item(z).InnerText.Trim();
                        if (xmlnode[i].ChildNodes.Item(z).Name == "cop_pfo_link")
                            btn.Link = xmlnode[i].ChildNodes.Item(z).InnerText.Trim();
                        if (xmlnode[i].ChildNodes.Item(z).Name == "cop_pfo_visible")
                            btn.Visible = xmlnode[i].ChildNodes.Item(z).InnerText.Trim();
                        if (xmlnode[i].ChildNodes.Item(z).Name == "cop_pfo_enable")
                            btn.Enable = xmlnode[i].ChildNodes.Item(z).InnerText.Trim();
                        if (xmlnode[i].ChildNodes.Item(z).Name == "title")
                            btn.Title = xmlnode[i].ChildNodes.Item(z).InnerText.Trim();
                    }



                    dt.Rows.Add(
                        btn.Key ?? string.Empty,
                        btn.Value ?? string.Empty,
                        btn.Menu ?? string.Empty,
                        btn.Link ?? string.Empty,
                        btn.Visible ?? string.Empty,
                        btn.Enable ?? string.Empty ,
                        btn.Title ?? string.Empty);

                }

            });


            return dt;
        }
    }
}


