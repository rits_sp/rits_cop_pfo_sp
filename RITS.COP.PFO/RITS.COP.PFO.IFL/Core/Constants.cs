﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;

namespace RITS.COP.PFO.IFL.Core
{

    public static class Constants
    {
        public static class UIMessages
        {

        }

        public static partial class MailChimpConfigurations
        {
            public static string apiKey = "cc03262066a3c27808d279643695f0a1-us1";
            public static string listId = "ceafe36015"; 
        }

        public static partial class Areas
        {
            public static string FormacaoOlimpica = "Formacao Olimpica";
            public static string Highlights = "Destaques";
            public static string Formations = "Formacoes";
            public static string Publications = "Publicacoes";
            public static string COPAwards = "PremiosCOP";
        }

        public static partial class Urls
        {
            public static string _formacao = SPContext.Current.Site.RootWeb.Url;
           // public static string _Desafios = _program + "/" + Areas.Activities;

        }

        public static partial class UrlsPages
        {
            static string _program = SPContext.Current.Site.RootWeb.Url;
            public static string _pageError = "/Pages/404.aspx";
            public static string _pageHomePage = "/Pages/HomePage.aspx";
            public static string _pageLoginAdmin = "/Pages/LoginAdmin.aspx";
            public static string _pageSearchResults = "/Pages/ResultadosPesquisa.aspx";
        }

        public static partial class Grupos
        {
     
        }

        public static partial class Lists
        {
            public static string _cop_pfo_Configurations = "COP_PFO_Configuracoes";
            public static string _cop_pfo_Navigation = "COP_PFO_Navegacao";
            public static string _cop_pfo_Faqs = "COP_PFO_FAQS";
            public static string _cop_pfo_Text = "COP_PFO_Texto";
            public static string _cop_pfo_Imags = "COP_PFO_Imagens";
            public static string _cop_pfo_Contacts = "COP_PFO_Contactos";
            public static string _cop_pfo_Documents = "COP_PFO_Documentos";
            public static string _cop_pfo_Newsletter = "COP_PFO_NewsLetter";
            public static string _cop_pfo_Parceiros = "COP_PFO_Parceiros";
        }

        public static partial class Menus
        {
            public static string _menu_Main = "Menu Main";
            public static string _menu_Header_Top = "Menu Header Top";
            public static string _menu_Social = "Menu Social";
            public static string _menu_Footer = "Menu Footer";
            public static string _menu_Footer_Bottom = "Menu Footer Bottom";
        }

        public static partial class PagesName
        {
            public static string _page_pfo_Home = "Home Page";
            public static string _page_pfo_SchoolMaps = "Mapa Escolas";
            public static string _page_pfo_Contacts = "Contactos";
            public static string _page_pfo_FAQS = "FAQS";
            public static string _page_pfo_Notifications = "Notificacoes";
            public static string _page_pfo_Activities = "Atividades";
            public static string _page_pfo_createActivities = "Adicionar Atividades";
            public static string _page_pfo_resumeActivities = "Resumo Atividades";
            public static string _page_pfo_resources = "Conteudos";
        }


        public static partial class EncriptDefinition
        {
            public static string KEY_ENCRIPT_DECRIPT = "Pw.CoP/2o16";
        }
    }
}

