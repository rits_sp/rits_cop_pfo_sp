﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;
using RITS.COP.PFO.BL.BusinessEntities;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.IFL.Core
{

    public class LoadObjects
    {

        public static String Text { get; set; }

        public static DataTable LoadMenu(string url, string siteList, string menu, uint rowlimit)
        {
            var dt = new DataTable();
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                SPListItemCollection items = null;


                dt.Columns.AddRange(new[]
                {
                    new DataColumn("Key", typeof (string)),
                    new DataColumn("Value", typeof (string)),
                    new DataColumn("Link", typeof (string)),
                    new DataColumn("Visible", typeof (bool)),
                    new DataColumn("Enable", typeof (bool))
                });

                using (var site = new SPSite(url))
                using (var web = site.OpenWeb())
                {
                    var list = web.Lists.TryGetList(siteList);
                    if (list != null && list.ItemCount > 0)
                    {
                        var query = string.Format(@"<Where>
                                                     <Eq>
                                                      <FieldRef Name='cop_pfo_menu'></FieldRef>
                                                      <Value Type='Choice'>{0}</Value>
                                                     </Eq>
                                                 </Where>", menu);

                        string viewfields = @"<FieldRef Name='cop_pfo_key' />
                                           <FieldRef Name='cop_pfo_value' />
                                           <FieldRef Name='cop_pfo_menu' />
                                           <FieldRef Name='cop_pfo_link' />
                                           <FieldRef Name='cop_pfo_visible' />
                                           <FieldRef Name='cop_pfo_enable' />";

                        var spquery = new SPQuery
                        {
                            Query = query,
                            ViewFields = viewfields,
                            RowLimit = rowlimit
                        };

                        items = list.GetItems(spquery);
                    }
                }

                if (items != null)
                {


                    foreach (SPListItem item in items)
                        try
                        {
                            var link = item[item.Fields.GetFieldByInternalName("cop_pfo_link").Id] as String ?? string.Empty;

                            link = link.Contains(",") ? link.Split(',').FirstOrDefault() : link;

                            dt.Rows.Add(
                                item[item.Fields.GetFieldByInternalName("cop_pfo_key").Id] ?? string.Empty,
                                item[item.Fields.GetFieldByInternalName("cop_pfo_value").Id] ?? string.Empty,
                                link,
                                item[item.Fields.GetFieldByInternalName("cop_pfo_visible").Id] ?? string.Empty,
                                item[item.Fields.GetFieldByInternalName("cop_pfo_enable").Id] ?? string.Empty);
                        }
                        catch (Exception ex)
                        {
                            ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "", "", ex.Message);
                        }
                }
            });

            return dt;
        }

        public static BottomLink GetParameter(string parameter, DataTable dt)
        {
            var headerBottomLink = new BottomLink();

            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                var r = dt.AsEnumerable().FirstOrDefault(x => x.Field<string>("Key") == parameter);

                if (r != null)
                {
                    headerBottomLink.Value = r.Field<string>("Value");
                    headerBottomLink.Link = r.Field<string>("Link") ?? string.Empty;
                    headerBottomLink.Enable = r.Field<bool>("Enable").ToString();
                    headerBottomLink.Visible = r.Field<bool>("Visible").ToString();
                }
            });
            return headerBottomLink;

        }

        public static DataTable LoadFaqs(string url, string siteList, uint rowlimit)
        {
            var dt = new DataTable();
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                try
                {

                    SPListItemCollection items = null;

                    dt.Columns.AddRange(new[]
            {
                new DataColumn("ID", typeof (string)),
                new DataColumn("Question", typeof (string)),
                new DataColumn("Answer", typeof (string))
            });

                    using (var site = new SPSite(url))
                    using (var web = site.OpenWeb())
                    {
                        var list = web.Lists.TryGetList(siteList);
                        if (list != null && list.ItemCount > 0)
                        {
                            var query = "<OrderBy><FieldRef Name=\'Order\' /></OrderBy>";

                            string viewfields = @" <FieldRef Name='cop_pfo_answer' />
                                           <FieldRef Name='cop_pfo_question' />";

                            var spquery = new SPQuery
                            {
                                Query = query,
                                ViewFields = viewfields,
                                RowLimit = rowlimit
                            };

                            items = list.GetItems(spquery);
                        }
                    }
                    int count = 0;
                    if (items != null && items.Count > 0)
                    {
                        foreach (SPListItem item in items)
                        {
                            ++count;
                            dt.Rows.Add(
                                count.ToString(),
                                item[item.Fields.GetFieldByInternalName("cop_pfo_question").Id].ToString()
                                    .Replace("<p>", "")
                                    .Replace("</p>", "")
                                     ?? string.Empty,
                                item[item.Fields.GetFieldByInternalName("cop_pfo_answer").Id].ToString()
                                    .Replace("<p>", "")
                                    .Replace("</p>", "")
                                     ?? string.Empty);
                        }
                    }
                }

                catch (Exception ex)
                {
                    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "", "", ex.Message);
                }
            });

            return dt;
        }

        public static DataTable LoadAreaParceiros(string url, string siteList, string partnerType, uint rowlimit)
        {
            //partnerType - I/ L / P
            var tpartner = string.Empty;
            var dt = new DataTable();



            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                try
                {
                    switch (partnerType)
                    {
                        case "P":
                            tpartner = "Parceiros";
                            break;
                        case "I":
                            tpartner = "Instituições";
                            break;
                        case "L":
                            tpartner = "Laboratórios";
                            break;
                    }

                    SPListItemCollection items = null;

                    dt.Columns.AddRange(new[]
                    {
                        new DataColumn("Name", typeof (string)),
                        new DataColumn("LinkUrl", typeof (string)),
                        new DataColumn("LinkImage", typeof (string)),
                        new DataColumn("Visible", typeof (bool))

                    });

                    using (var site = new SPSite(url))
                    using (var web = site.OpenWeb())
                    {
                        var list = web.Lists.TryGetList(siteList);
                        if (list != null && list.ItemCount > 0)
                        {
                            var query =
                                string.Format(
                                    "<Where><Eq><FieldRef Name='cop_pfo_partnerType' /><Value Type='MultiChoice'>{0}</Value></Eq></Where>",
                                    tpartner);

                            string viewfields =
                                                @"<FieldRef Name='cop_pfo_name' />
                                                  <FieldRef Name='cop_pfo_partnerType' />
                                                  <FieldRef Name='cop_pfo_link' />
                                                  <FieldRef Name='FileRef' />
                                                  <FieldRef Name='cop_pfo_visible' />
                                             ";

                            var spquery = new SPQuery
                            {
                                Query = query,
                                ViewFields = viewfields,
                                RowLimit = rowlimit
                            };

                            items = list.GetItems(spquery);
                        }
                    }

                    if (items != null && items.Count > 0)
                    {
                        foreach (SPListItem item in items)
                        {
                            dt.Rows.Add(
                                item[item.Fields.GetFieldByInternalName("cop_pfo_name").Id]  ?? string.Empty,
                                item[item.Fields.GetFieldByInternalName("cop_pfo_link").Id]  ?? string.Empty,
                                Constants.Urls._formacao + item[item.Fields.GetFieldByInternalName("FileRef").Id]  ?? string.Empty,
                                item[item.Fields.GetFieldByInternalName("cop_pfo_visible").Id] ?? false);
                        }
                    }
                }
                catch (Exception ex)
                {
                    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "", "", ex.Message);
                }
            });

            return dt;
        }


        public static String LoadTextPage(string url, string siteList, string key, uint rowlimit)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                try
                {
                    SPListItemCollection items = null;

                    Text = string.Empty;
                    using (var site = new SPSite(url))
                    using (var web = site.OpenWeb())
                    {
                        var list = web.Lists.TryGetList(siteList);
                        if (list != null && list.ItemCount > 0)
                        {
                            var query = string.Format(@"<Where>
                                                     <Eq>
                                                      <FieldRef Name='cop_pfo_key'></FieldRef>
                                                      <Value Type='Text'>{0}</Value>
                                                     </Eq>
                                                 </Where>", key);

                            string viewfields = @"<FieldRef Name='cop_pfo_richText' />";

                            var spquery = new SPQuery
                            {
                                Query = query,
                                ViewFields = viewfields,
                                RowLimit = rowlimit
                            };

                            items = list.GetItems(spquery);
                        }
                    }

                    if (items != null)
                    {
                        foreach (SPListItem item in items)
                        {
                            Text = item[items.Fields.GetFieldByInternalName("cop_pfo_richText").Id].ToString() ??
                                   string.Empty;
                        }
                    }
                }
                catch (Exception ex)
                {
                    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "", "", "");
                }
            });
            return Text ?? string.Empty;
        }

        public static String LoadConfiguration(string url, string siteList, string key, uint rowlimit)
        {
            string value = string.Empty;

            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                try
                {
                    SPListItemCollection items = null;

                    using (var site = new SPSite(url))
                    using (var web = site.OpenWeb())
                    {
                        var list = web.Lists.TryGetList(siteList);
                        if (list != null && list.ItemCount > 0)
                        {
                            var query = string.Format(@"<Where>
                                                     <Eq>
                                                      <FieldRef Name='cop_pfo_key'></FieldRef>
                                                      <Value Type='Text'>{0}</Value>
                                                     </Eq>
                                                 </Where>", key);

                            string viewfields = @"<FieldRef Name='cop_pfo_value' />";

                            var spquery = new SPQuery
                            {
                                Query = query,
                                ViewFields = viewfields,
                                RowLimit = rowlimit
                            };

                            items = list.GetItems(spquery);
                        }
                    }

                    if (items != null && items.Count > 0)
                    {
                        foreach (SPListItem item in items)
                        {
                            value = item[items.Fields.GetFieldByInternalName("cop_pfo_value").Id].ToString();

                        }
                    }
                }
                catch (Exception ex)
                {
                    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "", "", "");

                }
            });


            return value ?? "False";
        }

        public static DataTable GetDocuments(string url, string siteList, string keyDocument, uint rowlimit)
        {

            var dt = new DataTable();
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                try
                {

                    SPListItemCollection items = null;
                    dt.Columns.AddRange(new[]
                    {
                        new DataColumn("Link", typeof (string)),
                        new DataColumn("Title", typeof (string)),

                    });


                    using (var site = new SPSite(url))
                    using (var web = site.OpenWeb())
                    {
                        var list = web.Lists.TryGetList(siteList);
                        if (list != null && list.ItemCount > 0)
                        {
                            var query = string.Format(@"<Where>
                                                     <And>
                                                     <Eq>
                                                      <FieldRef Name='cop_pfo_key'></FieldRef>
                                                      <Value Type='Text'>{0}</Value>
                                                     </Eq>
                                                      <Eq>
                                                          <FieldRef Name='cop_pfo_visible'></FieldRef>
                                                          <Value Type='Boolean'>1</Value>
                                                     </Eq>
                                                    </And>
                                                 </Where>", keyDocument);


                            string viewfields = @"<FieldRef Name='FileRef' />
                                              <FieldRef Name='cop_title' />";


                            var spquery = new SPQuery
                            {
                                Query = query,
                                ViewFields = viewfields,
                                RowLimit = rowlimit,
                                ViewAttributes = "Scope=\"Recursive\""
                            };

                            items = list.GetItems(spquery);
                        }
                    }

                    if (items != null || items.Count > 0)
                        foreach (SPListItem item in items)
                        {
                            dt.Rows.Add(

                                item[item.Fields.GetFieldByInternalName("FileRef").Id] != null
                                    ? item[item.Fields.GetFieldByInternalName("FileRef").Id].ToString()
                                    : string.Empty,


                                item[item.Fields.GetFieldByInternalName("cop_title").Id] != null
                                    ? item[item.Fields.GetFieldByInternalName("cop_title").Id].ToString()
                                    : string.Empty);
                        }

                }

                catch (Exception ex)
                {
                    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "002", "IFL.Core.loadobjects.GetDocuments",
                        ex.Message);
                }
            });

            return dt;
        }

        public static int GetContactsCount(string url, string siteList, bool isRevied)
        {
            int count = 0; 
            SPSecurity.RunWithElevatedPrivileges(delegate
            {
                try
                {
                    SPListItemCollection items = null;

                    using (var site = new SPSite(url))
                    using (var web = site.OpenWeb())
                    {
                        var list = web.Lists.TryGetList(siteList);
                        if (list != null && list.ItemCount > 0)
                        {
                            var query = string.Format(@"<Where>
                                                     <Eq>
                                                      <FieldRef Name='cop_pfo_read'></FieldRef>
                                                      <Value Type='Boolean'>{0}</Value>
                                                     </Eq>
                                                 </Where>", isRevied);

                            string viewfields = @" <FieldRef Name='cop_pfo_key' />";

                            var spquery = new SPQuery
                            {
                                Query = query,
                                ViewFields = viewfields
                            };

                            items = list.GetItems(spquery);
                            count = items.Count;
                        }
                    }
                }

                catch (Exception ex)
                {
                    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "", "", ex.Message);
                }
            });

            return count;
        }
    }
}
