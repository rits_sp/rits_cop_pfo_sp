﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using Microsoft.SharePoint;

namespace RITS.COP.PFO.IFL.Core
{
    public static class TagsSEO
    {
        public static void ApplyTagsJS(string title, string keywords, string description, string author, ClientScriptManager csx, System.Type tp)
        {
            String csname2 = "isTagFunction";
            Type cstype2 = tp;

            if (!csx.IsStartupScriptRegistered(cstype2, csname2))
            {
                StringBuilder cstext2 = new StringBuilder();
                cstext2.Append("<script type=text/javascript>");
                if (!string.IsNullOrEmpty(keywords))
                {
                    cstext2.Append("$('meta[name=keywords]').remove();");
                    cstext2.AppendFormat("$('head').append( '<meta name=\"keywords\" content=\"{0}\">');", keywords);
                }
                if (!string.IsNullOrEmpty(description))
                {
                    cstext2.Append("$('meta[name=description]').remove();");
                    cstext2.AppendFormat("$('head').append( '<meta name=\"description\" content=\"{0}\">' );", description);
                }
                if (!string.IsNullOrEmpty(title))
                {
                    cstext2.AppendFormat("document.title = \"{0}\"; </script>", title);
                }
                //if (!string.IsNullOrEmpty(author))
                //{
                //    cstext2.Append("$('meta[name=author]').remove();");
                //    cstext2.AppendFormat("$('head').append( '<meta name=\"author\" content=\"{0}\">' );",author);
                //}
                csx.RegisterStartupScript(cstype2, csname2, cstext2.ToString());
            }
        }

        public static void ApplyTagsCSharp(string xtitle, string xkeywords, string xdescription, Page page)
        {
            page.Title = xtitle;
            ContentPlaceHolder contentPlaceHolder = (ContentPlaceHolder)page.Master.FindControl("PlaceHolderPageTitle");
            contentPlaceHolder.SetRenderMethodDelegate(delegate(System.Web.UI.HtmlTextWriter writer,
            System.Web.UI.Control Container)
            {
                writer.Write(xtitle);
            });


            ////Add Keywords Meta Tag
            HtmlMeta keywords = new HtmlMeta();
            keywords.HttpEquiv = "keywords";
            keywords.Name = "keywords";
            keywords.Content = xkeywords;
            page.Header.Controls.Add(keywords);

            //Add Description Meta Tag
            HtmlMeta description = new HtmlMeta();
            description.HttpEquiv = "description";
            description.Name = "description";
            description.Content = xdescription;
            page.Header.Controls.Add(description);
        }

        public static void ShareImagesTags(string titleS, string descriptionS, string urlS, string imgUrlS, Page page)
        {
            HtmlMeta tag = new HtmlMeta();
            tag.Attributes.Add("property", "og:title");
            tag.Content = titleS;
            page.Header.Controls.Add(tag);

            HtmlMeta tag1 = new HtmlMeta();
            tag1.Attributes.Add("property", "og:description");
            tag1.Content = descriptionS;
            page.Header.Controls.Add(tag1);

            HtmlMeta tagurl = new HtmlMeta();
            tagurl.Attributes.Add("property", "og:url");
            tagurl.Content = urlS;
            page.Header.Controls.Add(tagurl);

            var url = SPContext.Current.Web.Site.Url ?? string.Empty; 

            HtmlMeta tagimg = new HtmlMeta();
            tagimg.Attributes.Add("property", "og:image");
            tagimg.Content = string.Format("{0}{1}", url, imgUrlS); 
            page.Header.Controls.Add(tagimg);
        }
    }
}
