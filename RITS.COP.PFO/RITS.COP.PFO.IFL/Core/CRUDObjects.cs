﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SharePoint;

namespace RITS.COP.PFO.IFL.Core
{
   public class CRUDObjects
    {

        public static bool SaveNewsletter(SPWeb web, string siteList, string name, string email)
        {
            bool isSaved = false;
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {

                var list = web.Lists.TryGetList(siteList);
                if (list != null)
                {
                    web.AllowUnsafeUpdates = true;
                    SPListItemCollection listItems = list.Items;

                    SPListItem item = listItems.Add();
                    Random r = new Random();
                    int rInt = r.Next(0, 100000000);
                    item["Title"] = rInt.ToString();
                    item[item.Fields.GetFieldByInternalName("cop_pfo_name").Id] = name ?? string.Empty;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_email").Id] = email ?? string.Empty;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_enable").Id] = true;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_subscriptionDate").Id] = DateTime.Now;

                    item.Update();
                    list.Update();
                    web.AllowUnsafeUpdates = false;

                    isSaved = true;
                }
                else
                {
                    isSaved = false;
                }
            });
            return isSaved;
        }

        public static bool SaveContacts(SPWeb web, string siteList, string name, string email, string phone,string entity, string subject, string message)
        {
            bool isSaved = false;
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {

                var list = web.Lists.TryGetList(siteList);
                if (list != null)
                {
                    web.AllowUnsafeUpdates = true;
                    SPListItemCollection listItems = list.Items;

                    SPListItem item = listItems.Add();

                    var r = new Random();
                    int rInt = r.Next(0, 100000000);
                    item["Title"] = rInt.ToString();
                    item[item.Fields.GetFieldByInternalName("cop_pfo_key").Id] = rInt.ToString();
                    item[item.Fields.GetFieldByInternalName("cop_pfo_name").Id] = name ?? string.Empty;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_email").Id] = email ?? string.Empty;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_phone").Id] = phone ?? string.Empty;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_entity").Id] = entity ?? string.Empty;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_subject").Id] = subject ?? string.Empty;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_message").Id] = message ?? string.Empty;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_date").Id] = DateTime.Now;
                    item[item.Fields.GetFieldByInternalName("cop_pfo_read").Id] = false;
                    item.Update();

                    list.Update();
                    web.AllowUnsafeUpdates = false;

                    isSaved = true;
                }
            });

            return isSaved;
        }

    }
}
