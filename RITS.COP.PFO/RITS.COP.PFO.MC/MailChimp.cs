﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RITS.COP.PFO.IFL.Core;
using MailChimp;
using MailChimp.Types;



namespace RITS.COP.PFO.MC
{
  public class MailChimp
    {
   

    public static bool MailChimpRegister(string name, string email)
        {
            bool isRegister = false;
            try
            {
                //string apiKey = IFL.Core.Constants.MailChimpConfigurations.apiKey;
                //string listId = IFL.Core.Constants.MailChimpConfigurations.listId;

                string apiKey = LoadObjects.LoadConfiguration(Constants.Urls._formacao, Constants.Lists._cop_pfo_Configurations, "ApiKey", 2) ?? string.Empty;
                string listId = LoadObjects.LoadConfiguration(Constants.Urls._formacao, Constants.Lists._cop_pfo_Configurations, "ListID", 2) ?? string.Empty;

                if (!string.IsNullOrEmpty(apiKey) && !string.IsNullOrEmpty(listId))
                {
                    var options = new List.SubscribeOptions();
                    options.DoubleOptIn = true;
                    options.EmailType = List.EmailType.Html;
                    options.SendWelcome = false;

                    var mergeText = new List.Merges(email, List.EmailType.Text)
                    {
                        {"NAME", name},
                        {"MC_LANGUAGE", "pt_PT"}

                    };

                    var merges = new List<List.Merges> {mergeText};

                    var mcApi = new MCApi(apiKey, false);
                    var batchSubscribe = mcApi.ListBatchSubscribe(listId, merges, options);

                    //caso de erro
                    if (batchSubscribe.Errors.Count < 1)
                    {
                        isRegister = true;
                    }
                }
            }
            catch (Exception ex)
            {


            }
            return isRegister;
        }
    }
}
