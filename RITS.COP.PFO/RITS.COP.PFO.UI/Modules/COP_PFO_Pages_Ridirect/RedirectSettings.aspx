﻿<%@ Page UICulture="en" Culture="en-US" Language="C#" MasterPageFile="/_catalogs/masterpage/COP_MasterPage.master" Inherits="Microsoft.SharePoint.WebPartPages.WebPartPage,Microsoft.SharePoint,Version=15.0.0.0,Culture=neutral,PublicKeyToken=71e9bce111e9429c" meta:webpartpageexpansion="full" meta:progid="SharePoint.WebPartPage.Document" %>

<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>


<asp:Content ID="PageHead" ContentPlaceHolderID="PlaceHolderAdditionalPageHead" runat="server">
</asp:Content>

<asp:Content ID="Main" ContentPlaceHolderID="PlaceHolderMain" runat="server">
    <script>
        var clientContext;
        var website;

        SP.SOD.executeFunc('sp.js', 'SP.ClientContext', sharePointReady);

        function sharePointReady() {
            clientContext = SP.ClientContext.get_current();
            website = clientContext.get_web();

            clientContext.load(website);
            clientContext.executeQueryAsync(onRequestSucceeded, onRequestFailed);
        }
        function onRequestSucceeded() {
            window.location.href = website.get_url() + "/_layouts/15/viewlsts.aspx";
        }
        function onRequestFailed(sender, args) {
           // window.location.href = website.get_url() + "/_layouts/15/viewlsts.aspx";
        }
    </script>
</asp:Content>

<asp:Content ID="PageTitle" ContentPlaceHolderID="PlaceHolderPageTitle" runat="server">
    Redirect - Formação Olímpica
</asp:Content>

<asp:Content ID="PageTitleInTitleArea" ContentPlaceHolderID="PlaceHolderPageTitleInTitleArea" runat="server">
        Redirect - Formação Olímpica
</asp:Content>

