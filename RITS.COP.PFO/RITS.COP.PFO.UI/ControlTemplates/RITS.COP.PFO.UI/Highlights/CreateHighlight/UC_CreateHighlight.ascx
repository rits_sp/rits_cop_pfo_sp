﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_CreateHighlight.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Highlights.CreateHighlight.UC_CreateHighlight" %>

<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_activities"></asp:HyperLink></li>
                    <li>CreateHighlight</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section class="createstuff">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="row">
                    <h3 class="section-title"><i class="zmdi zmdi-collection-plus"></i>Adicionar Destaque</h3>
                </div>
                <div class="row">
                    <!-- Form -->

                    <div id="add-activity" class="create-content">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="activityName" name="activity_name" required>
                                    <label class="mdl-textfield__label" for="activity_name">Nome da Destaque</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="mdl-textfield mdl-js-textfield">
                                    <textarea class="mdl-textfield__input" type="text" rows="3" id=" activityDescription" name="activity_description" required></textarea>
                                    <label class="mdl-textfield__label description" for="activity_description">Descrição</label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-10">
                                <div class="mdl-textfield mdl-js-textfield">
                                    <div id="content_text" name="content_text" id="activityText" class="mdl-textfield__input richtext textarea"></div>
                                    <label class="richtext mdl-textfield__label" for="content_text">Texto</label>
                                </div>
                                <input id="content_text_counter" name="content_text_counter" class="cancel" type="text" min="1" readonly="readonly" />
                            </div>
                        </div>

                        <div class="col-md-10 foruploads">
                            <div id="form-images" class="mdl-card mdl-shadow--2dp filterbox bg-gray-light form-container" style="display: none;">
                                <div class="row">
                                    <h4 class="section-title"><i class="fa fa-picture-o"></i>Fotos</h4>
                                </div>
                                <div class="row upload-file">
                                    <div class="col-md-10">
                                        <div id="images" class="input_fields_wrap upload">
                                            <div class="row">
                                                <div class="form-element">
                                                    <div class="col-md-6">
                                                        <div class="fileup">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto1" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto1" ID="RegularExpressionValidator33" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto1">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto1" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto1" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator12" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto1" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-clearthis">Limpar</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto2" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto2" ID="RegularExpressionValidator34" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto2">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto2" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto2" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator13" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto2" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto3" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto3" ID="RegularExpressionValidator35" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto3">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto3" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto3" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator14" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto3" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto4" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto4" ID="RegularExpressionValidator36" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto4">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto4" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto4" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator15" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto4" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto5" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto5" ID="RegularExpressionValidator37" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto5">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto5" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto5" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator16" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto5" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto6" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto6" ID="RegularExpressionValidator38" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto6">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto6" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto6" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator17" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto6" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto7" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto7" ID="RegularExpressionValidator39" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto7">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto7" runat="server" ClientIDMode="Static" CssClass="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-green addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto7" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator18" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto7" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto8" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto8" ID="RegularExpressionValidator40" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto8">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto8" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto8" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator19" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto8" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto9" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto9" ID="RegularExpressionValidator41" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto9">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto9" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto9" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator20" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto9" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Foto10" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Foto10" ID="RegularExpressionValidator42" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Foto10">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Foto10" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Foto10" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator21" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                                ControlToValidate="file_Foto10" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored bg-orange bt-addmore">
                                                <i class="zmdi zmdi-plus"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="form-videos" class="mdl-card mdl-shadow--2dp filterbox bg-gray-light form-container" style="display: none;">
                                <div class="row">
                                    <h4 class="section-title"><i class="zmdi zmdi-videocam"></i>Vídeos</h4>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="video" class="input_fields_wrap fields">
                                            <div class="form-element mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                <input class="mdl-textfield__input videoLink" type="text" id="form-videos-video1" name="ytubeurl">
                                                <label class="mdl-textfield__label" for="form-videos-video1">URL do vídeo <small>Youtube</small></label>
                                            </div>
                                            <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored bg-orange add_videos_button">
                                                <i class="zmdi zmdi-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="form-files" class="mdl-card mdl-shadow--2dp filterbox bg-gray-light form-container">
                                <div class="row">
                                    <h4 class="section-title"><i class="fa fa-file-pdf-o"></i>Anexos</h4>
                                </div>
                                <div class="row upload-file">
                                    <div class="col-md-8">
                                        <div id="files" class="input_fields_wrap upload">
                                            <div class="row">
                                                <div class="form-element">
                                                    <div class="col-md-6">
                                                        <div class="fileup">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo1" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo1" ID="RegularExpressionValidator22" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo1">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo1" runat="server" ClientIDMode="Static" CssClass="addFileButton" />

                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo1" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo1" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-clearthis">Limpar</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo2" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo2" ID="RegularExpressionValidator23" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo2">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo2" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo2" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator2" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo2" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo3" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo3" ID="RegularExpressionValidator24" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo3">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo3" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo3" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator3" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo3" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo4" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo4" ID="RegularExpressionValidator25" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo4">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo4" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo4" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator4" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo4" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo5" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo5">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo5" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo5" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo5" ID="RegularExpressionValidator26" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator5" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo5" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo6" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo6" ID="RegularExpressionValidator27" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo6">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo6" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo6" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator6" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo6" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo7" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo7" ID="RegularExpressionValidator28" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo7">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo7" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo7" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator7" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo7" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo8" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo8" ID="RegularExpressionValidator29" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo8">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo8" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo8" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator8" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo8" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo9" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo9" ID="RegularExpressionValidator30" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo9">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo9" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo9" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator9" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo9" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                        <div class="fileup hide">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo10" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo10" ID="RegularExpressionValidator31" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo10">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo10" runat="server" ClientIDMode="Static" CssClass="addFileButton" />
                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo10" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator10" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo10" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-removethis">Remover</div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored bg-orange bt-addmore">
                                                <i class="zmdi zmdi-plus"></i>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-10 foruploads">
                            <div id="form-links" class="mdl-card mdl-shadow--2dp filterbox bg-gray-light form-container">
                                <div class="row">
                                    <h4 class="section-title"><i class="fa fa-external-link"></i>Links Externos</h4>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div id="links" class="input_fields_wrap fields linkG">
                                            <div class="form-element mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                <input class="mdl-textfield__input linksExternos" type="text" id="links1" name="url">
                                                <label class="mdl-textfield__label" for="links1">URL <small>http://</small></label>
                                            </div>
                                            <button class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored bg-orange add_links_button">
                                                <i class="zmdi zmdi-plus"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="actionbuttons">
                        <div class="row">
                            <div class="col-md-8">
                                <div id="success"></div>
                                <asp:Button runat="server" ClientIDMode="Static" ID="btn_SubmeterAtividade" CssClass="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bt-save" OnClick="btn_SubmeterAtividade_OnClick" OnClientClick="return GetAllLinks(); " Text="Concluir" />
                                <button id="cancel" onclick="window.history.back();" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray">Cancelar</button>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="alert fade in">
                                <%--    <p id="demo"></p>--%>
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <asp:Label runat="server" ID="lbl_Info" ClientIDMode="Static"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="col-md-4">
                <div class="row">
                    <div class="mdl-card mdl-shadow--2dp filterbox bg-gray-light">
                        <div class="row">
                            <div class="mdl-card__title">
                                <h2 class="mdl-card__title-text">Imagem de Destaque</h2>
                            </div>
                            <div class="row">
                                <div class="mdl-card__supporting-text">
                                    <div id="main-image" class="input_fields_wrap upload">
                                        <div class="row">
                                            <div class="form-element mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                    <asp:TextBox ID="txt_file_FotoD" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                    <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_FotoD" ID="RegularExpressionValidator32" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                    <label class="mdl-textfield__label" for="txt_file_FotoD">Nome do ficheiro</label>
                                                </div>
                                                <asp:FileUpload ID="file_FotoD" runat="server" ClientIDMode="Static" CssClass="addFileButton" name="file_FotoD" />
                                                <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_FotoD" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator11" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.jpg|.jpeg|.png|.JPG|.JPEG|.PNG)$"
                                                    ControlToValidate="file_FotoD" runat="server" ForeColor="Red" ErrorMessage="Por favor, escolha um formato de imagem correcto (jpg / jpeg / png)"
                                                    Display="Dynamic" CssClass="label error" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="mdl-card mdl-shadow--2dp filterbox bg-gray-light">
                        <div class="row">
                            <div class="mdl-card__title">
                                <h2 class="mdl-card__title-text">Elementos a Inserir</h2>
                            </div>
                            <div class="row">
                                <div class="mdl-card__supporting-text">
                                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-images" onchange="valueChanged()">
                                        <input type="checkbox" id="checkbox-images" class="mdl-checkbox__input elementsinserted" value="Fotos">
                                        <span class="mdl-checkbox__label">Fotos<i class="fa fa-picture-o float-right"></i></span>
                                    </label>
                                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-videos" onchange="valueChanged()">
                                        <input type="checkbox" id="checkbox-videos" class="mdl-checkbox__input elementsinserted" value="Vídeos">
                                        <span class="mdl-checkbox__label">Vídeos<i class="fa fa-video-camera float-right"></i></span>
                                    </label>
                                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-hangouts" onchange="valueChanged()">
                                        <input type="checkbox" id="checkbox-hangouts" class="mdl-checkbox__input elementsinserted" value="Hangouts">
                                        <span class="mdl-checkbox__label">Hangouts<i class="fa icon hangouts-gray float-right"></i></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="mdl-card mdl-shadow--2dp filterbox bg-gray-light">
                        <div class="row">
                            <div class="mdl-card__title">
                                <h2 class="mdl-card__title-text">Notícias</h2>
                            </div>
                            <div class="row">
                                <div class="mdl-card__supporting-text">
                                    <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-noticias" onchange="valueChanged()">
                                        <input type="checkbox" id="checkbox-noticias" class="mdl-checkbox__input elementsinserted" value="Notícias">
                                        <span class="mdl-checkbox__label">Associar como Notícia<i class="fa fa-newspaper-o float-right"></i></span>
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="mdl-card mdl-shadow--2dp filterbox bg-gray-light">
                        <div class="row">
                            <div class="mdl-card__title">
                                <h2 class="mdl-card__title-text">Tags</h2>
                            </div>
                            <div class="row">
                                <div class="mdl-card__supporting-text">
                                    <asp:Repeater ID="rptTagActivities" runat="server">
                                        <ItemTemplate>
                                            <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="checkbox-tag-<%# DataBinder.Eval (Container.DataItem, "Choice") %>" onchange="valueChanged()">
                                                <input type="checkbox" id="checkbox-tag-<%# DataBinder.Eval (Container.DataItem, "Choice") %>" class="mdl-checkbox__input tagsAtividades" value="<%# DataBinder.Eval (Container.DataItem, "Choice") %>">
                                                <span class="mdl-checkbox__label"><%# DataBinder.Eval (Container.DataItem, "Choice") %></span>
                                            </label>
                                        </ItemTemplate>
                                    </asp:Repeater>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <span id="lblError" style="color: red;"></span>
</section>
<link href="../../../../Style%20Library/cop/css/bootstrap3-wysihtml5.min.css" rel="stylesheet" />
<script src="../../../../Style%20Library/cop/js/peo-addactivity.js"></script>
<script src="../../../../Style%20Library/cop/js/bootstrap3-wysihtml5.min.js"></script>
<script src="../../../../Style%20Library/cop/js/bootstrap3-wysihtml5.all.min.js"></script>
<script>
    //add activity form
    $('#aspnetForm').validate({
        debug: false,
        rules: {
            activity_name: {
                minlength: 5,
                maxlength: 50
            },
            activity_description: {
                minlength: 20,
                maxlength: 200
            },
            ytubeurl: {
                //schemes: [/^(?:https?:\/\/)?(?:www\.)?(?:youtu\.be\/|youtube\.com\/(?:embed\/|v\/|watch\?v=|watch\?.+&v=))((\w|-){11})(?:\S+)?$/],
                youtubeurl: true
            },
            url: {
                url: true
            },
            ctl00$PlaceHolderMain$UC_CreateActivities$file_FotoD: {
                required: true    
            }
        },
        messages: {
            content_text_counter: "Ultrapassou o limite de caracteres."
        },
        submitHandler: function (form) {
                form.submit();
        }
    });

    //end of add activity form
    $('.nav').find('.item.highlights').addClass('selected');

    $(document).ready(function () {
        $('footer li a.highlights').addClass('selected');
    });
</script>

<asp:HiddenField runat="server" ID="hdf_linksG" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_linksV" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_linksH" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_linksF" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_linksD" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_Tags" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_fileName" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_fileUrl" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_imageName" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_imageUrl" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_imageDName" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_imageDUrl" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_TextDescription" ClientIDMode="Static" />
<asp:HiddenField runat="server" ID="hdf_Elements" ClientIDMode="Static" />


<script type="text/javascript">

    //Get all the Hyperlinks in the page
    function GetAllLinks() {


        $("#hdf_TextDescription").val($('.wysihtml5-sandbox').html());


        var linksGerais;
        $(".linksExternos").each(function () {
            if (linksGerais === undefined) {
                linksGerais = $(this).val() + ',';
            }
            else {
                linksGerais += $(this).val() + ',';
            }
        });

        $('#hdf_linksG').val(linksGerais);


        //Get Hyperlinks Videos
        var linksVideos;
        $(".videoLink").each(function () {
            if (linksVideos === undefined) {
                linksVideos = $(this).val() + ',';
            }
            else {
                linksVideos += $(this).val() + ',';
            }
        });

        $("#hdf_linksV").val(linksVideos);


        //Get Hyperlinks Hangouts
        var linkHangouts;
        $(".hangoutLink").each(function () {
            if (linkHangouts === undefined) {
                linkHangouts = $(this).val() + ',';
            }
            else {
                linkHangouts += $(this).val() + ',';
            }
        });

        $("#hdf_linksH").val(linkHangouts);

        GetAllTags();

        GetImages();

        GetFiles();

        GetImagDestaque();


    }

    function GetAllTags() {

        var elementsinserted;
        $(".elementsinserted").each(function () {

            if ($(this).parent().hasClass('is-checked')) {
                if (elementsinserted === undefined) {
                    elementsinserted = $(this).val() + ',';
                } else {
                    elementsinserted += $(this).val() + ',';
                }
            }
        });


        $('#hdf_Elements').val(elementsinserted);

        var tags;
        $(".tagsAtividades").each(function () {

            if ($(this).parent().hasClass('is-checked')) {
                if (tags === undefined) {
                    tags = $(this).val() + ',';
                } else {
                    tags += $(this).val() + ',';
                }
            }
        });
        $('#hdf_Tags').val(tags);
    }

    function GetFiles() {

        var fileName;

        $(".fileName").each(function () {
            if (fileName === undefined) {
                fileName = $(this).val() + ',';
            }
            else {
                fileName += $(this).val() + ',';
            }
        });

        $('#hdf_fileName').val(fileName);

        var fileUrl;

        $(".fileUrl").each(function () {
            if (fileUrl === undefined) {
                fileUrl = $(this).val() + ',';
            }
            else {
                fileUrl += $(this).val() + ',';
            }
        });

        $('#hdf_fileUrl').val(fileUrl);

    }

    function GetImages() {

        var imagName;

        $(".imagName").each(function () {
            if (imagName === undefined) {
                imagName = $(this).val() + ',';
            }
            else {
                imagName += $(this).val() + ',';
            }
        });

        $('#hdf_imageName').val(imagName);

        var imagUrl;

        $(".imagUrl").each(function () {
            if (imagUrl === undefined) {
                imagUrl = $(this).val() + ',';
            }
            else {
                imagUrl += $(this).val() + ',';
            }
        });

        $('#hdf_imageUrl').val(imagUrl);




    }

    function GetImagDestaque() {

        $('#hdf_imageDName').val($(".imagDName").val());

        $('#hdf_imageDUrl').val($(".imagDUrl").val());
    }



</script>
