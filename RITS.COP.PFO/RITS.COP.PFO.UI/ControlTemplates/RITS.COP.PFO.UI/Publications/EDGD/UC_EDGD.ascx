﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_EDGD.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Publications.EDGD.UC_EDGD" %>

<link href="../../../../Style%20Library/cop/css/jplist/jplist.core.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.filter-toggle-bundle.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.history-bundle.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.pagination-bundle.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.preloader-control.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.textbox-filter.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.views-control.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.peo.css" rel="stylesheet" />

<section>

    <div class="slider-container">
        <!-- Header Carousel -->
        <!-- START REVOLUTION SLIDER 5.0 -->
        <div class="rev_slider_wrapper">
            <div id="rev_slider_inside" class="rev_slider" data-version="5.0">
                <ul>
                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="../../../../_layouts/15/images/RITS.COP.BR/PHO10432129.retouche.jpg" alt="" width="1920" height="1080" data-bgposition="center center" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption News-Title"
                            data-x="left" data-hoffset="80"
                            data-y="bottom" data-voffset="150"
                            data-whitespace="normal"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="500"
                            data-bgposition="center center">
                            <div class="insidepages">
                                <h1 class="page-header">Publicações</h1>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>
        <!-- END OF SLIDER WRAPPER -->
    </div>
</section>

<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                    <li class="active">LblEconomiaDireitoGestaoDesporto</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container activities">
        <div class="row">
            <div class="col-lg-9 withtoprightmenu">
                <h2>LblTEconomiaDireitoGestaoDesporto</h2>
                <p class="introtext">
                    TxtEconomiaDireitoGestaoDesportoText
                </p>
                <div class="revisors-container">
                    <div class="logos-title">Revisores</div>
                    <ul id="revisors">
                        <li>
                            <div class="revisor">
                                <div class="revisor-image" style="background-image: url(http://www.abbc.pt/xms/dbimages/Colaboradores/Jose_Meirim.JPG);"></div>
                                <div class="revisor-name">José Manuel Meirim</div>
                                <div class="revisor-uni">Universidade Nova de Lisboa</div>
                            </div>
                        </li>
                        <li>
                            <div class="revisor">
                                <div class="revisor-image" style="background-image: url(http://www.abbc.pt/xms/dbimages/Colaboradores/Jose_Meirim.JPG);"></div>
                                <div class="revisor-name">José Manuel Meirim</div>
                                <div class="revisor-uni">Universidade Nova de Lisboa</div>
                            </div>
                        </li>
                        <li>
                            <div class="revisor">
                                <div class="revisor-image" style="background-image: url(http://www.abbc.pt/xms/dbimages/Colaboradores/Jose_Meirim.JPG);"></div>
                                <div class="revisor-name">José Manuel Meirim</div>
                                <div class="revisor-uni">Universidade Nova de Lisboa</div>
                            </div>
                        </li>                                          
                    </ul>
                </div>
            </div>
            <div class="col-lg-3 toprightmenu">
                <div class="mdl-card mdl-shadow--2dp categorybox publications bg-gray-light ">
                    <div class="row">
                        <div class="mdl-card__title toptitle bg-red-dark">
                            <h2 class="mdl-card__title-text">Economia, Direito e Gestão do Desporto</h2>
                            
                            <button id="menu-lower-right" class="mdl-button mdl-js-button mdl-button--icon" onclick="return false">
                              <i class="zmdi zmdi-more-vert"></i>
                            </button>

                            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="menu-lower-right">
                              <li class="mdl-menu__item"><a href="/Pages/RevistaOlimpo.aspx">Revista Olimpo</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/ValorizarSocialmenteDesporto.aspx">Valorizar Socialmente o Desporto</a></li>
                              <li class="mdl-menu__item topubs"><a href="/Pages/PublicacoesPeriodicas.aspx">Publicações Periódicas</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/TreinoDesportivo.aspx">Treino Desportivo</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/PsicologiaPedagogiaDesporto.aspx">Psicologia e Pedagogia do Desporto</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/MedicinaDesporto.aspx">Medicina do Desporto</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/FisiologiaBiomecanicaDesporto.aspx">Fisiologia e Biomecânica do Desporto</a></li>
                              <li class="mdl-menu__item hide"><a href="/Pages/EconomiaDireitoGestaoDesporto.aspx">Economia, Direito e Gestão do Desporto</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/SociologiaHistoriaDesporto.aspx">Sociologia e História do Desporto</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mdl-card__image" style="background-image: url(../../../../../Style%20Library/cop/imgs/IMG_7786.jpg)"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="resources-responsive-panel" class="responsive-item mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="zmdi zmdi-search-for"></i>Filtrar Conteúdos</div>
        <!-- main content -->
        <div class="box">
            <div>
                <!--<><><><><><><><><><><><><><><><><><><><><><><><><><> FILTERING START <><><><><><><><><><><><><><><><><><><><><><><><><><>-->

                <div id="filtering" class="box jplist">
                    <div class="col-md-9">
                        <!-- data -->
                        <div class="list box text-shadow">
                            <!-- item -->
                            <div class="list-item box item-type-list mdl-card mdl-shadow--2dp bg-gray-light">
                                <div class="item-type-list-container">
                                    <div class="mdl-card__title bg-gray-medium Livros"><!-- METER AQUI CLASS DINAMICA PARA OS TIPOS DE PUBLICACOES, EU COM CSS ALTERO DEPOIS OS ICONS: substituir o livros -->
                                            <i class="zmdi zmdi-file-text"></i> <!-- Artigos -->
                                            <i class="zmdi zmdi-book"></i> <!-- Livros -->
                                            <i class="zmdi zmdi-border-color"></i> <!-- Dissertacoes -->
                                            <i class="zmdi zmdi-image"></i> <!-- Imagens -->
                                            <i class="zmdi zmdi-copy"></i> <!-- Periodicos -->
                                            <i class="zmdi zmdi-file"></i> <!-- Outros -->
                                    </div>
                                    <div class="block mdl-card__actions mdl-card--border">
                                        <div class="row item-type-list-top">
                                            <div class="col-md-6">
                                                <h3 class="title">Lorem Ipsum</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="date">01/01/2040 00:00:00</div>
                                            </div>
                                        </div>
                                        <div class="tagshere row item-type-list-bottom">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <i class="fa fa-university" aria-hidden="true"></i><div class="uni-name desc">Universidade LoremIpsum</div>
                                                </div>
                                                <div class="row">
                                                     <i class="fa fa-user" aria-hidden="true"></i><div class="authors-name">Cristiano Ronaldo</div><div class="authors-name">Joaquim Gomes</div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 txt-right">
                                                <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                                    <i class="fa fa-tags"></i>
                                                </a>
                                                <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                </a>
                                                <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                                    <i class="fa fa-link" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div class="theme tags-shower">
                                                <div class="tags-holder">
                                                    <div class="col-md-3">
                                                        <div class="tag-title">Tipo</div>
                                                    </div>
                                                    <div class="col-md-9 tag-holder">
                                                        <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TypeActivities") %>--%>Livros</div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="tag-title">Tags</div>
                                                    </div>
                                                    <div class="col-md-9 tag-holder">
                                                        <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Qualidades Físicas</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <!-- item -->
                            <div class="list-item box item-type-list mdl-card mdl-shadow--2dp bg-gray-light">
                                <div class="item-type-list-container">
                                    <div class="mdl-card__title bg-gray-medium Outros"><!-- METER AQUI CLASS DINAMICA PARA OS TIPOS DE PUBLICACOES, EU COM CSS ALTERO DEPOIS OS ICONS: substituir o livros -->
                                            <i class="zmdi zmdi-file-text"></i> <!-- Artigos -->
                                            <i class="zmdi zmdi-book"></i> <!-- Livros -->
                                            <i class="zmdi zmdi-border-color"></i> <!-- Dissertacoes -->
                                            <i class="zmdi zmdi-image"></i> <!-- Imagens -->
                                            <i class="zmdi zmdi-copy"></i> <!-- Periodicos -->
                                            <i class="zmdi zmdi-file"></i> <!-- Outros -->
                                    </div>
                                    <div class="block mdl-card__actions mdl-card--border">
                                        <div class="row item-type-list-top">
                                            <div class="col-md-6">
                                                <h3 class="title">Achdom Lormpz</h3>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="date">01/02/2020 12:00:00</div>
                                            </div>
                                        </div>
                                        <div class="tagshere row item-type-list-bottom">
                                            <div class="col-md-8">
                                                <div class="row">
                                                    <i class="fa fa-university" aria-hidden="true"></i><div class="uni-name desc">Universidade Nova de Lisboa</div>
                                                </div>
                                                <div class="row">
                                                     <i class="fa fa-user" aria-hidden="true"></i><div class="authors-name">Alberto Ribeiro</div><div class="authors-name">John Rambo</div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 txt-right">
                                                <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                                    <i class="fa fa-tags"></i>
                                                </a>
                                                <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                                    <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                                </a>
                                                <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                                    <i class="fa fa-link" aria-hidden="true"></i>
                                                </a>
                                            </div>
                                            <div class="theme tags-shower">
                                                <div class="tags-holder">
                                                    <div class="col-md-3">
                                                        <div class="tag-title">Tipo</div>
                                                    </div>
                                                    <div class="col-md-9 tag-holder">
                                                        <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TypeActivities") %>--%>Outros</div>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <div class="tag-title">Tags</div>
                                                    </div>
                                                    <div class="col-md-9 tag-holder">
                                                        <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Planeamento e Periodização</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            

                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <!-- end of data -->
                        <!-- panel -->
                        <div class="jplist-panel box panel-bottom">

                            <div
                                class="jplist-label"
                                data-type="{start} - {end} of {all}"
                                data-control-type="pagination-info"
                                data-control-name="paging"
                                data-control-action="paging">
                            </div>

                            <div
                                class="jplist-pagination"
                                data-control-type="pagination"
                                data-control-name="paging"
                                data-control-action="paging"
                                data-items-per-page="9"
                                data-control-animate-to-top="false">
                            </div>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="mdl-card mdl-shadow--2dp filterbox bg-gray-light">
                            <div class="mdl-card__title">
                                <h2 class="mdl-card__title-text">Filtrar Conteúdos</h2>
                            </div>



                            <!-- panel -->
                            <div class="jplist-panel box panel-top">
                                <!-- filter by title -->
                                <div class="text-filter-box">

                                    <i class="zmdi zmdi-search"></i>
                                    <div class="mdl-textfield mdl-js-textfield">
                                        <input data-path=".title,.desc"
                                            type="text"
                                            value=""
                                            data-control-type="textbox"
                                            data-control-name="title-filter desc-filter"
                                            data-control-action="filter" class="mdl-textfield__input" type="text" id="search-filters" placeholder="pesquisar" />
                                    </div>

                                </div>

                                <!-- checkbox text filter -->
                                <div
                                    class=""
                                    data-control-type="checkbox-text-filter"
                                    data-control-action="filter"
                                    data-control-name="keywords"
                                    data-path=".keywords"
                                    data-logic="or">

                                    <div class="filter-type">
                                        <label>Tipo</label>
                                       <%-- <asp:Repeater ID="rptTagActivities" runat="server">
                                            <ItemTemplate>
                                                <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-<%# DataBinder.Eval (Container.DataItem, "ID") %>">
                                                    <input type="checkbox" id="Publications-Tag-<%# DataBinder.Eval (Container.DataItem, "ID") %>" class="mdl-checkbox__input" value="<%# DataBinder.Eval (Container.DataItem, "Choice") %>">
                                                    <span class="mdl-checkbox__label"><%# DataBinder.Eval (Container.DataItem, "Choice") %></span>
                                                </label>
                                            </ItemTemplate>
                                        </asp:Repeater>--%>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Type-Artigos">
                                            <input type="checkbox" id="Publications-Type-Artigos" class="mdl-checkbox__input" value="Artigos">
                                            <span class="mdl-checkbox__label">Artigos</span><i class="zmdi zmdi-file-text"></i> <!-- Artigos -->
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Type-Livros">
                                            <input type="checkbox" id="Publications-Type-Livros" class="mdl-checkbox__input" value="Livros">
                                            <span class="mdl-checkbox__label">Livros</span><i class="zmdi zmdi-book"></i> <!-- Livros -->
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Type-Dissertacoes">
                                            <input type="checkbox" id="Publications-Type-Dissertacoes" class="mdl-checkbox__input" value="Dissertações / Teses">
                                            <span class="mdl-checkbox__label">Dissertações / Teses</span><i class="zmdi zmdi-border-color"></i> <!-- Dissertacoes -->
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Type-Periodicos">
                                            <input type="checkbox" id="Publications-Type-Periodicos" class="mdl-checkbox__input" value="Periódicos">
                                            <span class="mdl-checkbox__label">Periódicos</span><i class="zmdi zmdi-image"></i> <!-- Imagens -->
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Type-Imagens">
                                            <input type="checkbox" id="Publications-Type-Imagens" class="mdl-checkbox__input" value="Imagens / Posters">
                                            <span class="mdl-checkbox__label">Imagens / Posters</span><i class="zmdi zmdi-copy"></i> <!-- Periodicos -->
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Type-Outros">
                                            <input type="checkbox" id="Publications-Type-Outros" class="mdl-checkbox__input" value="Outros">
                                            <span class="mdl-checkbox__label">Outros</span><i class="zmdi zmdi-file"></i> <!-- Outros -->
                                        </label>
                                    </div>
                                    
                                     <div class="filter-type">
                                        <label>Tags</label>
                                       <%-- <asp:Repeater ID="rptTagActivities" runat="server">
                                            <ItemTemplate>
                                                <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-<%# DataBinder.Eval (Container.DataItem, "ID") %>">
                                                    <input type="checkbox" id="Publications-Tag-<%# DataBinder.Eval (Container.DataItem, "ID") %>" class="mdl-checkbox__input" value="<%# DataBinder.Eval (Container.DataItem, "Choice") %>">
                                                    <span class="mdl-checkbox__label"><%# DataBinder.Eval (Container.DataItem, "Choice") %></span>
                                                </label>
                                            </ItemTemplate>
                                        </asp:Repeater>--%>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-EconomiaDesporto">
                                            <input type="checkbox" id="Publications-Tag-EconomiaDesporto" class="mdl-checkbox__input" value="Economia do Desporto">
                                            <span class="mdl-checkbox__label">Economia do Desporto</span>
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-DireitoDesporto">
                                            <input type="checkbox" id="Publications-Tag-DireitoDesporto" class="mdl-checkbox__input" value="Direito do Desporto">
                                            <span class="mdl-checkbox__label">Direito do Desporto</span>
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-PlaneamentoEstrategia">
                                            <input type="checkbox" id="Publications-Tag-PlaneamentoEstrategia" class="mdl-checkbox__input" value="Planeamento e Estratégia">
                                            <span class="mdl-checkbox__label">Planeamento e Estratégia</span>
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-MarketingDesporto">
                                            <input type="checkbox" id="Publications-Tag-MarketingDesporto" class="mdl-checkbox__input" value="Marketing do Desporto">
                                            <span class="mdl-checkbox__label">Marketing do Desporto</span>
                                        </label>
                                    </div>

                                </div>
                                



                                <!-- items per page dropdown -->
                                <div
                                    class="jplist-drop-down"
                                    data-control-type="items-per-page-drop-down"
                                    data-control-name="paging"
                                    data-control-action="paging">

                                    <ul>
                                        <li><span data-number="3">3 por página </span></li>
                                        <li><span data-number="6">6 por página </span></li>
                                        <li><span data-number="9" data-default="true">9 por página </span></li>
                                        <li><span data-number="all">Ver todos </span></li>
                                    </ul>
                                </div>

                                <!-- sort dropdown -->
                                <div
                                    class="jplist-drop-down"
                                    data-control-type="sort-drop-down"
                                    data-control-name="sort"
                                    data-control-action="sort"
                                    data-datetime-format="{month}/{day}/{year} {hour}:{min}:{sec}">
                                    <!-- {year}, {month}, {day}, {hour}, {min}, {sec} -->

                                    <ul>
                                        <li><span data-path=".title" data-order="asc" data-type="text">Título A-Z</span></li>
                                        <li><span data-path=".title" data-order="desc" data-type="text">Título Z-A</span></li>
                                        <li><span data-path=".desc" data-order="asc" data-type="text">Universidade A-Z</span></li>
                                        <li><span data-path=".desc" data-order="desc" data-type="text">Universidade Z-A</span></li>
                                        <li><span data-path=".date" data-order="asc" data-type="datetime">Data asc</span></li>
                                        <li><span data-path=".date" data-order="desc" data-type="datetime" data-default="true">Data desc</span></li>
                                    </ul>
                                </div>


                                <!-- views -->
                                <div
                                    class="jplist-views"
                                    data-control-type="views"
                                    data-control-name="views"
                                    data-control-action="views"
                                    data-default="jplist-list-view">


                                <!-- pagination control -->
                                <div
                                    class="jplist-pagination"
                                    data-control-type="pagination"
                                    data-control-name="paging"
                                    data-items-per-page="9"
                                    data-control-action="paging">
                                </div>

                            </div>


                            <div class="box jplist-no-results text-shadow align-center">
                                <p>Sem resultados</p>
                            </div>

                        
            
            <!--<><><><><><><><><><><><><><><><><><><><><><><><><><> FILTERING END <><><><><><><><><><><><><><><><><><><><><><><><><><>-->
                        </div>
                    </div>
                </div>
</section>
<!-- /.container -->
<script src="../../../../Style%20Library/cop/js/jplist/jplist.core.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.sort-bundle.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.textbox-filter.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.pagination-bundle.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.history-bundle.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.filter-toggle-bundle.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.views-control.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.preloader-control.min.js"></script>
<script src="../../../../../Style%20Library/cop/js/publications/peo-publications.js"></script>
<script src="../../../../../Style%20Library/cop/js/peo-tags.js"></script>