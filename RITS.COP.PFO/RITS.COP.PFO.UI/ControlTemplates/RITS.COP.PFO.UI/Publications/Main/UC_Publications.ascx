﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Publications.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Publications.Main.UC_Publications" %>

<div class="slider-container">
    <!-- Header Carousel -->
    <!-- START REVOLUTION SLIDER 5.0 -->
    <div class="rev_slider_wrapper">
        <div id="rev_slider_inside" class="rev_slider" data-version="5.0">
            <ul>
                <li data-transition="fade">
                    <!-- MAIN IMAGE -->
                    <img src="../../../../_layouts/15/images/RITS.COP.BR/PHO10432129.retouche.jpg" alt="" width="1920" height="1080" data-bgposition="center center" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                    <!-- LAYER NR. 1 -->
                    <div class="tp-caption News-Title"
                        data-x="left" data-hoffset="80"
                        data-y="bottom" data-voffset="150"
                        data-whitespace="normal"
                        data-transform_idle="o:1;"
                        data-transform_in="o:0"
                        data-transform_out="o:0"
                        data-start="500"
                        data-bgposition="center center">
                        <div class="insidepages">
                            <h1 class="page-header"><%=LBL_Publicacoes%></h1>
                        </div>
                    </div>
                </li>

            </ul>
        </div>
        <!-- END REVOLUTION SLIDER -->
    </div>
    <!-- END OF SLIDER WRAPPER -->
</div>
</section>

<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                    <li class="active"><%=LBL_Publicacoes%></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container activities">
        <div class="row">
            <div class="col-lg-12">
                <h2><%=LBL_Publicacoes%></h2>
                <p class="introtext">
                    <%=TXT_Text_Publication%>
                </p>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="row publications top">
        <div class="container">
            <div class="col-md-4">
                <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                    <div class="mdl-card__title bg-gray-medium">
                        <h2 class="mdl-card__title-text"><%=LBL_RevistaOlimpo %></h2>
                    </div>
                    <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/slide1.jpg)'></div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a href="/Pages/RevistaOlimpo.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><%=LBL_SaberMais%></a>
                    </div>
                    <!--div class="mdl-card__menu">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </div-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                    <div class="mdl-card__title bg-gray-medium">
                        <h2 class="mdl-card__title-text">Valorizar Socialmente o Desporto</h2>
                    </div>
                    <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/slide1.jpg)'></div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a href="/Pages/ValorizarSocialmenteDesporto.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><%=LBL_SaberMais%></a>
                    </div>
                    <!--div class="mdl-card__menu">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </div-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                    <div class="mdl-card__title bg-gray-medium">
                        <h2 class="mdl-card__title-text">Publicações Periódicas</h2>
                    </div>
                    <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/slide1.jpg)'></div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a href="/Pages/PublicacoesPeriodicas.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><%=LBL_SaberMais%></a>
                    </div>
                    <!--div class="mdl-card__menu">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </div-->
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="row publications">
        <div class="container">
            <div class="col-md-4">
                <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                    <div class="mdl-card__title bg-green">
                        <h2 class="mdl-card__title-text">Treino Desportivo</h2>
                    </div>
                    <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/slide1.jpg)'></div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a href="/Pages/TreinoDesportivo.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><%=LBL_SaberMais%></a>
                    </div>
                    <!--div class="mdl-card__menu">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </div-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                    <div class="mdl-card__title bg-blue">
                        <h2 class="mdl-card__title-text">Psicologia e Pedagogia do Desporto</h2>
                    </div>
                    <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/slide1.jpg)'></div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a href="/Pages/PsicologiaPedagogiaDesporto.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><%=LBL_SaberMais%></a>
                    </div>
                    <!--div class="mdl-card__menu">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </div-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                    <div class="mdl-card__title bg-orange">
                        <h2 class="mdl-card__title-text">Medicina do Desporto</h2>
                    </div>
                    <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/slide1.jpg)'></div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a href="/Pages/MedicinaDesporto.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><%=LBL_SaberMais%></a>
                    </div>
                    <!--div class="mdl-card__menu">
                        <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                    </div-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                    <div class="mdl-card__title bg-gray">
                        <h2 class="mdl-card__title-text">Fisiologia e Biomecânica do Desporto</h2>
                    </div>
                    <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/slide1.jpg)'></div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a href="/Pages/FisiologiaBiomecanicaDesporto.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><%=LBL_SaberMais%></a>
                    </div>
                    <!--div class="mdl-card__menu">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        </div-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                    <div class="mdl-card__title bg-red-dark">
                        <h2 class="mdl-card__title-text">Economia, Direito e Gestão do Desporto</h2>
                    </div>
                    <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/slide1.jpg)'></div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a href="/Pages/EconomiaDireitoGestaoDesporto.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><%=LBL_SaberMais%></a>
                    </div>
                    <!--div class="mdl-card__menu">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        </div-->
                </div>
            </div>
            <div class="col-md-4">
                <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                    <div class="mdl-card__title bg-pink">
                        <h2 class="mdl-card__title-text">Sociologia e História do Desporto</h2>
                    </div>
                    <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/slide1.jpg)'></div>
                    <div class="mdl-card__actions mdl-card--border">
                        <a href="/Pages/SociologiaHistoriaDesporto.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect"><%=LBL_SaberMais%></a>
                    </div>
                    <!--div class="mdl-card__menu">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        </div-->
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function () {
        var callthis = $('.calltoaction').closest('section'); $('.jssocials').closest('section').insertBefore(callthis);
        $('.nav').find('.item.publications').addClass('selected');
        $('footer li a.publications').addClass('selected');
    });
</script>
