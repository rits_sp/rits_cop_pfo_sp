﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_VSD.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Publications.VSD.UC_VSD" %>


<link href="../../../../Style%20Library/cop/css/jplist/jplist.core.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.filter-toggle-bundle.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.history-bundle.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.pagination-bundle.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.preloader-control.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.textbox-filter.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.views-control.min.css" rel="stylesheet" />
<link href="../../../../Style%20Library/cop/css/jplist/jplist.peo.css" rel="stylesheet" />

<section>

    <div class="slider-container">
        <!-- Header Carousel -->
        <!-- START REVOLUTION SLIDER 5.0 -->
        <div class="rev_slider_wrapper">
            <div id="rev_slider_inside" class="rev_slider" data-version="5.0">
                <ul>
                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="../../../../_layouts/15/images/RITS.COP.BR/PHO10432129.retouche.jpg" alt="" width="1920" height="1080" data-bgposition="center center" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption News-Title"
                            data-x="left" data-hoffset="80"
                            data-y="bottom" data-voffset="150"
                            data-whitespace="normal"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="500"
                            data-bgposition="center center">
                            <div class="insidepages">
                                <h1 class="page-header">Publicações</h1>
                            </div>
                        </div>
                    </li>

                </ul>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>
        <!-- END OF SLIDER WRAPPER -->
    </div>
</section>

<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                    <li class="active">LblValorizarSocialmenteDesporto</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container activities">
        <div class="row">
            <div class="col-lg-9 withtoprightmenu">
                <h2>LblValorizarSocialmenteDesporto</h2>
                <p class="introtext">
                    TxtValorizarSocialmenteDesportoIntroText
                </p>
            </div>
            <div class="col-lg-3 toprightmenu">
                <div class="mdl-card mdl-shadow--2dp categorybox publications bg-gray-light ">
                    <div class="row">
                        <div class="mdl-card__title toptitle bg-gray-medium">
                            <h2 class="mdl-card__title-text">Valorizar Socialmente o Desporto</h2>
                            
                            <button id="menu-lower-right" class="mdl-button mdl-js-button mdl-button--icon" onclick="return false">
                              <i class="zmdi zmdi-more-vert"></i>
                            </button>

                            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect" for="menu-lower-right">
                              <li class="mdl-menu__item"><a href="/Pages/RevistaOlimpo.aspx">Revista Olimpo</a></li>
                              <li class="mdl-menu__item hide"><a href="/Pages/ValorizarSocialmenteDesporto.aspx">Valorizar Socialmente o Desporto</a></li>
                              <li class="mdl-menu__item topubs"><a href="/Pages/PublicacoesPeriodicas.aspx">Publicações Periódicas</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/TreinoDesportivo.aspx">Treino Desportivo</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/PsicologiaPedagogiaDesporto.aspx">Psicologia e Pedagogia do Desporto</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/MedicinaDesporto.aspx">Medicina do Desporto</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/FisiologiaBiomecanicaDesporto.aspx">Fisiologia e Biomecânica do Desporto</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/EconomiaDireitoGestaoDesporto.aspx">Economia, Direito e Gestão do Desporto</a></li>
                              <li class="mdl-menu__item"><a href="/Pages/SociologiaHistoriaDesporto.aspx">Sociologia e História do Desporto</a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="mdl-card__image" style="background-image: url(../../../../../Style%20Library/cop/imgs/IMG_7786.jpg)"></div>
                    </div>
                </div>
            </div>
        </div>
        <div id="resources-responsive-panel" class="responsive-item mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="zmdi zmdi-search-for"></i>Filtrar Conteúdos</div>
        <!-- main content -->
        <div class="box">
            <div>
                <!--<><><><><><><><><><><><><><><><><><><><><><><><><><> FILTERING START <><><><><><><><><><><><><><><><><><><><><><><><><><>-->

                <div id="filtering" class="box jplist">
                    <div class="col-md-9">
                        <!-- data -->
                        <div class="list box text-shadow">
                            <!-- item -->
                            <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                <p class="date hide">01/01/2040 00:00:00</p>
                                <div class="mdl-card__title bg-gray-medium">
                                    <h3 class="title mdl-card__title-text"><a href="/Pages/resumoatividades.aspx">Revista Olimpo #000</a></h3>
                                </div>
                                <a href=""><div class="img mdl-card__image" style="background-image:url(../../../../../_layouts/15/images/RITS.COP.PFO.BR/DSC_0092.jpg)"></div></a>
                                <div class="block mdl-card__actions mdl-card--border">
                                    <div class="tagshere">
                                        <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-tags"></i>
                                        </a>
                                        <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                        <div class="theme tags-shower">
                                            <div class="tags-holder">
                                                <div class="col-md-3">
                                                    <div class="tag-title">Tags</div>
                                                </div>
                                                <div class="col-md-9 tag-holder">
                                                    <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Qualidades Físicas</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <!-- item -->
                            <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                <p class="date hide">01/01/2040 00:00:00</p>
                                <div class="mdl-card__title bg-gray-medium">
                                    <h3 class="title mdl-card__title-text"><a href="/Pages/resumoatividades.aspx">Revista Olimpo #000</a></h3>
                                </div>
                                <a href=""><div class="img mdl-card__image" style="background-image:url(../../../../../_layouts/15/images/RITS.COP.PFO.BR/DSC_0092.jpg)"></div></a>
                                <div class="block mdl-card__actions mdl-card--border">
                                    <div class="tagshere">
                                        <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-tags"></i>
                                        </a>
                                        <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                        <div class="theme tags-shower">
                                            <div class="tags-holder">
                                                <div class="col-md-3">
                                                    <div class="tag-title">Tags</div>
                                                </div>
                                                <div class="col-md-9 tag-holder">
                                                    <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Qualidades Físicas</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <!-- item -->
                            <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                <p class="date hide">01/01/2040 00:00:00</p>
                                <div class="mdl-card__title bg-gray-medium">
                                    <h3 class="title mdl-card__title-text"><a href="/Pages/resumoatividades.aspx">Revista Olimpo #000</a></h3>
                                </div>
                                <a href=""><div class="img mdl-card__image" style="background-image:url(../../../../../_layouts/15/images/RITS.COP.PFO.BR/DSC_0092.jpg)"></div></a>
                                <div class="block mdl-card__actions mdl-card--border">
                                    <div class="tagshere">
                                        <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-tags"></i>
                                        </a>
                                        <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                        <div class="theme tags-shower">
                                            <div class="tags-holder">
                                                <div class="col-md-3">
                                                    <div class="tag-title">Tags</div>
                                                </div>
                                                <div class="col-md-9 tag-holder">
                                                    <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Qualidades Físicas</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <!-- item -->
                            <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                <p class="date hide">01/01/2040 00:00:00</p>
                                <div class="mdl-card__title bg-gray-medium">
                                    <h3 class="title mdl-card__title-text"><a href="/Pages/resumoatividades.aspx">Revista Olimpo #000</a></h3>
                                </div>
                                <a href=""><div class="img mdl-card__image" style="background-image:url(../../../../../_layouts/15/images/RITS.COP.PFO.BR/DSC_0092.jpg)"></div></a>
                                <div class="block mdl-card__actions mdl-card--border">
                                    <div class="tagshere">
                                        <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-tags"></i>
                                        </a>
                                        <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                        <div class="theme tags-shower">
                                            <div class="tags-holder">
                                                <div class="col-md-3">
                                                    <div class="tag-title">Tags</div>
                                                </div>
                                                <div class="col-md-9 tag-holder">
                                                    <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Qualidades Físicas</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <!-- item -->
                            <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                <p class="date hide">01/01/2040 00:00:00</p>
                                <div class="mdl-card__title bg-gray-medium">
                                    <h3 class="title mdl-card__title-text"><a href="/Pages/resumoatividades.aspx">Revista Olimpo #000</a></h3>
                                </div>
                                <a href=""><div class="img mdl-card__image" style="background-image:url(../../../../../_layouts/15/images/RITS.COP.PFO.BR/DSC_0092.jpg)"></div></a>
                                <div class="block mdl-card__actions mdl-card--border">
                                    <div class="tagshere">
                                        <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-tags"></i>
                                        </a>
                                        <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                        <div class="theme tags-shower">
                                            <div class="tags-holder">
                                                <div class="col-md-3">
                                                    <div class="tag-title">Tags</div>
                                                </div>
                                                <div class="col-md-9 tag-holder">
                                                    <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Qualidades Físicas</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <!-- item -->
                            <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                <p class="date hide">01/01/2040 00:00:00</p>
                                <div class="mdl-card__title bg-gray-medium">
                                    <h3 class="title mdl-card__title-text"><a href="/Pages/resumoatividades.aspx">Revista Olimpo #000</a></h3>
                                </div>
                                <a href=""><div class="img mdl-card__image" style="background-image:url(../../../../../_layouts/15/images/RITS.COP.PFO.BR/DSC_0092.jpg)"></div></a>
                                <div class="block mdl-card__actions mdl-card--border">
                                    <div class="tagshere">
                                        <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-tags"></i>
                                        </a>
                                        <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                        <div class="theme tags-shower">
                                            <div class="tags-holder">
                                                <div class="col-md-3">
                                                    <div class="tag-title">Tags</div>
                                                </div>
                                                <div class="col-md-9 tag-holder">
                                                    <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Qualidades Físicas</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <!-- item -->
                            <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                <p class="date hide">01/01/2040 00:00:00</p>
                                <div class="mdl-card__title bg-gray-medium">
                                    <h3 class="title mdl-card__title-text"><a href="/Pages/resumoatividades.aspx">Revista Olimpo #000</a></h3>
                                </div>
                                <a href=""><div class="img mdl-card__image" style="background-image:url(../../../../../_layouts/15/images/RITS.COP.PFO.BR/DSC_0092.jpg)"></div></a>
                                <div class="block mdl-card__actions mdl-card--border">
                                    <div class="tagshere">
                                        <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-tags"></i>
                                        </a>
                                        <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                        <div class="theme tags-shower">
                                            <div class="tags-holder">
                                                <div class="col-md-3">
                                                    <div class="tag-title">Tags</div>
                                                </div>
                                                <div class="col-md-9 tag-holder">
                                                    <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Qualidades Físicas</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <!-- item -->
                            <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                <p class="date hide">01/01/2040 00:00:00</p>
                                <div class="mdl-card__title bg-gray-medium">
                                    <h3 class="title mdl-card__title-text"><a href="/Pages/resumoatividades.aspx">Revista Olimpo #000</a></h3>
                                </div>
                                <a href=""><div class="img mdl-card__image" style="background-image:url(../../../../../_layouts/15/images/RITS.COP.PFO.BR/DSC_0092.jpg)"></div></a>
                                <div class="block mdl-card__actions mdl-card--border">
                                    <div class="tagshere">
                                        <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-tags"></i>
                                        </a>
                                        <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                        <div class="theme tags-shower">
                                            <div class="tags-holder">
                                                <div class="col-md-3">
                                                    <div class="tag-title">Tags</div>
                                                </div>
                                                <div class="col-md-9 tag-holder">
                                                    <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Qualidades Físicas</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <!-- item -->
                            <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                <p class="date hide">01/01/2040 00:00:00</p>
                                <div class="mdl-card__title bg-gray-medium">
                                    <h3 class="title mdl-card__title-text"><a href="/Pages/resumoatividades.aspx">Revista Olimpo #000</a></h3>
                                </div>
                                <a href=""><div class="img mdl-card__image" style="background-image:url(../../../../../_layouts/15/images/RITS.COP.PFO.BR/DSC_0092.jpg)"></div></a>
                                <div class="block mdl-card__actions mdl-card--border">
                                    <div class="tagshere">
                                        <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-tags"></i>
                                        </a>
                                        <a class="pdf-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect bg-blue">
                                            <i class="fa fa-file-pdf-o" aria-hidden="true"></i>
                                        </a>
                                        <a class="link-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                            <i class="fa fa-link" aria-hidden="true"></i>
                                        </a>
                                        <div class="theme tags-shower">
                                            <div class="tags-holder">
                                                <div class="col-md-3">
                                                    <div class="tag-title">Tags</div>
                                                </div>
                                                <div class="col-md-9 tag-holder">
                                                    <div class="taghere keywords"><%--<%# DataBinder.Eval (Container.DataItem, "TagActivities") %>--%>Técnica Desportiva</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end of item -->
                            <%--<asp:Repeater ID="rptActivities" runat="server">
                                <ItemTemplate>
                                    <!-- item -->
                                    <div class="list-item box mdl-card mdl-shadow--2dp bg-gray-light">
                                        <p class="date hide"><%# DataBinder.Eval (Container.DataItem, "Date","{0:d/M/yyyy HH:mm:ss}") %></p>
                                        <div class="mdl-card__title bg-gray-medium">
                                            <h3 class="title mdl-card__title-text"><a href="/Pages/Atividade.aspx?at=<%# DataBinder.Eval (Container.DataItem, "Key") %>"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></h3>
                                        </div>
                                        <a href="/Pages/Atividade.aspx?at=<%# DataBinder.Eval (Container.DataItem, "Key") %>"><div class="img mdl-card__image" style='background-image:url(<%# DataBinder.Eval (Container.DataItem, "Image") %>)'></div></a>
                                        <div class="mdl-card__supporting-text bg-gray-lighter">
                                            <p class="desc"><%# DataBinder.Eval (Container.DataItem, "Description") %></p>
                                        </div>
                                        <div class="block mdl-card__actions mdl-card--border">
                                            <div class="tagshere">
                                                <a class="tags-show mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect">
                                                    <i class="fa fa-tags"></i>
                                                </a>
                                                <a href="/Pages/Atividade.aspx?at=<%# DataBinder.Eval (Container.DataItem, "Key") %>" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Ler mais</a>
                                                <div class="theme tags-shower">
                                                    <div class="tags-holder">
                                                        <div class="col-md-3">
                                                            <div class="tag-title">Tipo</div>
                                                        </div>
                                                        <div class="col-md-9 tag-holder">
                                                            <div class="taghere keywords"><%# DataBinder.Eval (Container.DataItem, "TypeActivities") %></div>
                                                        </div>
                                                    </div>
                                                    <div class="tags-holder">
                                                        <div class="col-md-3">
                                                            <div class="tag-title">Tags</div>
                                                        </div>
                                                        <div class="col-md-9 tag-holder">
                                                            <div class="taghere keywords"><%# DataBinder.Eval (Container.DataItem, "TagActivities") %></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>--%>

                                    <!-- end of item -->

                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <!-- end of data -->
                        <!-- panel -->
                        <div class="jplist-panel box panel-bottom">

                            <div
                                class="jplist-label"
                                data-type="{start} - {end} of {all}"
                                data-control-type="pagination-info"
                                data-control-name="paging"
                                data-control-action="paging">
                            </div>

                            <div
                                class="jplist-pagination"
                                data-control-type="pagination"
                                data-control-name="paging"
                                data-control-action="paging"
                                data-items-per-page="9"
                                data-control-animate-to-top="false">
                            </div>

                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="mdl-card mdl-shadow--2dp filterbox bg-gray-light">
                            <div class="mdl-card__title">
                                <h2 class="mdl-card__title-text">Filtrar Conteúdos</h2>
                            </div>



                            <!-- panel -->
                            <div class="jplist-panel box panel-top">
                                <!-- filter by title -->
                                <div class="text-filter-box">

                                    <i class="zmdi zmdi-search"></i>
                                    <div class="mdl-textfield mdl-js-textfield">
                                        <input data-path=".title,.desc"
                                            type="text"
                                            value=""
                                            data-control-type="textbox"
                                            data-control-name="title-filter desc-filter"
                                            data-control-action="filter" class="mdl-textfield__input" type="text" id="search-filters" placeholder="pesquisar" />
                                    </div>

                                </div>

                                <!-- checkbox text filter -->
                                <div
                                    class=""
                                    data-control-type="checkbox-text-filter"
                                    data-control-action="filter"
                                    data-control-name="keywords"
                                    data-path=".keywords"
                                    data-logic="or">

                                    <div class="filter-type">
                                        <label>Tags</label>
                                       <%-- <asp:Repeater ID="rptTagActivities" runat="server">
                                            <ItemTemplate>
                                                <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-<%# DataBinder.Eval (Container.DataItem, "ID") %>">
                                                    <input type="checkbox" id="Publications-Tag-<%# DataBinder.Eval (Container.DataItem, "ID") %>" class="mdl-checkbox__input" value="<%# DataBinder.Eval (Container.DataItem, "Choice") %>">
                                                    <span class="mdl-checkbox__label"><%# DataBinder.Eval (Container.DataItem, "Choice") %></span>
                                                </label>
                                            </ItemTemplate>
                                        </asp:Repeater>--%>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-Planeamento-Periodização">
                                            <input type="checkbox" id="Publications-Tag-Planeamento-Periodização" class="mdl-checkbox__input" value="Planeamento e Periodização">
                                            <span class="mdl-checkbox__label">Planeamento e Periodização</span>
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-Qualidades-Fisicas">
                                            <input type="checkbox" id="Publications-Tag-Qualidades-Fisicas" class="mdl-checkbox__input" value="Qualidades Físicas">
                                            <span class="mdl-checkbox__label">Qualidades Físicas</span>
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-Tecnica-Desportiva">
                                            <input type="checkbox" id="Publications-Tag-Tecnica-Desportivao" class="mdl-checkbox__input" value="Técnica Desportiva">
                                            <span class="mdl-checkbox__label">Técnica Desportiva</span>
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-Tactica-Analise-Jogo">
                                            <input type="checkbox" id="Publications-Tag-Tactica-Analise-Jogo" class="mdl-checkbox__input" value="Táctica e Análise de Jogo">
                                            <span class="mdl-checkbox__label">Táctica e Análise de Jogo</span>
                                        </label>
                                        <label class="mdl-checkbox mdl-js-checkbox mdl-js-ripple-effect" for="Publications-Tag-Avaliacao-Controlo-Treino">
                                            <input type="checkbox" id="Publications-Tag-Avaliacao-Controlo-Treino" class="mdl-checkbox__input" value="Avaliação e Controlo do Treino">
                                            <span class="mdl-checkbox__label">Avaliação e Controlo do Treino</span>
                                        </label>
                                    </div>

                                </div>


                                <!-- items per page dropdown -->
                                <div
                                    class="jplist-drop-down"
                                    data-control-type="items-per-page-drop-down"
                                    data-control-name="paging"
                                    data-control-action="paging">

                                    <ul>
                                        <li><span data-number="3">3 por página </span></li>
                                        <li><span data-number="6">6 por página </span></li>
                                        <li><span data-number="9" data-default="true">9 por página </span></li>
                                        <li><span data-number="all">Ver todos </span></li>
                                    </ul>
                                </div>

                                <!-- sort dropdown -->
                                <div
                                    class="jplist-drop-down"
                                    data-control-type="sort-drop-down"
                                    data-control-name="sort"
                                    data-control-action="sort"
                                    data-datetime-format="{month}/{day}/{year} {hour}:{min}:{sec}">
                                    <!-- {year}, {month}, {day}, {hour}, {min}, {sec} -->

                                    <ul>
                                        <li><span data-path=".title" data-order="asc" data-type="text">Título A-Z</span></li>
                                        <li><span data-path=".title" data-order="desc" data-type="text">Título Z-A</span></li>
                                        <li><span data-path=".desc" data-order="asc" data-type="text">Descrição A-Z</span></li>
                                        <li><span data-path=".desc" data-order="desc" data-type="text">Descrição Z-A</span></li>
                                        <li><span data-path=".date" data-order="asc" data-type="datetime">Data asc</span></li>
                                        <li><span data-path=".date" data-order="desc" data-type="datetime" data-default="true">Data desc</span></li>
                                    </ul>
                                </div>


                                <!-- views -->
                                <div
                                    class="jplist-views"
                                    data-control-type="views"
                                    data-control-name="views"
                                    data-control-action="views"
                                    data-default="jplist-grid-view">


                                <!-- pagination control -->
                                <div
                                    class="jplist-pagination"
                                    data-control-type="pagination"
                                    data-control-name="paging"
                                    data-items-per-page="9"
                                    data-control-action="paging">
                                </div>

                            </div>


                            <div class="box jplist-no-results text-shadow align-center">
                                <p>Sem resultados</p>
                            </div>

                        
            
            <!--<><><><><><><><><><><><><><><><><><><><><><><><><><> FILTERING END <><><><><><><><><><><><><><><><><><><><><><><><><><>-->
                        </div>
                    </div>
                </div>
</section>
<!-- /.container -->
<script src="../../../../Style%20Library/cop/js/jplist/jplist.core.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.sort-bundle.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.textbox-filter.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.pagination-bundle.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.history-bundle.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.filter-toggle-bundle.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.views-control.min.js"></script>
<script src="../../../../Style%20Library/cop/js/jplist/jplist.preloader-control.min.js"></script>
<script src="../../../../../Style%20Library/cop/js/peo-tags.js"></script>

<script>
    $('.nav').find('.item.publications').addClass('selected');
    $('.nav').find('.item.publications').addClass('selected');

    //responsive filtering tab
    function closepanel() {
        $('.filterbox').animate({ "left": "-100%" }, "slow");
        setTimeout(function () {
            $('#resources-close-panel').remove();
        }, 500);
    }

    $('document').ready(function () {
        $('footer li a.publications').addClass('selected');
        $('#filtering').jplist({


            //enable/disable logging information: if firebug is installed the debuging information will be displayed in the firebug console
            debug: false

            //main options
            , itemsBox: '.list' //items container jQuery path
            , itemPath: '.list-item' //jQuery path to the item within the items container
            , panelPath: '.jplist-panel' //panel jQuery path
            , noResults: '.jplist-no-results' //'no reaults' section jQuery path
            , redrawCallback: function (collection, $dataview, statuses) {
                //this code occurs on every jplist action
                if ($('body').hasClass('mobile')) {
                    $('#resources-close-panel i').removeClass('.zmdi-close-circle-o').addClass('zmdi-eye');
                }
            }
            , iosBtnPath: '.jplist-ios-button'

            //animate to top - enabled by data-control-animate-to-top="true" attribute in control
            , animateToTop: 'html, body'
            , animateToTopDuration: 0 //in milliseconds (1000 ms = 1 sec)

            //animation effects
            , effect: '' //'', 'fade'
            , duration: 300
            , fps: 24

            //save plugin state with storage
            , storage: '' //'', 'cookies', 'localstorage'      
            , storageName: 'jplist'
            , cookiesExpiration: -1 //cookies expiration in minutes (-1 = cookies expire when browser is closed)

            //deep linking
            , deepLinking: false
            , delimiter0: ':' //this delimiter is placed after the control name 
            , delimiter1: '|' //this delimiter is placed between key-value pairs
            , delimiter2: '~' //this delimiter is placed between multiple value of the same key
            , delimiter3: '!' //additional delimiter

            //history
            , historyLength: 10

            //data source
            , dataSource: {

                type: 'html' //'html', 'server'

                //data source server side
                //,server: {

                //    //ajax settings
                //    ajax:{
                //        //url: 'server.php',
                //    dataType: 'html'
                //    ,type: 'POST'
                //        //,cache: false
                //    }
                // ,serverOkCallback: null
                // ,serverErrorCallback: null
                //}
            }

            //panel controls
            , controlTypes: {

                'default-sort': {
                    className: 'DefaultSort'
                 , options: {}
                }

            , 'drop-down': {
                className: 'Dropdown'
             , options: {}
            }

            , 'pagination-info': {
                className: 'PaginationInfo'
             , options: {}
            }

            , 'counter': {
                className: 'Counter'
             , options: {
                 ignore: '[~!@#$%^&*()+=`\'"\/\\_]+' //[^a-zA-Z0-9]+ not letters/numbers: [~!@#$%^&*\(\)+=`\'"\/\\_]+
             }
            }

            , 'pagination': {
                className: 'Pagination'
             , options: {

                 //paging
                 range: 4
             , jumpToStart: false

                 //arrows
             , prevArrow: '‹'
             , nextArrow: '›'
             , firstArrow: '«'
             , lastArrow: '»'
             }
            }

            , 'reset': {
                className: 'Reset'
             , options: {}
            }

            , 'select': {
                className: 'Select'
             , options: {}
            }

            , 'textbox': {
                className: 'Textbox'
             , options: {
                 eventName: 'keyup'
             , ignore: '[~!@#$%^&*()+=`\'"\/\\_]+' //[^a-zA-Z0-9]+ not letters/numbers: [~!@#$%^&*\(\)+=`\'"\/\\_]+              
             }
            }

            , 'views': {
                className: 'Views'
             , options: {}
            }

            , 'checkbox-group-filter': {
                className: 'CheckboxGroupFilter'
             , options: {}
            }

            , 'checkbox-text-filter': {
                className: 'CheckboxTextFilter'
             , options: {
                 ignore: '' //regex for the characters to ignore, for example: [^a-zA-Z0-9]+
             }
            }

            , 'button-filter': {
                className: 'ButtonFilter'
             , options: {}
            }

            , 'button-filter-group': {
                className: 'ButtonFilterGroup'
             , options: {}
            }

            , 'button-text-filter': {
                className: 'ButtonTextFilter'
             , options: {
                 ignore: '[~!@#$%^&*()+=`\'"\/\\_]+' //[^a-zA-Z0-9]+ not letters/numbers: [~!@#$%^&*\(\)+=`\'"\/\\_]+
             }
            }

            , 'button-text-filter-group': {
                className: 'ButtonTextFilterGroup'
             , options: {
                 ignore: '[~!@#$%^&*()+=`\'"\/\\_]+' //[^a-zA-Z0-9]+ not letters/numbers: [~!@#$%^&*\(\)+=`\'"\/\\_]+
             }
            }

            , 'radio-buttons-filters': {
                className: 'RadioButtonsFilter'
             , options: {}
            }

              , 'range-filter': {
                  className: 'Range<a href="http://www.jqueryscript.net/slider/">Slider</a>ToggleFilter'
             , options: {}
              }

            , 'back-button': {
                className: 'BackButton'
             , options: {}
            }

            , 'preloader': {
                className: 'Preloader'
             , options: {}
            }
            }
        });



        $('#resources-responsive-panel').click(function () {
            $('.filterbox').prepend('<a onclick="closepanel();" id="resources-close-panel" class="responsive-item mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"><i class="zmdi zmdi-close-circle-o"></i></a>');
            $('.filterbox').animate({ "left": "0px" }, "slow");
        });

        //resources-responsive-panel responsive fix for scrolling
        $(".portrait #resources-responsive-panel .filterbox").css({ maxHeight: $(window).height() - $(".navbar-header").height() + "px" });
        //

        componentHandler.upgradeDom('MaterialTextfield', 'mdl-js-textfield');

        //if ipad landscape change bootstrap lg column to md
        if ((window.innerHeight < window.innerWidth) && (navigator.userAgent.match(/(iPad)/))) {
            $('#filtering .col-md-9').removeClass('col-md-9').addClass('col-md-8');
            $('#filtering .col-md-3').removeClass('col-md-3').addClass('col-md-4');
            $('.jplist-drop-down li span[data-number="3"]').attr('data-number', '2').text('2 por página');
            $('.jplist-drop-down li span[data-number="6"]').attr('data-number', '4').text('4 por página').attr('data-default', 'true').click();
            $('.jplist-drop-down li span[data-number="9"]').attr('data-number', '8').text('8 por página');
        }
    });
</script>
