﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_footer.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_footer" %>


<%@ Register Src="~/_controltemplates/15/RITS.COP.PFO.UI/Main/UC_footerMenu.ascx" TagPrefix="uc1" TagName="UC_footerMenu" %>
<%@ Register Src="~/_controltemplates/15/RITS.COP.PFO.UI/Main/UC_footerSocial.ascx" TagPrefix="uc1" TagName="UC_footerSocial" %>
<%@ Register Src="~/_controltemplates/15/RITS.COP.PFO.UI/Main/UC_footerBottom.ascx" TagPrefix="uc1" TagName="UC_footerBottom" %>
<%@ Register Src="~/_controltemplates/15/RITS.COP.PFO.UI/Main/UC_footerNewsletter.ascx" TagPrefix="uc1" TagName="UC_footerNewsletter" %>

<section>
    <div class="container">
        <div id="share"></div>
    </div>
</section>
<!-- Footer -->
<footer>
    <div class="row footer-menu bg-gray">
        <div class="container">
            <div class="row">
                    <uc1:UC_footerMenu runat="server" id="UC_footerMenu" />
             
                <div class="col-lg-4 txt-right newsletter">
                    <uc1:UC_footerNewsletter runat="server" id="UC_footerNewsletter" />
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="social-icons">
                        <uc1:UC_footerSocial runat="server" id="UC_footerSocial" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row footer-menu bg-gray-dark">
        <div class="container">
            <uc1:UC_footerBottom runat="server" id="UC_footerBottom" />
        </div>
    </div>
</footer>
