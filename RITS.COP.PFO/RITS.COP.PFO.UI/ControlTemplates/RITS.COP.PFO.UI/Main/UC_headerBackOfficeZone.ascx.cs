﻿using System;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Core;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main
{
    public partial class UC_headerBackOfficeZone : UserControl
    {
        #region Variables
        public string UrlSettings { get; set; }
        public string NumberContactsNotReview { get; set; }

        public string UrlNotifications { get; set; }

        public string NumberNotifications { get; set; }

        public string UrlRegistoEscolas { get; set; }

        public string UrlRegistoProfessores { get; set; }

        public string UrlDocHelper { get; set; }

        public string Url { get; set; }

        public string List { get; set; }

        public string ListDocuments { get; set; }

        #endregion

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Url = IFL.Core.Constants.Urls._formacao;
                SPWeb web = SPContext.Current.Web;
                UrlSettings = string.Format("{0}{1}", web.Url, "/_layouts/15/viewlsts.aspx");
                SPUser user = SPContext.Current.Web.CurrentUser;

                if (user.LoginName != null)
                {
                    lbl_Username.Text = user.Name ?? user.LoginName;
                    div_backoffice_Settings.Visible = true;
                    div_backoffice_Logout.Visible = true;
                    div_backoffice_UserName.Visible = true;
                    div_backoffice_NofiticationsContacts.Visible = true;
                    div_backoffice_NofiticationsTS.Visible = true;
                    div_backoffice_Admin.Visible = true;
                }

                Url = IFL.Core.Constants.Urls._formacao;
                List = IFL.Core.Constants.Lists._cop_pfo_Contacts;

                NumberContactsNotReview = LoadObjects.GetContactsCount(Url, List, false).ToString();
            }



            catch (Exception ex)
            { }
        }



        protected void btn_Logout_OnClick(object sender, EventArgs e)
        {
            try
            {
                Url = IFL.Core.Constants.Urls._formacao;
                SPWeb web = SPContext.Current.Web;
                SPUser user = SPContext.Current.Web.CurrentUser;

                if (user.LoginName != null)
                {
                    var session = HttpContext.Current.Session;
                    if (session != null)
                    {
                        session.Clear();
                        session.Abandon();
                        session.RemoveAll();
                    }
                    System.Web.Security.FormsAuthentication.SignOut();
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
                    Response.Cache.SetNoStore();
                    ViewState.Clear();


                    HttpContext.Current.Response.Redirect(Url + IFL.Core.Constants.UrlsPages._pageHomePage, false);
                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

    }
}
