﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main
{
    public partial class UC_footerMenu : UserControl
    {
        DataTable dt_menus = null;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            LoadFields();
        }
        public void LoadFields()
        {
            try
            {
                //Referencias
                string _url = IFL.Core.Constants.Urls._formacao;
                string _list = IFL.Core.Constants.Lists._cop_pfo_Navigation;
                string _menu = IFL.Core.Constants.Menus._menu_Footer;

                dt_menus = IFL.Core.LoadObjects.LoadMenu(_url, _list, _menu, 15);

                // HyperLink Inicio
                var btn_hyp_start = IFL.Core.LoadObjects.GetParameter("hyp_start", dt_menus);
                hyp_start.Text = btn_hyp_start.Value ?? string.Empty;
                hyp_start.NavigateUrl = btn_hyp_start.Link ?? @"/";
                hyp_start.Visible = Convert.ToBoolean(btn_hyp_start.Visible);
                hyp_start.Enabled = Convert.ToBoolean(btn_hyp_start.Enable);

                // HyperLink - Formacões
                var btn_hyp_formations = IFL.Core.LoadObjects.GetParameter("hyp_formations", dt_menus);
                hyp_formations.Text = btn_hyp_formations.Value ?? string.Empty;
                hyp_formations.NavigateUrl = btn_hyp_formations.Link ?? @"/";
                hyp_formations.Visible = Convert.ToBoolean(btn_hyp_formations.Visible);
                hyp_formations.Enabled = Convert.ToBoolean(btn_hyp_formations.Enable);

                // HyperLink - Publicações
                var btn_hyp_publications = IFL.Core.LoadObjects.GetParameter("hyp_publications", dt_menus);
                hyp_publications.Text = btn_hyp_publications.Value ?? string.Empty;
                hyp_publications.NavigateUrl = btn_hyp_publications.Link ?? @"/";
                hyp_publications.Visible = Convert.ToBoolean(btn_hyp_publications.Visible);
                hyp_publications.Enabled = Convert.ToBoolean(btn_hyp_publications.Enable);

                // HyperLink - Prémios COP
                var btn_hyp_copawards = IFL.Core.LoadObjects.GetParameter("hyp_copawards", dt_menus);
                hyp_copawards.Text = btn_hyp_copawards.Value ?? string.Empty;
                hyp_copawards.NavigateUrl = btn_hyp_copawards.Link ?? @"/";
                hyp_copawards.Visible = Convert.ToBoolean(btn_hyp_copawards.Visible);
                hyp_copawards.Enabled = Convert.ToBoolean(btn_hyp_copawards.Enable);

                // HyperLink - Destaques
                var btn_hyp_highlights = IFL.Core.LoadObjects.GetParameter("hyp_highlights", dt_menus);
                hyp_highlights.Text = btn_hyp_highlights.Value ?? string.Empty;
                hyp_highlights.NavigateUrl = btn_hyp_highlights.Link ?? @"/";
                hyp_highlights.Visible = Convert.ToBoolean(btn_hyp_highlights.Visible);
                hyp_highlights.Enabled = Convert.ToBoolean(btn_hyp_highlights.Enable);

                // HyperLink - Agenda
                var btn_hyp_agenda = IFL.Core.LoadObjects.GetParameter("hyp_agenda", dt_menus);
                hyp_agenda.Text = btn_hyp_agenda.Value ?? string.Empty;
                hyp_agenda.NavigateUrl = btn_hyp_agenda.Link ?? @"/";
                hyp_agenda.Visible = Convert.ToBoolean(btn_hyp_agenda.Visible);
                hyp_agenda.Enabled = Convert.ToBoolean(btn_hyp_agenda.Enable);

                // HyperLink - Submeter Publicações
                var btn_hyp_submitDocument = IFL.Core.LoadObjects.GetParameter("hyp_submitDocument", dt_menus);
                hyp_submitDocument.Text = btn_hyp_submitDocument.Value ?? string.Empty;
                hyp_submitDocument.NavigateUrl = btn_hyp_submitDocument.Link ?? @"/";
                hyp_submitDocument.Visible = Convert.ToBoolean(btn_hyp_submitDocument.Visible);
                hyp_submitDocument.Enabled = Convert.ToBoolean(btn_hyp_submitDocument.Enable);

                // HyperLink - FAQs
                var btn_hyp_faqs = IFL.Core.LoadObjects.GetParameter("hyp_faqs", dt_menus);
                hyp_faqs.Text = btn_hyp_faqs.Value ?? string.Empty;
                hyp_faqs.NavigateUrl = btn_hyp_faqs.Link ?? @"/";
                hyp_faqs.Visible = Convert.ToBoolean(btn_hyp_faqs.Visible);
                hyp_faqs.Enabled = Convert.ToBoolean(btn_hyp_faqs.Enable);

                // HyperLink - Contatos
                var btn_hyp_contacts = IFL.Core.LoadObjects.GetParameter("hyp_contacts", dt_menus);
                hyp_contacts.Text = btn_hyp_contacts.Value ?? string.Empty;
                hyp_contacts.NavigateUrl = btn_hyp_contacts.Link ?? @"/";
                hyp_contacts.Visible = Convert.ToBoolean(btn_hyp_contacts.Visible);
                hyp_contacts.Enabled = Convert.ToBoolean(btn_hyp_contacts.Enable);

                // HyperLink - Historia
                var btn_hyp_history = IFL.Core.LoadObjects.GetParameter("hyp_history", dt_menus);
                hyp_history.Text = btn_hyp_history.Value ?? string.Empty;
                hyp_history.NavigateUrl = btn_hyp_history.Link ?? @"/";
                hyp_history.Visible = Convert.ToBoolean(btn_hyp_history.Visible);
                hyp_history.Enabled = Convert.ToBoolean(btn_hyp_history.Enable);


                // HyperLink - Educação
                var btn_hyp_education = IFL.Core.LoadObjects.GetParameter("hyp_education", dt_menus);
                hyp_education.Text = btn_hyp_education.Value ?? string.Empty;
                hyp_education.NavigateUrl = btn_hyp_education.Link ?? @"/";
                hyp_education.Visible = Convert.ToBoolean(btn_hyp_education.Visible);
                hyp_education.Enabled = Convert.ToBoolean(btn_hyp_education.Enable);
            }
            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected,"001","UI - UC_footerNewsletter - LoadFields()" ,ex.Message);
                //SPUtility.Redirect("",true,)
            }


            
        }
    }
}
