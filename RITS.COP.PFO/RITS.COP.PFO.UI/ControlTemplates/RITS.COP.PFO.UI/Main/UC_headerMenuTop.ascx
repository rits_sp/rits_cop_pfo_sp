﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_headerMenuTop.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_headerMenuTop" %>


<div class="navbar-topheader-item menu">
    <ul class="nav navbar-nav navbar-left">
        <li class="home">
            <asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink>
        </li>
        <li class="submitDocument">
            <asp:HyperLink runat="server" ID="hyp_submitDocument"></asp:HyperLink>
        </li>
        <li class="faqs">
            <asp:HyperLink runat="server" ID="hyp_faqs"></asp:HyperLink>
        </li>
        <li class="contacts">
            <asp:HyperLink runat="server" ID="hyp_contacts"></asp:HyperLink>
        </li>
    </ul>
</div>
<div class="topheader-sub-menu">
    <asp:HyperLink runat="server" ID="hyp_education" CssClass="educacao" Target="new"></asp:HyperLink>
    <asp:HyperLink runat="server" ID="hyp_history" CssClass="historia" Target="new"></asp:HyperLink>
</div>

