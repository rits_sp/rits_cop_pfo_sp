﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main
{
    public partial class UC_footerBottom : UserControl
    {
        DataTable dt_menus = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadFields();
        }

        public void LoadFields()
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {
                    //Referencias
                    string _url = IFL.Core.Constants.Urls._formacao;
                    string _list = IFL.Core.Constants.Lists._cop_pfo_Navigation;
                    string _menu = IFL.Core.Constants.Menus._menu_Footer_Bottom;

                    dt_menus = IFL.Core.LoadObjects.LoadMenu(_url, _list, _menu, 6);


                    // link Política de Privacidade
                    var btn_hyp_privacyPolicy = IFL.Core.LoadObjects.GetParameter("hyp_privacyPolicy", dt_menus);
                    hyp_privacyPolicy.Text = btn_hyp_privacyPolicy.Value ?? string.Empty;
                    hyp_privacyPolicy.NavigateUrl = btn_hyp_privacyPolicy.Link ?? @"/";
                    hyp_privacyPolicy.Visible = Convert.ToBoolean(btn_hyp_privacyPolicy.Visible);
                    hyp_privacyPolicy.Enabled = Convert.ToBoolean(btn_hyp_privacyPolicy.Enable);



                    // link Termos e Condições
                    var btn_hyp_termsConditions = IFL.Core.LoadObjects.GetParameter("hyp_termsConditions", dt_menus);
                    hyp_termsConditions.Text = btn_hyp_termsConditions.Value ?? string.Empty;
                    hyp_termsConditions.NavigateUrl = btn_hyp_termsConditions.Link ?? @"/";
                    hyp_termsConditions.Visible = Convert.ToBoolean(btn_hyp_termsConditions.Visible);
                    hyp_termsConditions.Enabled = Convert.ToBoolean(btn_hyp_termsConditions.Enable);


                    // link Política de Uso de Cookies
                    var btn_hyp_policyCookies = IFL.Core.LoadObjects.GetParameter("hyp_policyCookies", dt_menus);
                    hyp_policyCookies.Text = btn_hyp_policyCookies.Value ?? string.Empty;
                    hyp_policyCookies.NavigateUrl = btn_hyp_policyCookies.Link ?? @"/";
                    hyp_policyCookies.Visible = Convert.ToBoolean(btn_hyp_policyCookies.Visible);
                    hyp_policyCookies.Enabled = Convert.ToBoolean(btn_hyp_policyCookies.Enable);
                });
            }



            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "UI - UC_footerBottom - LoadFields()",
                    ex.Message);

            }
        }
    }

}
