﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main
{
    public partial class UC_headerMainMenu : UserControl
    {
        DataTable dt_menus = null;
        protected void Page_Load(object sender, EventArgs e)
        {

            LoadFields();

        }

        public void LoadFields()
        {
            try
            {
                //Referencias
                string _url = IFL.Core.Constants.Urls._formacao;
                string _list = IFL.Core.Constants.Lists._cop_pfo_Navigation;
                string _menu = IFL.Core.Constants.Menus._menu_Main;

                dt_menus = IFL.Core.LoadObjects.LoadMenu(_url, _list, _menu, 15);

                // HyperLink - Fromações
                var btn_hyp_formations = IFL.Core.LoadObjects.GetParameter("hyp_formations", dt_menus);
                hyp_formations.Text = btn_hyp_formations.Value ?? string.Empty;
                hyp_formations.NavigateUrl = btn_hyp_formations.Link ?? @"/";
                hyp_formations.Visible = Convert.ToBoolean(btn_hyp_formations.Visible);
                hyp_formations.Enabled = Convert.ToBoolean(btn_hyp_formations.Enable);

                // HyperLink - Publicações
                var btn_hyp_publications = IFL.Core.LoadObjects.GetParameter("hyp_publications", dt_menus);
                hyp_publications.Text = btn_hyp_publications.Value ?? string.Empty;
                hyp_publications.NavigateUrl = btn_hyp_publications.Link ?? @"/";
                hyp_publications.Visible = Convert.ToBoolean(btn_hyp_publications.Visible);
                hyp_publications.Enabled = Convert.ToBoolean(btn_hyp_publications.Enable);

                // HyperLink - Prémios COP
                var btn_hyp_copawards = IFL.Core.LoadObjects.GetParameter("hyp_copawards", dt_menus);
                hyp_copawards.Text = btn_hyp_copawards.Value ?? string.Empty;
                hyp_copawards.NavigateUrl = btn_hyp_copawards.Link ?? @"/";
                hyp_copawards.Visible = Convert.ToBoolean(btn_hyp_copawards.Visible);
                hyp_copawards.Enabled = Convert.ToBoolean(btn_hyp_copawards.Enable);

                // HyperLink - Destaques
                var btn_hyp_highlights = IFL.Core.LoadObjects.GetParameter("hyp_highlights", dt_menus);
                hyp_highlights.Text = btn_hyp_highlights.Value ?? string.Empty;
                hyp_highlights.NavigateUrl = btn_hyp_highlights.Link ?? @"/";
                hyp_highlights.Visible = Convert.ToBoolean(btn_hyp_highlights.Visible);
                hyp_highlights.Enabled = Convert.ToBoolean(btn_hyp_highlights.Enable);


                // HyperLink - Agenda
                var btn_hyp_agenda = IFL.Core.LoadObjects.GetParameter("hyp_agenda", dt_menus);
                hyp_agenda.Text = btn_hyp_agenda.Value ?? string.Empty;
                hyp_agenda.NavigateUrl = btn_hyp_agenda.Link ?? @"/";
                hyp_agenda.Visible = Convert.ToBoolean(btn_hyp_agenda.Visible);
                hyp_agenda.Enabled = Convert.ToBoolean(btn_hyp_agenda.Enable);
            }
            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "UI - UC_headerMainMenu - LoadFields()", ex.Message);
                
            }
        }
    }
}
