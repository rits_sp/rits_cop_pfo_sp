﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_footerMenu.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_footerMenu" %>


<div class="col-lg-8">
    <ul>
        <li>
            <asp:HyperLink runat="server" ID="hyp_start" CssClass="home"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_formations" CssClass="formations"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_publications" CssClass="publications"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_copawards" CssClass="copawards"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_highlights" CssClass="highlights"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_agenda" CssClass="agenda"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_submitDocument" CssClass="submitDocument"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_faqs" CssClass="faqs"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_contacts" CssClass="contacts"></asp:HyperLink>
        </li>
    </ul>

</div>
<div class="col-lg-8 footer-sub-menu">
    <asp:HyperLink runat="server" ID="hyp_education" CssClass="educacao" Target="new"></asp:HyperLink>
    <asp:HyperLink runat="server" ID="hyp_history" CssClass="historia" Target="new"></asp:HyperLink>
</div>
