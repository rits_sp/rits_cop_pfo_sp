﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_headerBackOfficeZone.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_headerBackOfficeZone" %>


<div class="config-container" id="div_backoffice_Admin" runat="server" Visible="False">
    <div class="config" runat="server" id="div_backoffice_Logout" clientidmode="Static" visible="False">
        <button type="submit" runat="server" onserverclick="btn_Logout_OnClick" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"><i class="fa fa-power-off"></i></button>
    </div>
    <div class="mdl-tooltip" for="div_backoffice_Logout">
        Logout
    </div>
    <div class="config" runat="server" id="div_backoffice_Settings" clientidmode="Static" visible="False">
        <a href="<%=UrlSettings %>" target="new" class="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored"><i class="fa fa-cog"></i></a>
    </div>
    <div class="mdl-tooltip" for="div_backoffice_Settings">
        Backoffice
    </div>
    <div class="config" runat="server" id="div_backoffice_NofiticationsContacts" clientidmode="Static" visible="False">
        <a href="/Lists/COP_PFO_Contactos/AllItems.aspx" target="new" class="notificationsicon">
            <span  class="material-icons mdl-badge" data-badge="<%=NumberContactsNotReview %>"><i class="fa fa-commenting"></i></span>
        </a>
    </div>
    <div class="mdl-tooltip" for="div_backoffice_NofiticationsContacts">
        Notificações de Contactos
    </div>

    <div class="config" runat="server" id="div_backoffice_NofiticationsTS" clientidmode="Static" visible="False">
        <a href="/Lists/COP_PFO_Contactos/AllItems.aspx" target="new" class="notificationsicon">
            <span class="material-icons mdl-badge" data-badge="15"><i class="fa fa-folder-o"></i></span>
        </a>
    </div>
    <div class="mdl-tooltip" for="div_backoffice_NofiticationsTS">
        Notificações de Trabalhos Submetidos
    </div>
   
    <div class="config" runat="server" id="div_backoffice_UserName" clientidmode="Static" visible="False">
        <i class="fa fa-user" aria-hidden="true"></i>
        <asp:Label runat="server" ID="lbl_Username"></asp:Label>
    </div>
</div>
