﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main
{
    public partial class UC_headerSocial : UserControl
    {
        DataTable dt_menus = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            LoadFields();
        }

        public void LoadFields()
        {
            try
            {
                //Referencias
                string _url = IFL.Core.Constants.Urls._formacao;
                string _list = IFL.Core.Constants.Lists._cop_pfo_Navigation;
                string _menu = IFL.Core.Constants.Menus._menu_Social;

                dt_menus = IFL.Core.LoadObjects.LoadMenu(_url, _list, _menu, 8);

                // botão Facebook
                var btn_hyp_Facebook = IFL.Core.LoadObjects.GetParameter("hyp_Facebook", dt_menus);
             
                hyp_Facebook.NavigateUrl = btn_hyp_Facebook.Link ?? @"/";
                hyp_Facebook.Visible = Convert.ToBoolean(btn_hyp_Facebook.Visible);
                hyp_Facebook.Enabled = Convert.ToBoolean(btn_hyp_Facebook.Enable);

                // botão youtube
                var btn_hyp_youtube = IFL.Core.LoadObjects.GetParameter("hyp_youtube", dt_menus);
                
                hyp_youtube.NavigateUrl = btn_hyp_youtube.Link ?? @"/";
                hyp_youtube.Visible = Convert.ToBoolean(btn_hyp_youtube.Visible);
                hyp_youtube.Enabled = Convert.ToBoolean(btn_hyp_youtube.Enable);

                // botão flickr
                var btn_hyp_flickr = IFL.Core.LoadObjects.GetParameter("hyp_flickr", dt_menus);
              
                hyp_flickr.NavigateUrl = btn_hyp_flickr.Link ?? @"/";
                hyp_flickr.Visible = Convert.ToBoolean(btn_hyp_flickr.Visible);
                hyp_flickr.Enabled = Convert.ToBoolean(btn_hyp_flickr.Enable);

                // botão linkedin
                var btn_hyp_linkedin = IFL.Core.LoadObjects.GetParameter("hyp_linkedin", dt_menus);
               
                hyp_linkedin.NavigateUrl = btn_hyp_linkedin.Link ?? @"/";
                hyp_linkedin.Visible = Convert.ToBoolean(btn_hyp_linkedin.Visible);
                hyp_linkedin.Enabled = Convert.ToBoolean(btn_hyp_linkedin.Enable);

            }
            catch (Exception ex)
            {

                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "UI - UC_headerSocial - LoadFields()", ex.Message);
            }
        }
    }
}
