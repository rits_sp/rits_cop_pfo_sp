﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_footerSocial.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_footerSocial" %>


<asp:HyperLink ID="hyp_Facebook"  runat="server" Target="new" CssClass="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" >
    <i class="fa fa-facebook"></i>
</asp:HyperLink>

<asp:HyperLink ID="hyp_youtube"  runat="server" Target="new" CssClass="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" >
    <i class="fa fa-youtube"></i>
</asp:HyperLink>

<asp:HyperLink ID="hyp_flickr"  runat="server" Target="new" CssClass="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" >
   <i class="fa fa-flickr"></i>
</asp:HyperLink>

<asp:HyperLink ID="hyp_linkedin"  runat="server" Target="new" CssClass="mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored" >
    <i class="fa fa-linkedin"></i>
</asp:HyperLink>
