﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_headerLoginArea.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_headerLoginArea" %>

<div class="login-area" ID="isUnLogged" clientidmode="Static" runat="server" Visible="False">
    <a href="/Pages/Login.aspx" id="login" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-lock"></i>LOGIN</a>
    <div class="mdl-tooltip" for="login">
        Iniciar sessão
    </div>
</div>

<div class="login-area" ID="isLogged" clientidmode="Static" runat="server" Visible="False">
  <%--  <a href="/Pages/InformacaoPessoal.aspx" id="logged" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-user"></i><%=Utilizador %></a>--%>
    <div class="mdl-tooltip" for="login">
        Perfil de Utilizador
    </div>
 <%--   <asp:Button runat="server" ID="btn_Logout" runat="server" Text="Logout" CssClass="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent cancel bt-logout" <%--OnClick="btn_Logout_OnClick"--%> Visible="False"/>--%>
</div>