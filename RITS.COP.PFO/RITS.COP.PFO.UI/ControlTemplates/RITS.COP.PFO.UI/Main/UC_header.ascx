﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_header.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_header" %>

<%@ Register Src="~/_controltemplates/15/RITS.COP.PFO.UI/Main/UC_headerMenuTop.ascx" TagPrefix="uc1" TagName="UC_headerMenuTop" %>
<%@ Register Src="~/_controltemplates/15/RITS.COP.PFO.UI/Main/UC_headerSocial.ascx" TagPrefix="uc1" TagName="UC_headerSocial" %>
<%@ Register Src="~/_controltemplates/15/RITS.COP.PFO.UI/Main/UC_headerLoginArea.ascx" TagPrefix="uc1" TagName="UC_headerLoginArea" %>
<%@ Register Src="~/_controltemplates/15/RITS.COP.PFO.UI/Main/UC_headerMainMenu.ascx" TagPrefix="uc1" TagName="UC_headerMainMenu" %>
<%@ Register Src="~/_controltemplates/15/RITS.COP.PFO.UI/Main/UC_headerBackOfficeZone.ascx" TagPrefix="uc1" TagName="UC_headerBackOfficeZone" %>





<header>
    <div id="configuration">
        <uc1:UC_headerBackOfficeZone runat="server" id="UC_headerBackOfficeZone" />
    </div>
    <!-- Navigation -->
    <div id="nav">
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="navbar-topheader">
                <div class="container">
                    <div class="row">
                        <div class="col-md-8">
                            <uc1:UC_headerMenuTop runat="server" id="UC_headerMenuTop" />
                        </div>
                        <div class="col-md-4 txt-right">
                            <div class="social-icons">
                                <uc1:UC_headerSocial runat="server" id="UC_headerSocial" />
                            </div>
                            <div class="login-area">
                                <uc1:UC_headerLoginArea runat="server" id="UC_headerLoginArea" />
                            </div>
                            <!-- Expandable Textfield -->

                            <form>
                                <div id="searchfield">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--expandable">
                                        <label id="searchfield-button" class="mdl-button mdl-js-button mdl-button--icon" for="k">
                                            <i class="zmdi zmdi-search"></i>
                                        </label>
                                        <div id="searchsubmit" class="mdl-textfield__expandable-holder">
                                            <input class="mdl-textfield__input" type="text" id="k" name="k">
                                            <label class="mdl-textfield__label" for="k">Expandable Input</label>
                                        </div>
                                    </div>
                               <button class="searchsubmitbt mdl-button mdl-js-button mdl-button--fab mdl-js-ripple-effect mdl-button--colored cancel" runat="server" onserverclick="OnServerClick"><i class="fa fa-chevron-right"></i></button>
                                </div>
                            </form>
                            <div class="mdl-tooltip" for="searchfield-button">
                                Procurar
                            </div>
                        </div>
                    </div>
                    <div class="navbar-topheader-item">
                        <a class="navbar-brand" href="/">
                            <img class="brand-normal" alt="Educação Olímpica" src="../../../../Style%20Library/cop/imgs/logo-cop-formacao-olimpica.png" />
                        </a>
                    </div>
                </div>
                <div class="navbar-topheader-bar">
                    <div class="container">Centro de Pesquisa e Desenvolvimento Desportivo</div>
                </div>
            </div>
            <div id="header-slideimage-holder"></div>
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <div class="container">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
            </div>
            <div id="menu-collapse">
                <div class="container">
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <a class="navbar-brand" href="/">
                        <img class="brand-smaller" alt="Educação Olímpica" style="display: none;" src="../../../../Style%20Library/cop/imgs/logo-cop-formacao-olimpica-h.png" />
                    </a>
                    <div class="collapse navbar-collapse" id="navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">

                            <uc1:UC_headerMainMenu runat="server" id="UC_headerMainMenu1" />

                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container -->
            </div>
            <!-- /menu-collpase -->
        </nav>
    </div>
</header>


<script type="text/javascript">

    var k = $('#k').val();

    if (k != null) {
    }

    function submitForm(containerElement) {
        var theForm = document.createElement("form");
        theForm.action = '/Pages/ResultadosPesquisa.aspx';
        theForm.method = "GET";
        theForm.innerHTML = document.getElementById(containerElement).outerHTML;
        var formToSubmit = document.body.appendChild(theForm);
        document.getElementsByName('k')[1].value = document.getElementsByName('k')[0].value;
        formToSubmit.submit();
    }

</script>
