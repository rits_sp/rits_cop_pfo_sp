﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint.Utilities;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main
{
    public partial class UC_header : UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void OnServerClick(object sender, EventArgs e)
        {
            string kk = Request.Params["k"];

            if (!string.IsNullOrEmpty(kk))
            {
                string k = string.Format("?k={0}", kk ?? string.Empty); //Termos em pesquisa
                string s = string.Format("&s={0}", "true"); // Ordenacao
                string db = string.Format("&db={0}","2000-01-01" ); // Data de Inicio
                string de = string.Format("&de={0}",DateTime.Today.ToString("yyyy-MM-dd"));// Data de Inicio
                string sa = string.Format("&sa={0}", "true"); // Atividade
                string sc = string.Format("&sc={0}", "true"); // conteudo
                string sd = string.Format("&sd={0}", "true"); // Desafio
         
                string urlPesqusisa = string.Format("{0}{1}{2}{3}{4}{5}{6}", k, s, db, de, sa, sc, sd);

                SPUtility.Redirect(IFL.Core.Constants.UrlsPages._pageSearchResults + urlPesqusisa,
                        SPRedirectFlags.DoNotEncodeUrl, this.Context);
                HttpContext.Current.Response.End();
            }
        }
    }
}

