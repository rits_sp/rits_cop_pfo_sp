﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_footerBottom.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_footerBottom" %>

<div class="col-lg-5 copyright">
    <p>&copy; COMITÉ OLÍMPICO DE PORTUGAL - Designed & Developed by <a href="http://www.rightitservices.com/" target="new"><img class="rits" src="../../../../Style%20Library/cop/imgs/icons/right-it-services-logo.png" alt="Right IT Services" /></a></p>
</div>
  <div class="col-lg-7 txt-right">
    <ul>
        <li>
            <asp:HyperLink runat="server" ID="hyp_privacyPolicy" CssClass="privacy"></asp:HyperLink>
        </li>
        <li>
             <asp:HyperLink runat="server" ID="hyp_termsConditions" CssClass="terms"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_policyCookies" CssClass="cookies"></asp:HyperLink>
        </li>
    </ul>
</div>