﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_menuFooter.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_menuFooter" %>

<div class="col-lg-8">
    <ul>
        <li>
            <asp:HyperLink runat="server" ID="hyp_Inicio"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_aboutProgram"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_resources"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_activities"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_challengers"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_values"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_schoolMaps"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_faqs"></asp:HyperLink>
        </li>
        <li>
            <asp:HyperLink runat="server" ID="hyp_contacts"></asp:HyperLink>
        </li>
    </ul>
</div>
