﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_headerMainMenu.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Main.UC_headerMainMenu" %>



<li class="responsive-item">
    <a href="/">Início</a>
</li>
<li class="item formations">
    <asp:HyperLink runat="server" ID="hyp_formations"></asp:HyperLink>
</li>
<li class="item publications">
    <asp:HyperLink runat="server" ID="hyp_publications"></asp:HyperLink>
</li>
<li class="item copawards">
    <asp:HyperLink runat="server" ID="hyp_copawards"></asp:HyperLink>
</li>
<li class="item highlights">
    <asp:HyperLink runat="server" ID="hyp_highlights"></asp:HyperLink>
</li>
<li class="item agenda">
    <asp:HyperLink runat="server" ID="hyp_agenda"></asp:HyperLink>
</li>
<li class="responsive-item faqs">
    <a href="/Pages/FAQS.aspx">FAQs</a>
</li>
<li class="responsive-item submit">
    <a href="/Pages/SubmeterPublicacao.aspx">Submeter Publicação</a>
</li>
<li class="responsive-item contacts">
    <a href="/Pages/Contactos.aspx">Contatos</a>
</li>
<li class="responsive-item privacy">
    <a href="/Pages/PoliticaPrivacidade.aspx">Política de Privacidade</a>
</li>
<li class="responsive-item terms">
    <a href="/Pages/TermosCondicoes.aspx">Termos e Condições</a>
</li>
<li class="responsive-item cookies">
    <a href="/Pages/PoliticaCookies.aspx">Política de Uso de Cookies</a>
</li>
<li class="responsive-item search">
    <a href="/Pages/ResultadosPesquisa.aspx?k=%20&s=true">Pesquisa</a>
</li>


