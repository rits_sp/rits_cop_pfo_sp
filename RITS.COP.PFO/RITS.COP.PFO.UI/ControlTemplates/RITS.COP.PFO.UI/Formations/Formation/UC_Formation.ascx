﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Formation.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Formations.Formation.UC_Formation" %>

<section>

    <div class="slider-container">
        <!-- Header Carousel -->
        <!-- START REVOLUTION SLIDER 5.0 -->
        <div class="rev_slider_wrapper">
            <div id="rev_slider_inside" class="rev_slider" data-version="5.0">
                <ul>
                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="../../../../_layouts/15/images/RITS.COP.BR/PHO10432129.retouche.jpg" alt="" width="1920" height="1080" data-bgposition="center center" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption News-Title"
                            data-x="left" data-hoffset="80"
                            data-y="bottom" data-voffset="150"
                            data-whitespace="normal"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="500"
                            data-bgposition="center center">
                            <div class="insidepages">
                                <h1 class="page-header">Formações</h1>
                            </div>
                        </div>
                    </li>
                    <%--<li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="../../../../_layouts/15/images/RITS.COP.BR/PHO10816520.retouche.jpg" alt="<%TitleActivities%>" width="1920" height="1080" data-bgposition="center center" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption News-Title"
                            data-x="left" data-hoffset="80"
                            data-y="bottom" data-voffset="150"
                            data-whitespace="normal"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="500"
                            data-bgposition="center center">
                            <div class="insidepages">
                                <h2 class="page-header"><%=LblActivities%></h2>
                            </div>
                        </div>
                    </li>--%>

                </ul>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>
        <!-- END OF SLIDER WRAPPER -->
    </div>
</section>

<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_activities"></asp:HyperLink></li>
                    <li class="active">LblFormation</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- /.container -->
<section>
    <%--<div class="container <%=StateAct%>">--%>
    <div class="container Publicado">
        <div class="row preview">
            <div class="col-lg-8">
               <%-- <a href="<%=EditCore %>" target="_blank" id="edit-texts" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Textos</a>--%>
                 <a href="" target="_blank" id="edit-texts" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Textos</a>
                <div class="mdl-tooltip" for="edit-texts">
                    Editar o Título, Descrição, Texto e Tags desta Formação
                </div>
            </div>
            <div class="col-lg-8">
                <h1>TitleFormation</h1>
                <div class="introtext">DescriptionFormation</div>
                <div class="contenttext">TextFormation</div>
                <div class="section-container">
                    <div class="row">
                        <div class="col-md-8">
                            <h3 class="section-title"><i class="fa fa-picture-o"></i>LblImages</h3>
                        </div>
                        <div class="col-md-4">
                            <%--<a href="<%=EditImages%>" target="_blank" id="edit-images" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Imagens</a>--%>
                            <a href="" target="_blank" id="edit-images" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Imagens</a>
                            <div class="mdl-tooltip" for="edit-images">
                                Editar as Imagens desta Formação
                            </div>
                        </div>
                    </div>
                    <div class="row contentshere">
                        <ul class="thumbnails" id="images-gallery">
                                    <li class="span">
                                        <a href="../../../../../Style%20Library/cop/imgs/IMG_7852.jpg" class="thumbnail image-thumbnail" data-title="yellow">
                                            <span class="img-container" style="background-image: url(../../../../../Style%20Library/cop/imgs/IMG_7852.jpg);"></span>
                                        </a>
                                    </li>
                            <%--<asp:Repeater ID="rptImages" runat="server">
                                <ItemTemplate>
                                    <li class="span">
                                        <a href="<%# DataBinder.Eval (Container.DataItem, "FileRef") %>" class="thumbnail image-thumbnail" data-title="<%# DataBinder.Eval (Container.DataItem, "Title") %>">
                                            <span class="img-container" style="background-image: url(<%# DataBinder.Eval (Container.DataItem, "FileRef") %>);"></span>
                                        </a>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>--%>
                        </ul>
                    </div>
                </div>
                <div class="section-container">
                    <div class="row">
                        <div class="col-md-8">
                            <h3 class="section-title"><i class="zmdi zmdi-videocam"></i>lblVideos</h3>
                        </div>
                        <div class="col-md-4">
                            <%--<a href="<%=EditsVideos%>" target="_blank" id="edit-videos" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Vídeos</a>--%>
                            <a href="" target="_blank" id="edit-videos" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Vídeos</a>
                            <div class="mdl-tooltip" for="edit-videos">
                                Editar os Vídeos desta Formação
                            </div>
                        </div>
                    </div>
                    <div class="row contentshere">
                        <ul class="thumbnails" id="videos-gallery">

                                    <li class="span">
                                        <a href='http://rd3.videos.sapo.pt/playhtml?file=http://rd3.videos.sapo.pt/yuF2Bluk5wW0QHDbjjg8/mov/1' class="ilightbox video thumbnail" data-type="iframe" data-options="width: 638, height: 360"><i class="fa fa-play-circle-o"></i>
                                            <img class="video-thumbnail" src="../../../../../Style%20Library/cop/imgs/IMG_7852.jpg" />
                                        </a>
                                    </li>

                            <%--<asp:Repeater ID="rptvideos" runat="server">
                                <ItemTemplate>
                                    <li class="span">
                                        <a href='<%# DataBinder.Eval (Container.DataItem, "Link") %>' class="ilightbox video thumbnail" data-type="iframe" data-options="width: 638, height: 360"><i class="fa fa-play-circle-o"></i>
                                            <img class="video-thumbnail" src="<%# DataBinder.Eval (Container.DataItem, "LinkSrc") %>" />
                                        </a>
                                    </li>
                                </ItemTemplate>
                            </asp:Repeater>--%>
                        </ul>
                    </div>
                </div>
                <div class="section-container">
                    <div class="row">
                        <div class="col-md-8">
                            <h3 class="section-title"><i class="zmdi zmdi-link"></i>lblLinks</h3>
                        </div>
                        <div class="col-md-4">
                            <%--<a href="<%=EditExternalLinks%>" target="_blank" id="edit-links" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Links</a>--%>
                            <a href="" target="_blank" id="edit-links" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Links</a>
                            <div class="mdl-tooltip" for="edit-links">
                                Editar os Links Externos desta Formação
                            </div>
                        </div>
                    </div>
                    <div class="row contentshere linkz">
                                <a class="external-links" href="http://www.sapo.pt" target="new" rel="nofollow">http://www.sapo.pt</a>
                        <%--<asp:Repeater ID="rptLinks" runat="server">
                            <ItemTemplate>
                                <a class="external-links" href="<%# DataBinder.Eval (Container.DataItem, "Link") %>" target="new" rel="nofollow"><%# DataBinder.Eval (Container.DataItem, "Link") %></a>
                            </ItemTemplate>
                        </asp:Repeater>--%>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div id="div_btn_Edit" runat="server" visible="False">
                    <button id="edit-button" onclick="isForReview(); return false;" type="submit" value="botão temporário" class="notForReview mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-green bt-save">Editar</button>
                </div>
                <div class="buttons-preview">
                    <%--<asp:Button runat="server" ID="btn_Publishing" OnClick="btn_Publishing_OnClick" CssClass="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-green" Text="Publicar" />--%>
                    <button onclick="btn_Publishing_OnClick" classs="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-green">Publicar</button>
                    <button id="refresh" onclick="window.location.reload();" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-blue-dark bt-refresh">Atualizar</button>
                    <a href="/Pages/Atividades.aspx" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-green bt-refresh">Sair do modo de Edição</a>
                </div>
                <div class="buttons-published">
                    <a onclick="window.location.reload();" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-green bt-refresh">Sair do modo de Edição</a>
                </div>

                <div class="mdl-card mdl-shadow--2dp categorybox activities bg-gray-light">
                    <div class="row">
                        <div class="mdl-card__title toptitle bg-blue-dark">
                            <h2 class="mdl-card__title-text">Publicação Lorem Ipsum</h2>
                        </div>
                    </div>
                    <div class="row">
                        <%--<div class="mdl-card__image" style="background-image: url(<%=Linkimagempref%>)"></div>--%>
                        <div class="mdl-card__image" style="background-image: url(../../../Style%20Library/cop/imgs/PHO10354409.retouche.jpg)"></div>
                    </div>
                    <div class="row">
                        <div class="mdl-card__title">
                            <h2 class="mdl-card__title-text txt-gray-dark"><i class="fa fa-tags"></i>Tags</h2>
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                            <div class="row">
                                <div class="col-lg-12 tags">

                                    <%--<%=Tags%>--%>
                                    <p class="tag">Formações Avançadas</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="section-container">
                        <div class="row">
                            <div class="mdl-card__title">
                                <h2 class="mdl-card__title-text txt-gray-dark"><i class="fa fa-file-pdf-o"></i>lblAttachments</h2>
                            </div>
                            <div class="mdl-card__actions mdl-card--border attachhere">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <a href="http://www.eduolimpica.comiteolimpicoportugal.pt/Atividades/COP_Anexos_Atividades/VXUtg4KKtkqluGL4AhA2Dg/Gloss%C3%A1rio%20de%20Termos%20Ol%C3%ADmpicos_VF2.pdf" target="new" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                                            <i class="fa fa-file-pdf-o"></i>Loremipsum
                                        </a>
                                    </div>
                                </div>
                                <%--<asp:Repeater ID="rptDocuments" runat="server">
                                    <ItemTemplate>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <a href="<%# DataBinder.Eval (Container.DataItem, "Link") %>" target="new" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                                                    <i class="fa fa-file-pdf-o"></i><%# DataBinder.Eval (Container.DataItem, "Title") %>
                                                </a>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>--%>

                            </div>
                        </div>
                    </div>
                </div>
                <%--<a href="<%=EditAttachments%>" target="_blank" id="edit-attachtments" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Anexos</a>--%>
                <a href="" target="_blank" id="edit-attachtments" class="forReview hide mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"><i class="fa fa-pencil"></i>Editar Anexos</a>
                <div class="mdl-tooltip" for="edit-attachtments">
                    Editar os Anexos desta Atividade
                </div>

            </div>
        </div>
    </div>
</section>
<section>
    <div class="forReview modalview hide"></div>
</section>
<script>
    $('.nav').find('.item.formations').addClass('selected');

    function isForReview() {

        $('.forReview').removeClass('hide');
        $('.notForReview').addClass('hide');
        $('.section-container').show();
    }

    $(document).ready(function () {
        $('footer li a.formations').addClass('selected');
        //if section is emtpy, hide it
        if (!$.trim($('#images-gallery').html()).length) {
            $('#images-gallery').parents('.section-container').hide();
        }
        if (!$.trim($('#videos-gallery').html()).length) {
            $('#videos-gallery').parents('.section-container').hide();
        }
        if (!$.trim($('#hangouts-gallery').html()).length) {
            $('#hangouts-gallery').parents('.section-container').hide();
        }
        if (!$.trim($('.linkz').html()).length) {
            $('.linkz').parents('.section-container').hide();
        }
        if (!$.trim($('.attachhere').html()).length) {
            $('.attachhere').parents('.section-container').hide();
        }
        //

    });
</script>