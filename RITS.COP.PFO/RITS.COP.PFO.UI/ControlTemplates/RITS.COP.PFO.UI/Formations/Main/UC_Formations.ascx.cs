﻿using RITS.COP.PFO.IFL.Core;
using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.Formations.Main
{
    public partial class UC_Formations : UserControl
    {
         public string Url { get; set; }
        public string ListText { get; set; }
        public string LBL_Formation { get; set; }

        public string TXT_Text_Formation { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                Url = Constants.Urls._formacao;
                ListText = Constants.Lists._cop_pfo_Text;
                LoadFields();
                LoadText();
            }
            catch (Exception ex)
            {

            }
        }

        public void LoadFields()
        {
            string _menu = IFL.Core.Constants.Menus._menu_Header_Top;

            var dt_menus = IFL.Core.LoadObjects.LoadMenu(Url, IFL.Core.Constants.Lists._cop_pfo_Navigation, _menu, 15);

            // HyperLink - inicio
            var btn_hyp_start = IFL.Core.LoadObjects.GetParameter("hyp_start", dt_menus);
            hyp_start.Text = btn_hyp_start.Value ?? string.Empty;
            hyp_start.NavigateUrl = btn_hyp_start.Link ?? @"/";
            hyp_start.Visible = Convert.ToBoolean(btn_hyp_start.Visible);
            hyp_start.Enabled = Convert.ToBoolean(btn_hyp_start.Enable);

            LBL_Formation = ResourcesHelper.GetStringFromResource("PFO", "Formation", "PT");
        }

        public void LoadText()
        {
            TXT_Text_Formation = LoadObjects.LoadTextPage(Url, ListText, "Texto_Publicacoes_Formation", 2) ?? string.Empty;
        }
        
    }
}
