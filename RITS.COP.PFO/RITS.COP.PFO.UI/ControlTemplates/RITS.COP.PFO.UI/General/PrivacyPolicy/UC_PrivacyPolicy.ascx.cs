﻿using System;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.PrivacyPolicy
{
    public partial class UC_PrivacyPolicy : UserControl
    {
        #region Variables

        public string Text { get; set; }
        public string LblPrivacityPolicy { get; set; }
        public string MenuTop { get; set; }

        string _url = IFL.Core.Constants.Urls._formacao;
        string _list = IFL.Core.Constants.Lists._cop_pfo_Text;


        #endregion
       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                MenuTop = IFL.Core.Constants.Menus._menu_Header_Top;

                GetFields();
                //SEO
                string tl = "Política de Privacidade - Formação Olímpica";
                string kw = "Privacidade, COP";
                string dc = "Política de Privacidade do Programa de Formação Olímpica.";
                IFL.Core.TagsSEO.ApplyTagsCSharp(tl, kw, dc, this.Page);
                //

            }
       
            
             catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP001", "UI.UC_PrivacityPolicy.Page_Load", ex.Message);
                HttpContext.Current.Response.Redirect(_url + Constants.UrlsPages._pageError, false);
            }

        }

        protected void GetFields()
        {
            var dt_menus = LoadObjects.LoadMenu(_url, Constants.Lists._cop_pfo_Navigation, MenuTop, 15);

            // HyperLink - inicio
            var btn_hyp_start = IFL.Core.LoadObjects.GetParameter("hyp_start", dt_menus);
            hyp_start.Text = btn_hyp_start.Value ?? string.Empty;
            hyp_start.NavigateUrl = btn_hyp_start.Link ?? @"/";
            hyp_start.Visible = Convert.ToBoolean(btn_hyp_start.Visible);
            hyp_start.Enabled = Convert.ToBoolean(btn_hyp_start.Enable);

            //textos da pagina
            Text = LoadObjects.LoadTextPage(_url, _list, "txt_PrivacityPolicy", 2);

            //label da pagina
            LblPrivacityPolicy = ResourcesHelper.GetStringFromResource("PFO", "PrivacityPolicy", "PT");
        }
    }
}
