﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Utilities;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.SearchResults
{
    public partial class UC_SearchResults : UserControl
    {
     
        public string SearchResults { get; set; }
        public string KeySearch { get; set; }

        public string Url { get; set; }
        protected void Page_Load(object sender, EventArgs e)
        {
            //try
            //{
            //    GetFields();
            //   //  Url = Constants.Urls._program;
            //    if (!Page.IsPostBack)
            //    {
            //        string text;
            //        bool isActivity;
            //        bool isChallange;
            //        bool isContent;
            //        bool isAscending;
            //        string dtInicio;
            //        string dtFim;

            //        string parameterQueryString = Request.QueryString["k"];

            //        string parameterSort = Request.QueryString["s"];

            //        if (String.IsNullOrEmpty(parameterSort))
            //        {
            //            string k = string.Format("?k={0}", parameterQueryString ?? string.Empty); //Termos em pesquisa
            //            string s = string.Format("&s={0}", hf_Sort.Value ?? "true"); // Ordenacao
            //            string db = string.Format("&db={0}",
            //                hf_Inicio.Value == "Data de Início" ? "2000-01-01" : hf_Inicio.Value); // Data de Inicio
            //            string de = string.Format("&de={0}",
            //                hf_Fim.Value == "Data de Fim" ? DateTime.Today.ToString("yyyy-MM-dd") : hf_Fim.Value);
            //            // Data de Inicio
            //            string sa = string.Format("&sa={0}", hf_isActividades.Value ?? "true"); // Atividade
            //            string sc = string.Format("&sc={0}", hf_isConteudos.Value ?? "true"); // conteudo
            //            string sd = string.Format("&sd={0}", hf_isDesafios.Value ?? "true"); // Desafio

            //            string urlPesqusisa = string.Format("{0}{1}{2}{3}{4}{5}{6}", k, s, db, de, sa, sc, sd);

            //            SPUtility.Redirect(IFL.Core.Constants.UrlsPages._pageSearchResults + urlPesqusisa,
            //                SPRedirectFlags.DoNotEncodeUrl, this.Context);
            //            HttpContext.Current.Response.End();
            //        }

            //        Page.ClientScript.RegisterStartupScript(this.GetType(), "btnExecuteQuery", "btnExecuteQuery()", true);

            //        if (parameterQueryString != null)
            //        {
            //            txt_Search.Text = parameterQueryString;
            //            text = txt_Search.Text ?? hf_Text.Value;
            //            dtInicio =
            //                hf_Inicio.Value = Request.QueryString["db"] == "" ? "1999-01-01" : Request.QueryString["db"];
            //            dtFim =
            //                hf_Fim.Value =
            //                    Request.QueryString["de"] == ""
            //                        ? DateTime.Today.ToString("yyyy-MM-dd")
            //                        : Request.QueryString["de"];
            //            isActivity =
            //                Convert.ToBoolean(Request.QueryString["sa"] == "" ? "true" : Request.QueryString["sa"]);
            //            isChallange =
            //                Convert.ToBoolean(Request.QueryString["sd"] == "" ? "true" : Request.QueryString["sd"]);
            //            isContent =
            //                Convert.ToBoolean(Request.QueryString["sc"] == "" ? "true" : Request.QueryString["sc"]);
            //            isAscending =
            //                Convert.ToBoolean(Request.QueryString["s"] == "" ? "true" : Request.QueryString["s"]);

            //            DataTable dtResults = LoadObjects.GetSearchResults(isActivity, isChallange, isContent, text,
            //                dtInicio, dtFim, isAscending, 50);

            //            if (dtResults != null && dtResults.Rows.Count > 0)
            //            {
            //                rptResults.DataSource = dtResults;
            //                rptResults.DataBind();
            //            }
            //        }
            //    }
            //}

            //catch
            //    (Exception ex)
            //{
            //    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP001", "UI.UC_SearchResults.Page_Load", ex.Message);
            //    HttpContext.Current.Response.Redirect(Url + Constants.UrlsPages._pageError, false);

            //}

        }

        public void GetFields()
        {
            txt_DataInicio.Text = "Data de Início";
            txt_DataFim.Text = "Data de Fim";
        }

        protected void btn_SubmeterPesquisa_OnClick(object sender, EventArgs e)
        {

            ////  " ?k=maria&s=true&db=22-01-2010&de=25-03-2013&sa=true&sc=true&sd=false";

            //string k = string.Format("?k={0}", hf_Text.Value ?? string.Empty); //Termos em pesquisa
            //string s = string.Format("&s={0}", hf_Sort.Value ?? "true"); // Ordenacao
            //string db = string.Format("&db={0}", hf_Inicio.Value == "Data de Início" ? "2000-01-01" : hf_Inicio.Value); // Data de Inicio
            //string de = string.Format("&de={0}", hf_Fim.Value == "Data de Fim" ? DateTime.Today.ToString("yyyy-MM-dd") : hf_Fim.Value); // Data de Inicio
            //string sa = string.Format("&sa={0}", hf_isActividades.Value ?? "true"); // Atividade
            //string sc = string.Format("&sc={0}", hf_isConteudos.Value ?? "true"); // conteudo
            //string sd = string.Format("&sd={0}", hf_isDesafios.Value ?? "true"); // Desafio

            //string urlPesqusisa = string.Format("{0}{1}{2}{3}{4}{5}{6}", k, s, db, de, sa, sc, sd);

            //if (k.Length > 0)
            //{
            //    SPUtility.Redirect(IFL.Core.Constants.UrlsPages._pageSearchResults + urlPesqusisa,
            //        SPRedirectFlags.DoNotEncodeUrl, this.Context);
            //    HttpContext.Current.Response.End();
            //}
            //else
            //{
            //    SPUtility.Redirect(IFL.Core.Constants.UrlsPages._pageSearchResults,
            //       SPRedirectFlags.DoNotEncodeUrl, this.Context);
            //    HttpContext.Current.Response.End();
            //}
        }
    }
}
