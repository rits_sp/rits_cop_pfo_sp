﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_SearchResults.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.SearchResults.UC_SearchResults" %>


<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="/">Início</a></li>
                    <li>Pesquisa</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- /.container -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <h3 class="section-title"><i class="fa fa-search"></i>Pesquisa</h3>
                </div>
                <!-- comments table -->
                <div class="row">
                    <!-- Expandable Textfield -->
                    <div id="search" class="mdl-card mdl-shadow--2dp searchbox bg-gray-light search-results">
                        <div class="row">
                            <div class="col-md-8">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <asp:TextBox MaxLength="20" TextMode="SingleLine" runat="server" ID="txt_Search" CssClass="textfield__input mdl-textfield__input" ClientIDMode="Static"></asp:TextBox>
                                    <label class="mdl-textfield__label" for="txt_Search">Pesquisar termo</label>
                                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="hf_Text" />
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="col-md-4 mdl-list__item">
                                    <span class="mdl-list__item-primary-content">Conteúdos
                                    </span>
                                    <span class="mdl-list__item-secondary-action">
                                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="chk_Conteudos">
                                            <input type="checkbox" id="chk_Conteudos" class="mdl-switch__input" checked />
                                            <asp:HiddenField runat="server" ClientIDMode="Static" ID="hf_isConteudos" />
                                        </label>
                                    </span>
                                </div>
                                <div class="col-md-4 mdl-list__item">
                                    <span class="mdl-list__item-primary-content">Atividades
                                    </span>
                                    <span class="mdl-list__item-secondary-action">
                                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="chk_Actividades">
                                            <input type="checkbox" id="chk_Actividades" class="mdl-switch__input" checked />
                                            <asp:HiddenField runat="server" ClientIDMode="Static" ID="hf_isActividades" />
                                        </label>
                                    </span>
                                </div>
                                <div class="col-md-4 mdl-list__item">
                                    <span class="mdl-list__item-primary-content">Desafios
                                    </span>
                                    <span class="mdl-list__item-secondary-action">
                                        <label class="mdl-switch mdl-js-switch mdl-js-ripple-effect" for="chk_Desafios">
                                            <input type="checkbox" id="chk_Desafios" class="mdl-switch__input" checked />
                                            <asp:HiddenField runat="server" ClientIDMode="Static" ID="hf_isDesafios" />
                                        </label>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <asp:DropDownList ID="DropDownList1" ClientIDMode="Static" CssClass="mdl-textfield__input" runat="server" AutoPostBack="False" AppendDataBoundItems="true">
                                        <asp:ListItem Text="Ascendente" Value="true" />
                                        <asp:ListItem Text="Descendente" Value="false" />
                                    </asp:DropDownList>
                                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="hf_Sort" />
                                    <label class="mdl-textfield__label is-dirty" for="DropDownList1">Organizar por data</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-daterange input-group" id="datepicker">
                                    <asp:TextBox CssClass="input-md form-control" runat="server" ID="txt_DataInicio" name="start" ClientIDMode="Static"></asp:TextBox>
                                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="hf_Inicio" />
                                    <span class="input-group-addon">a</span>
                                    <asp:TextBox CssClass="input-md form-control" runat="server" ID="txt_DataFim" name="end" ClientIDMode="Static"></asp:TextBox>
                                    <asp:HiddenField runat="server" ClientIDMode="Static" ID="hf_Fim" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <asp:Button ID="btn_SubmeterPesquisa" OnClick="btn_SubmeterPesquisa_OnClick" OnClientClick="btnExecuteQuery();" runat="server" ClientIDMode="Static" CssClass="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent" Text="Pesquisar" />
                                <asp:HiddenField runat="server" ClientIDMode="Static" ID="siteUrlPageResult" />
                            </div>
                        </div>
                    </div>
                    <div class="mdl-tooltip" for="searchfield-button">
                        Procurar
                    </div>
                </div>
                <!-- search results table -->
                <div class="row">
                    <h4 class="section-title">Resultados</h4>
                </div>

                <div class="row">
                    <table id="search-table" class="display responsive no-wrap mdl-shadow--2dp bg-gray-light" cellspacing="0" width="100%">
                        <thead>
                            <tr class="bg-gray-dark txt-white">
                                <th id="search-table-title" class="min-mobile-p"><i class="zmdi zmdi-format-size"></i>Tipo<div class="mdl-tooltip" for="ttitle">Organizar por Tipo</div>
                                </th>
                                <th id="search-table-title" class="min-mobile-p"><i class="zmdi zmdi-format-size"></i>Título<div class="mdl-tooltip" for="ttitle">Organizar por Título</div>
                                </th>
                                <th id="search-table-tags" class="min-desktop"><i class="zmdi zmdi-comment-list"></i>Tags<div class="mdl-tooltip" for="tcomment">Organizar por Tags</div>
                                </th>
                                <th id="search-table-data" class="min-tablet-l"><i class="fa fa-calendar"></i>Data<div class="mdl-tooltip" for="tdata">Organizar por Data</div>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <asp:Repeater ID="rptResults" runat="server" EnableViewState="True">
                                <ItemTemplate>
                                    <tr class="search-for <%# DataBinder.Eval (Container.DataItem, "TypeResult") %>">
                                        <td class="type">
                                            <div class="search-type-title">
                                                <i class="fa fa-clipboard"></i><i class="zmdi zmdi-run"></i><i class="fa fa-bullseye"></i>
                                                <h5><%# DataBinder.Eval (Container.DataItem, "Type") %></h5>
                                            </div>
                                        </td>
                                        <td class="search-title"><a href="<%# DataBinder.Eval (Container.DataItem, "Link") %>" target="new"><%# DataBinder.Eval (Container.DataItem, "Title") %></a></td>
                                        <td class="search-tags">
                                            <div class="tag"><%# DataBinder.Eval (Container.DataItem, "Tags") %></div>
                                        </td>
                                        <td class="search-data"><%# DataBinder.Eval (Container.DataItem, "Date", "{0:d/M/yyyy}") %></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:Repeater>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="../../../../Style%20Library/cop/js/datepicker/bootstrap-datepicker.min.js"></script>
<script src="../../../../Style%20Library/cop/js/datepicker/bootstrap-datepicker.pt.min.js"></script>
<script src="../../../../Style%20Library/cop/js/datatable/datatables.min.js" type="text/javascript"></script>
<script src="../../../../Style%20Library/cop/js/peo-search.js"></script>
<script src="../../../../Style%20Library/cop/js/cookies/jquery.cookie.js"></script>
<script src="../../../../Style%20Library/cop/js/cookies/jquery.cookie.min.js"></script>

<script>

    function getUrlVars() {
        var vars = [], hash;
        var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
        for (var i = 0; i < hashes.length; i++) {
            hash = hashes[i].split('=');
            vars.push(hash[0]);
            vars[hash[0]] = hash[1];
        }
        return vars;
    }

    window.onload = function fieldFields() {
        $('#txt_Search').val(decodeURIComponent(getUrlVars()["k"]));
        $('#DropDownList1').val(getUrlVars()["s"]);
        $('#txt_DataFim').val(getUrlVars()["de"]);
        $('#txt_DataInicio').val(getUrlVars()["db"]);

        var isConteudos = getUrlVars()["sc"];

        if (isConteudos !== "false") {
            $('#chk_Conteudos').parent().addClass('is-checked');
        } else {
            $('#chk_Conteudos').parent().removeClass('is-checked');
        }

        var isAtividades = getUrlVars()["sa"];

        if (isAtividades !== "false") {
            $('#chk_Actividades').parent().addClass('is-checked');
        } else {
            $('#chk_Actividades').parent().removeClass('is-checked');
        }

        var isDesafios = getUrlVars()["sd"];

        if (isDesafios !== "false") {
            $('#chk_Desafios').parent().addClass('is-checked');
        } else {
            $('#chk_Desafios').parent().removeClass('is-checked');
        }
    }

    function btnExecuteQuery() {
        $('#hf_isConteudos').val($('#chk_Conteudos').parent().hasClass('is-checked'));
        $('#hf_isActividades').val($('#chk_Actividades').parent().hasClass('is-checked'));
        $('#hf_isDesafios').val($('#chk_Desafios').parent().hasClass('is-checked'));
        $('#hf_Sort').val($('#DropDownList1').val());
        $('#hf_Fim').val($('#txt_DataFim').val());
        $('#hf_Inicio').val($('#txt_DataInicio').val());
        $('#hf_Text').val($('#txt_Search').val());
    }

    $('#datepicker.input-daterange').datepicker({
        format: "yyyy-mm-dd",
        todayBtn: true,
        language: "pt",
        calendarWeeks: true,
        autoclose: true,
        todayHighlight: true,
        orientation: "bottom auto"
    });

    $("select").change(function () {
        if ($('#' + $(this).attr("id") + ' option:selected').val() === '') {
            $(this).parent('div').removeClass('is-dirty');
        } else {
            $(this).parent('div').addClass('is-dirty');
        }
    });

    if (!$.trim($('.alert span').html()).length) {
        $('.alert').remove();
    } else {
        $('#search-table_wrapper').hide();
    }
</script>

