﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_FAQs.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.FAQS.UC_FAQs" %>



<div>
     <section>
      <div class="slider-container">
      <!-- Header Carousel -->
          <!-- START REVOLUTION SLIDER 5.0 -->
        <div class="rev_slider_wrapper">
         <div id="rev_slider_inside" class="rev_slider"  data-version="5.0">
          <ul>    
           <li data-transition="fade">      
             <!-- MAIN IMAGE -->
             <img src="../../../../_layouts/15/images/RITS.COP.PFO.BR/PHO10732330.retouche.jpg"  alt=""  width="1920" height="1080" data-bgposition="center center" data-bgparallax="2" class="rev-slidebg" data-no-retina>            
             <!-- LAYER NR. 1 -->
             <div class="tp-caption News-Title"               
                data-x="left" data-hoffset="80" 
                data-y="bottom" data-voffset="150"               
                data-whitespace="normal"
                data-transform_idle="o:1;"      
                data-transform_in="o:0" 
                data-transform_out="o:0"               
                data-start="500"
                data-bgposition="center center">
                      <div class="insidepages">
                          <h1 class="page-header"><%=LBL_FAQS%></h1>
                      </div>
                </div>
           </li>
          </ul>       
         </div><!-- END REVOLUTION SLIDER -->
        </div><!-- END OF SLIDER WRAPPER -->
      </div>
    </section>
    <section>
        <!-- Page Content -->
        <div class="container">
            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                       <li><asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                        <li class="active"><%=LBL_FAQS%></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <!--div class="row">
                <div class="col-lg-8">
                    <h2>FAQs</h2>
                    <p class="introtext">Nullam cursus lacinia erat. Cras sagittis. Ut id nisl quis enim dignissim sagittis. Maecenas ullamcorper, dui et placerat feugiat, eros pede varius nisi, condimentum viverra felis nunc et lorem.<br>Cras non dolor. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Donec id justo. Cras id dui.<br>Cras ultricies mi eu turpis hendrerit fringilla. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Fusce risus nisl, viverra et, tempor et, pretium in, sapien. Proin magna.</p>
                </div>
                <div class="col-lg-4">
                </div>
        </div-->
            <div class="row">
                <div class="col-lg-12">
                    <div class="row">
                        <h3 class="section-title wow fadeInDown" data-wow-delay="0.4s"><i class="zmdi zmdi-pin-help"></i><%=LBL_FAQS%></h3>
                    </div>
                    <div class="row">
                        <div class="panel-group wow fadeInLeft" id="accordion">
                            <asp:Repeater ID="rptFaqs" runat="server">
                                <ItemTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h4 class="panel-title">
                                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#<%# DataBinder.Eval (Container.DataItem, "ID") %>"><%# DataBinder.Eval (Container.DataItem, "Question") %></a>
                                            </h4>
                                        </div>
                                        <div id="<%# DataBinder.Eval (Container.DataItem, "ID") %>" class="panel-collapse collapse">
                                            <div class="panel-body">
                                                <%# DataBinder.Eval (Container.DataItem, "Answer") %>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                        <!-- /.panel-group -->
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<script>
    $('.navbar-topheader-item').find('.faqs a').addClass('selected');
    $(document).ready(function () {
        $('footer li a.faqs').addClass('selected');
    });
</script>

