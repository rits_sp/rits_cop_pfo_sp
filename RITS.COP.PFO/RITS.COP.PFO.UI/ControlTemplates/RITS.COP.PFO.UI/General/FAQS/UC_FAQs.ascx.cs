﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint.Client;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.FAQS
{
    public partial class UC_FAQs : UserControl
    {
        public string Url { get; set; }
        public string List { get; set; }
        public string LBL_FAQS { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Url = IFL.Core.Constants.Urls._formacao;
                List = IFL.Core.Constants.Lists._cop_pfo_Faqs;

                LoadFields();
                GetFaqs();
                
                //SEO
                string tl = "FAQs - Formação Olímpica";
                string kw = "Programa de Formação Olímpica, FAQ, COP";
                string dc = "Perguntas frequentes sobre o Programa de Formação Olímpica.";
                IFL.Core.TagsSEO.ApplyTagsCSharp(tl, kw, dc, this.Page);
            }
            catch (Exception ex)
            {

                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP001", "UI.UC_FAQs.Page_Load", ex.Message);
                HttpContext.Current.Response.Redirect(Url + IFL.Core.Constants.UrlsPages._pageError, false);
            }

        }

        protected void GetFaqs()
        {
            DataTable dt = LoadObjects.LoadFaqs(Url, List, 100);

            if (dt.Rows.Count > 0)
            {
                rptFaqs.DataSource = dt;
                rptFaqs.DataBind();
            }
        }

        public void LoadFields()
        {
            string _menu = IFL.Core.Constants.Menus._menu_Header_Top;

            DataTable dt_menus = IFL.Core.LoadObjects.LoadMenu(Url, IFL.Core.Constants.Lists._cop_pfo_Navigation, _menu, 15);

            // HyperLink - inicio
            var btn_hyp_start = IFL.Core.LoadObjects.GetParameter("hyp_start", dt_menus);
            hyp_start.Text = btn_hyp_start.Value ?? string.Empty;
            hyp_start.NavigateUrl = btn_hyp_start.Link ?? @"/";
            hyp_start.Visible = Convert.ToBoolean(btn_hyp_start.Visible);
            hyp_start.Enabled = Convert.ToBoolean(btn_hyp_start.Enable);

            LBL_FAQS = ResourcesHelper.GetStringFromResource("PFO", "FAQS", "PT");


        }

    }
}


