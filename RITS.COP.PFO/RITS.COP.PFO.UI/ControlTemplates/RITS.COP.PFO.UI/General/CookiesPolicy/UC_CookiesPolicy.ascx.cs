﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.CookiesPolicy
{
    public partial class UC_CookiesPolicy : UserControl
    {
        public string TextCookies { get; set; }
        protected string LBL_CookiesPolicy;

        public string Url { get; set; }
        public string List { get; set; }
        public string Menu { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Url = IFL.Core.Constants.Urls._formacao;
                List = IFL.Core.Constants.Lists._cop_pfo_Text;
                Menu = IFL.Core.Constants.Menus._menu_Header_Top;

                LoadFields();

                //SEO
                string tl = "Política de Uso de Cookies - Formação Olímpica";
                string kw = "Programa de Formação Olímpica, Política de Uso de Cookies";
                string dc = "Política de Uso de Cookies do Programa de Formação Olímpica.";
                IFL.Core.TagsSEO.ApplyTagsCSharp(tl, kw, dc, this.Page);

            }

            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP001", "UI.UC_CookiesPolicy.Page_Load", ex.Message);
                HttpContext.Current.Response.Redirect(Url + Constants.UrlsPages._pageError, false);
            }
        }

        public void LoadFields()
        {
            DataTable dt_menus = LoadObjects.LoadMenu(Url, Constants.Lists._cop_pfo_Navigation, Menu, 15);

            // HyperLink - inicio
            var btn_hyp_start = IFL.Core.LoadObjects.GetParameter("hyp_start", dt_menus);
            hyp_start.Text = btn_hyp_start.Value ?? string.Empty;
            hyp_start.NavigateUrl = btn_hyp_start.Link ?? @"/";
            hyp_start.Visible = Convert.ToBoolean(btn_hyp_start.Visible);
            hyp_start.Enabled = Convert.ToBoolean(btn_hyp_start.Enable);

            TextCookies = LoadObjects.LoadTextPage(Url, List, "txt_Cookies", 2);

            LBL_CookiesPolicy = ResourcesHelper.GetStringFromResource("PFO", "CookiesPolicy", "PT");
        }

    }
}
