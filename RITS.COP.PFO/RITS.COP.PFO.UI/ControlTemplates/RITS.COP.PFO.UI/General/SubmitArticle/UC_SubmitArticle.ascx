﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_SubmitArticle.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.SubmitArticle.UC_SubmitArticle" %>

<section>
    <div class="slider-container">
        <!-- Header Carousel -->
        <!-- START REVOLUTION SLIDER 5.0 -->
        <div class="rev_slider_wrapper">
            <div id="rev_slider_inside" class="rev_slider" data-version="5.0">
                <ul>
                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="../../../../_layouts/15/images/RITS.COP.BR/PHO10557315.retouche.jpg" alt="" width="1920" height="1080" data-bgposition="center center" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption News-Title"
                            data-x="left" data-hoffset="80"
                            data-y="bottom" data-voffset="150"
                            data-whitespace="normal"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="500"
                            data-bgposition="center center">
                            <div class="insidepages">
                                <h1 class="page-header">LblSubmitArticle</h1>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>
        <!-- END OF SLIDER WRAPPER -->
    </div>
</section>
<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li><a href="/">Início</a></li>
                    <li class="active">Submete o teu Artigo</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- /.container -->
<section>
    <div class="container sendmessage">
        <div class="row">
            <div class="col-lg-12">
                <!-- Contact Form -->
                <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
                <div class="row">
                    <div class="col-md-10">
                        <h3 class="section-title"><i class="fa fa-paper-plane-o" aria-hidden="true"></i>Desejas enviar um artigo?</h3>
                        <!-- Form -->
                        <div id="contact" class="contact-form">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" id="name" name="name" required>
                                        <label class="mdl-textfield__label" for="name">Nome</label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="email" name="email" id="email" required>
                                        <label class="mdl-textfield__label" for="email">Email</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" name="telefone">
                                        <label class="mdl-textfield__label" for="telefone">Telefone</label>
                                    </div>
                                </div>
                                <div class="col-md-5">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" name="ename" id="ename" required>
                                        <label class="mdl-textfield__label" for="ename">Entidade</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <div class="mdl-selectfield">
                                            <label class="mdl-textfield__label" for="articletype">Tipo de Publicação</label>
                                            <select name="articletype" class="browser-default">
                                                <option value="" disabled selected>Tipo de Publicação</option>
                                                <option value"Treino Desportivo">Treino Desportivo</option>
                                                <option value"Psicologia e Pedagogia do Desporto">Psicologia e Pedagogia do Desporto</option>
                                                <option value"Medicina do Desporto">Medicina do Desporto</option>
                                                <option value"Fisiologia e Biomecânica do Desporto">Fisiologia e Biomecânica do Desporto</option>
                                                <option value"Economia, Direito e Gestão do Desporto">Economia, Direito e Gestão do Desporto</option>
                                                <option value"Sociologia e História do Desporto">Sociologia e História do Desporto</option>
                                            </select>
                                        </div>
                                        </div>
                                    </div>
                                     <div class="col-md-6">
                                    
                                              <div class="fileup">
                                                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                                                <asp:TextBox ID="txt_file_Anexo1" runat="server" ClientIDMode="Static" CssClass="mdl-textfield__input displayName" />
                                                                <asp:RegularExpressionValidator Display="Dynamic" ControlToValidate="txt_file_Anexo1" ID="RegularExpressionValidator22" ValidationExpression="^[\s\S]{5,}$" runat="server" ErrorMessage="Por favor, introduza pelo menos 5 caracteres" CssClass="label error"></asp:RegularExpressionValidator>
                                                                <label class="mdl-textfield__label" for="txt_file_Anexo1">Nome do ficheiro</label>
                                                            </div>
                                                            <asp:FileUpload ID="file_Anexo1" runat="server" ClientIDMode="Static" CssClass="addFileButton" />

                                                            <asp:RequiredFieldValidator ErrorMessage="Required" ControlToValidate="file_Anexo1" runat="server" Display="Dynamic" ForeColor="Red" CssClass="requiredthis" />
                                                            <asp:RegularExpressionValidator ID="RegularExpressionValidator1" ValidationExpression="([a-zA-Z0-9\s_\\.\-:])+(.pdf|.PDF)$"
                                                                ControlToValidate="file_Anexo1" runat="server" ForeColor="Red" ErrorMessage="O Anexo deverá ser no formato PDF"
                                                                Display="Dynamic" CssClass="label error" />
                                                            <div class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray bt-clearthis">Limpar</div>
                                                        </div>
                                    
                                          </div>
                                </div>
                           
                            <div class="row">
                                <div class="col-md-10">
                                    <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                        <input class="mdl-textfield__input" type="text" name="message" id="message" value="Desejo inscrever a minha escola no Programa de Educação Olímpica. Por favor, contactem-me." readonly>
                                        <label class="mdl-textfield__label" for="message">Mensagem</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div id="success">
                                        <%--<input type="submit" value="Submeter" id="enviarJn" runat="server" onserverclick="btn_submeteNewsletter_OnClick" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>--%>
                                        <input type="submit" value="Submeter" id="enviarJn" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
                                        <button id="cancel" onclick="window.history.back();" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray">Cancelar</button>
                                        <div class="alert fade in">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          <asp:Label runat="server" ID="lbl_Info" ClientIDMode="Static"></asp:Label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $('#aspnetForm').validate({
        debug: false,
        rules: {
            name: {
                minlength: 3
            },
            ename: {
                minlength: 3
            },
            telefone: 'type_telefone'
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    $(document).ready(function () {
        $('.nav').find('.item.submitDocument').addClass('selected');
        $('footer li a.submitDocument').addClass('selected');
        validateFields();
        $('#name, #email, #ename').change(validateFields);
    });
    function validateFields() {
        if ($('#name').val().length > 0 &&
            $('#email').val().length > 0 &&
            $('#ename').val().length > 0) {
            $("#success button:not(#cancel)").prop("disabled", false);
        }
        else {
            $("#success button:not(#cancel)").prop("disabled", true);
        }
    }
    if ($('.alert span').hasClass('success')) {
        $('.alert').addClass('alert-success');
    } else {
        $('.alert').addClass('alert-warning');
    }
    if (!$.trim($('.alert span').html()).length) {
        $('.alert').remove();
    }
</script>
