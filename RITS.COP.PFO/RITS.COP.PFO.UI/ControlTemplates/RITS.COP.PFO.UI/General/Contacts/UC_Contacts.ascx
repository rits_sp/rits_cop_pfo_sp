﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Contacts.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.Contacts.UC_Contacts" %>



<section>
    <div class="slider-container">
        <!-- Header Carousel -->
        <!-- START REVOLUTION SLIDER 5.0 -->
        <div class="rev_slider_wrapper">
            <div id="rev_slider_inside" class="rev_slider" data-version="5.0">
                <ul>
                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="../../../../_layouts/15/images/RITS.COP.PFO.BR/PHO10285829.retouche.jpg" alt="" width="1920" height="1080" data-bgposition="center center" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption News-Title"
                            data-x="left" data-hoffset="80"
                            data-y="bottom" data-voffset="150"
                            data-whitespace="normal"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="500"
                            data-bgposition="center center">
                            <div class="insidepages">
                                <h1 class="page-header"><%=LblContacts%></h1>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>
        <!-- END OF SLIDER WRAPPER -->
    </div>
</section>


<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                    <li class="active"><%=LblContacts%></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- /.container -->
<section>
    <div class="container sendmessage">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <h2 class="section-title wow fadeInDown" data-wow-delay="0.4s"><i class="fa fa-phone"></i><%=LblContacts%></h2>
                </div>
                <div class="row wow fadeInLeft" data-wow-delay="0.4s">
                    <div class="col-md-8">
                        <!-- Embedded Google Map -->
                        <iframe width="100%" height="450px" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2209.327935898801!2d-9.202270240613169!3d38.70285837581921!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xd1ecb50a94f86b1%3A0x9ede9798565c2fb8!2sComit%C3%A9+Ol%C3%ADmpico+de+Portugal!5e0!3m2!1spt-PT!2spt!4v1453214576202"></iframe>
                    </div>
                    <!-- Contact Details Column -->
                    <div class="col-md-4">
                        <h3><%=LblComite%></h3>
                        <p><i class="fa fa-home"></i><%=TextAddress%></p>
                        <p><i class="fa fa-phone"></i><%=TextPhone%></p>
                        <p><i class="fa fa-fax"></i><%=TextFax%></p>
                        <p><i class="fa fa-link"></i><a href="http://comiteolimpicoportugal.pt" target="new">http://comiteolimpicoportugal.pt</a></p>
                        <div class="social-icons">
                           <%-- <uc1:UC_headerSocial runat="server" id="UC_headerSocial" />--%>
                        </div>
                    </div>
                </div>

                <!-- Contact Form -->
                <!-- In order to set the email address and subject line for the contact form go to the bin/contact_me.php file. -->
                <div class="row wow fadeInLeft" data-wow-delay="0.4s">
                    <div class="col-md-10">
                        <h3 class="section-title"><i class="fa fa-paper-plane"></i>Enviar Mensagem</h3>
                        <!-- Form -->

                        <div class="row">
                            <div class="col-md-4">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="nameContact" name="nameContact" required>
                                    <label class="mdl-textfield__label" for="nameContact"><%=LblName%></label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" type="email" id="emailContact" name="emailContact" required>
                                    <label class="mdl-textfield__label" for="emailContact"><%=LblEmail%></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" id="phone" type="text" name="phone" onkeypress='return event.charCode >= 48 && event.charCode <= 57'>
                                    <label class="mdl-textfield__label" for="phone"><%=LblPhone%></label>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="entityname" name="entityname" required>
                                    <label class="mdl-textfield__label" for="entityname"><%=LblEntity%></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="subject" name="subject" required>
                                    <label class="mdl-textfield__label" for="subject"><%=LblSubject%></label>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-8">
                                <div class="mdl-textfield mdl-js-textfield">
                                    <textarea class="mdl-textfield__input" type="text" rows="3" id="message" name="message" required></textarea>
                                    <label class="mdl-textfield__label" for="message"><%=LblMessage%></label>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="row">
                                <div id="success">
                                    <input type="submit" value="Submeter" id="enviarPC" runat="server" onserverclick="Submit_Click" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
                                    <button id="cancel" onclick="window.history.back();" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray">Cancelar</button>
                                    <div class="alert fade in">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                      <asp:Label runat="server" ID="lbl_Info" ClientIDMode="Static"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <!-- Form -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script>
    $('#aspnetForm').validate({
        debug: false,
        rules: {
            type_password: {
                minlength: 5
            },
            confirm_password: {
                minlength: 5,
                equalTo: "#type_password"
            },
            nameContact: {
                minlength: 5,
            },
            entityname: {
                minlength: 5,
            },
            subject: {
                minlength: 10,
            },
            message: {
                minlength: 10,
            },
            phone: 'type_telefone'
        },
        submitHandler: function (form) {
            form.submit();
            $('#ConfirmDialog').modal('show');
        }
    });
    $('.navbar-topheader-item').find('.contacts a').addClass('selected');
    $(document).ready(function () {
        validateFields();
        $('#nameContact, #emailContact, #entityname, #subject, #message').change(validateFields);
        $('footer li a.contacts').addClass('selected');
    });
    function validateFields() {
        if ($('#nameContact').val().length > 0 &&
            $('#emailContact').val().length > 0 &&
            $('#entityname').val().length > 0 &&
            $('#subject').val().length > 0 &&
            $('#message').val().length > 0)
         {
            $("#success button:not(#cancel)").prop("disabled", false);
        }
        else {
            $("#success button:not(#cancel)").prop("disabled", true);
        }
    }
    if ($('.alert span').hasClass('success')) {
        $('.alert').addClass('alert-success');
    } else {
        $('.alert').addClass('alert-warning');
    }
    if (!$.trim($('.alert span').html()).length) {
        $('.alert').remove();
    }
    //if ($('#lbl_Info').hasClass('success')) {
    //    alertify.success('Sucesso!');
    //}
    $('.nav').find('.item.contacts').addClass('selected');
</script>

