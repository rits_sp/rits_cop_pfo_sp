﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Client;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.Contacts
{
    public partial class UC_Contacts : UserControl
    {

        #region Variables

        public String LblContacts { get; set; }
        public String LblComite { get; set; }
        public String LblName { get; set; }
        public String LblEmail { get; set; }
        public String LblPhone { get; set; }
        public String LblEntity { get; set; }
        public String LblSubject { get; set; }
        public String LblMessage { get; set; }
        public String TextAddress { get; set; }
        public String TextPhone { get; set; }
        public String TextFax { get; set; }
        public String TextSubscribeNews { get; set; }
        public string Menu { get; set; }
        public string Url { get; set; }
        public string List { get; set; }

        #endregion

        #region Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Menu = IFL.Core.Constants.Menus._menu_Header_Top;
                Url = IFL.Core.Constants.Urls._formacao;
                List = IFL.Core.Constants.Lists._cop_pfo_Text;

                LoadFields();

                //SEO
                var tl = "Contactos - Formação Olímpica";
                var kw = "Contactos, COP, PFO, Formação Olímpica, Comité Olímpico de Portugal";
                string dc = "Formação Olímpica. Contactos do Comité Olímpico de Portugal.";
                IFL.Core.TagsSEO.ApplyTagsCSharp(tl, kw, dc, this.Page);
            }
            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP001", "UI.UC_contacts.Page_Load", ex.Message);
                HttpContext.Current.Response.Redirect(Url + IFL.Core.Constants.UrlsPages._pageError, false);
            }
        }

        public void LoadFields()
        {
            DataTable dt_menus = IFL.Core.LoadObjects.LoadMenu(Url, IFL.Core.Constants.Lists._cop_pfo_Navigation, Menu, 15);

            // HyperLink - inicio
            var btn_hyp_start = IFL.Core.LoadObjects.GetParameter("hyp_start", dt_menus);
            hyp_start.Text = btn_hyp_start.Value ?? string.Empty;
            hyp_start.NavigateUrl = btn_hyp_start.Link ?? @"/";
            hyp_start.Visible = Convert.ToBoolean(btn_hyp_start.Visible);
            hyp_start.Enabled = Convert.ToBoolean(btn_hyp_start.Enable);

            //Labels da pagina
            LblContacts = ResourcesHelper.GetStringFromResource("PEO", "Contacts", "PT");
            LblComite = ResourcesHelper.GetStringFromResource("PEO", "Comite", "PT");
            LblName = ResourcesHelper.GetStringFromResource("PEO", "Name", "PT");
            LblEmail = ResourcesHelper.GetStringFromResource("PEO", "Email", "PT");
            LblPhone = ResourcesHelper.GetStringFromResource("PEO", "Phone", "PT");
            LblEntity = ResourcesHelper.GetStringFromResource("PEO", "Entity", "PT");
            LblSubject = ResourcesHelper.GetStringFromResource("PEO", "Subject", "PT");
            LblMessage = ResourcesHelper.GetStringFromResource("PEO", "Message", "PT");

               //Textos da Pagina
               var textAddress = LoadObjects.LoadTextPage(Url, List, "txt_Address", 2) ?? string.Empty;
              TextAddress = !string.IsNullOrEmpty(textAddress)
              ? textAddress.Replace("<p>", "").Replace("</p>", "")
                   : string.Empty;

              var textPhone = LoadObjects.LoadTextPage(Url, List, "txt_Phone", 2) ?? string.Empty;
               TextPhone = !string.IsNullOrEmpty(textPhone)
                    ? textPhone.Replace("<p>", "").Replace("</p>", "")
                   : string.Empty;


               var textFax = LoadObjects.LoadTextPage(Url, List, "txt_Fax", 2) ?? string.Empty;
              TextFax = !string.IsNullOrEmpty(textFax) ? textFax.Replace("<p>", "").Replace("</p>", "") : string.Empty;

               var textSubscribeNews = LoadObjects.LoadTextPage(Url, List, "txt_SubscribeNews", 2) ?? string.Empty;
                TextSubscribeNews = !string.IsNullOrEmpty(textSubscribeNews)
                   ? textSubscribeNews.Replace("<p>", "").Replace("</p>", "")
                    : string.Empty;


        }

        protected void Submit_Click(object sender, EventArgs e)
        {
                try
                {

                    SPSecurity.RunWithElevatedPrivileges(delegate()
                    {
                        using (var site = new SPSite(IFL.Core.Constants.Urls._formacao))
                        using (var web = site.RootWeb)
                        {
                            var n = Request.Form["nameContact"];
                            var em = Request.Form["emailContact"];
                            var p = Request.Form["phone"];
                            var et = Request.Form["entityname"];
                            var s = Request.Form["subject"];
                            var m = Request.Form["message"];

                            bool isCreated = CRUDObjects.SaveContacts(web, IFL.Core.Constants.Lists._cop_pfo_Contacts, n, em,p,et, s, m);

                            if (isCreated)
                            {
                                lbl_Info.Text = "O seu pedido de contacto foi enviado com sucesso!";
                                lbl_Info.CssClass = "success";
                            }
                            else
                            {
                                lbl_Info.Text = "Não foi possivel enviar o seu pedido contacto";
                                lbl_Info.CssClass = "failed";
                            }
                        }
                    });

                }
                catch (Exception ex)
                {

                    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP002", "UI.UC_contacts.Submit_Click", ex.Message);
                    HttpContext.Current.Response.Redirect(Url + IFL.Core.Constants.UrlsPages._pageError, false);
                }
        }


        #endregion


    }
}

