﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_HomeSlider.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.HomePage.UC_HomeSlider" %>

<section>
    <div class="slider-container home">
        <!-- Header Carousel -->
        <!-- START REVOLUTION SLIDER 5.0 -->
        <div class="rev_slider_wrapper">
            <div id="rev_slider_home" class="rev_slider" data-version="5.0">
                <ul>
                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="../../../../../Style%20Library/cop/imgs/slide1.jpg" alt="" width="1920" height="1080" data-bgposition="center center" data-kenburns="on" data-duration="22000" data-ease="Default" data-scalestart="100" data-scaleend="110" data-rotatestart="0" data-rotateend="0" data-offsetstart="-150 -10" data-offsetend="150 350" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption News-Title"
                            data-x="left" data-hoffset="80"
                            data-y="bottom" data-voffset="150"
                            data-whitespace="normal"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="500"
                            data-bgposition="center center">
                            <h2>CENTRO DE PESQUISA E DESENVOLVIMENTO DESPORTIVO</h2>
                            <a href="/Pages/Valores.aspx" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Saber mais</a>
                        </div>
                    </li>

                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="../../../../../Style%20Library/cop/imgs/slide2.jpg" alt="" width="1920" height="1080" data-bgposition="top right" data-kenburns="on" data-duration="22000" data-ease="Default" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="-150 -150" data-offsetend="150 150" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption News-Title"
                            data-x="left" data-hoffset="80"
                            data-y="bottom" data-voffset="150"
                            data-whitespace="normal"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="500"
                            data-bgposition="center center">
                            <h2>FORMAÇÃO OLÍMPICA</h2>
                            <a href="/Pages/SobrePrograma.aspx" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Saber mais</a>
                        </div>
                    </li>

                    <li data-transition="fade">
                        <!-- MAIN IMAGE -->
                        <img src="../../../../../Style%20Library/cop/imgs/slide3.jpg" alt="" width="1920" height="1080" data-bgposition="center center" data-kenburns="on" data-duration="22000" data-ease="Default" data-scalestart="100" data-scaleend="120" data-rotatestart="0" data-rotateend="0" data-offsetstart="250 350" data-offsetend="-250 -350" data-bgparallax="2" class="rev-slidebg" data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="tp-caption News-Title"
                            data-x="left" data-hoffset="80"
                            data-y="bottom" data-voffset="150"
                            data-whitespace="normal"
                            data-transform_idle="o:1;"
                            data-transform_in="o:0"
                            data-transform_out="o:0"
                            data-start="500"
                            data-bgposition="center center">
                            <h2>PUBLICAÇÕES</h2>
                            <a href="/Pages/Desafios.aspx" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent">Saber mais</a>
                        </div>
                    </li>
                </ul>
            </div>
            <!-- END REVOLUTION SLIDER -->
        </div>
        <!-- END OF SLIDER WRAPPER -->
    </div>
</section>