﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register TagPrefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %>
<%@ Register TagPrefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_Parceiros.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.HomePage.UC_Parceiros" %>

<div class="container logos-container-main">
    <div class="logos-container">
        <div class="logos-title">Parceiros</div>
        <ul id="logos-partners">
            <asp:Repeater ID="rptParceiros" runat="server">
                <ItemTemplate>
                    <li>
                    <asp:ImageButton runat="server"
                            Visible='<%# DataBinder.Eval(Container.DataItem, "Visible") %>'
                            ImageUrl='<%# DataBinder.Eval(Container.DataItem, "LinkImage") %>'
                            PostBackUrl='<%# DataBinder.Eval(Container.DataItem, "LinkUrl") %>'
                            ToolTip='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />

                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
    <div class="logos-container">
        <div class="logos-title">Instituições</div>
        <ul id="logos-institutions">
            <asp:Repeater ID="rptInstituicoes" runat="server">
                <ItemTemplate>
                    <li>
                        <asp:ImageButton runat="server"
                                          Visible='<%# DataBinder.Eval(Container.DataItem, "Visible") %>'
                            ImageUrl='<%# DataBinder.Eval(Container.DataItem, "LinkImage") %>'
                            PostBackUrl='<%# DataBinder.Eval(Container.DataItem, "LinkUrl") %>'
                            ToolTip='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
    <div id="logos_labs_container" class="logos-container" runat="server">
        <div class="logos-title">Laboratórios</div>
        <ul id="logos-labs">
            <asp:Repeater ID="rptLaboratorios" runat="server">
                <ItemTemplate>
                    <li>
                         <asp:ImageButton runat="server"
                             target="new" style="height: 70px"
                            Visible='<%# DataBinder.Eval(Container.DataItem, "Visible") %>'
                            ImageUrl='<%# DataBinder.Eval(Container.DataItem, "LinkImage") %>'
                            PostBackUrl='<%# DataBinder.Eval(Container.DataItem, "LinkUrl") %>'
                            ToolTip='<%# DataBinder.Eval(Container.DataItem, "Name") %>' />
                    </li>
                </ItemTemplate>
            </asp:Repeater>
        </ul>
    </div>
</div>

<script type="text/javascript">

    $(window).load(function () {

        $("#logos-partners").flexisel({
            visibleItems: 1,
            animationSpeed: 1000,
            infinite: false,
            navigationTargetSelector: null,
            autoPlay: {
                enable: false,
                interval: 5000,
                pauseOnHover: true
            },
            responsiveBreakpoints: {
                portrait: {
                    changePoint: 480,
                    visibleItems: 1,
                    itemsToScroll: 1
                },
                landscape: {
                    changePoint: 640,
                    visibleItems: 1,
                    itemsToScroll: 2
                },
                tablet: {
                    changePoint: 768,
                    visibleItems: 1,
                    itemsToScroll: 3
                }
            }
        });

        $("#logos-institutions").flexisel({
            visibleItems: 3,
            itemsToScroll: 3,
            animationSpeed: 1000,
            infinite: true,
            navigationTargetSelector: null,
            autoPlay: {
                enable: true,
                interval: 5000,
                pauseOnHover: true
            },
            responsiveBreakpoints: {
                portrait: {
                    changePoint: 480,
                    visibleItems: 1,
                    itemsToScroll: 1
                },
                landscape: {
                    changePoint: 640,
                    visibleItems: 2,
                    itemsToScroll: 2
                },
                tablet: {
                    changePoint: 768,
                    visibleItems: 3,
                    itemsToScroll: 3
                }
            }
        });

        $("#logos-labs").flexisel({
            visibleItems: 5,
            itemsToScroll: 3,
            animationSpeed: 1000,
            infinite: true,
            navigationTargetSelector: null,
            autoPlay: {
                enable: true,
                interval: 5000,
                pauseOnHover: true
            },
            responsiveBreakpoints: {
                portrait: {
                    changePoint: 480,
                    visibleItems: 1,
                    itemsToScroll: 1
                },
                landscape: {
                    changePoint: 640,
                    visibleItems: 2,
                    itemsToScroll: 2
                },
                tablet: {
                    changePoint: 768,
                    visibleItems: 3,
                    itemsToScroll: 3
                }
            }
        });

        if ($('#logos-partners li').length < 2) {
            $('#logos-partners').parent().find('.nbs-flexisel-nav-left').hide();
            $('#logos-partners').parent().find('.nbs-flexisel-nav-right').hide();
        }

    });


</script>
