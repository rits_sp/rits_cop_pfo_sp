﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.HomePage
{
    public partial class UC_Parceiros : UserControl
    {
        public string Url { get; set; }
        public string List { get; set; }

        private DataTable dt_P;
        private DataTable dt_I;
        private DataTable dt_L;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                Url = IFL.Core.Constants.Urls._formacao;
                List = IFL.Core.Constants.Lists._cop_pfo_Parceiros;

                string labKey = LoadObjects.LoadConfiguration(Constants.Urls._formacao, Constants.Lists._cop_pfo_Configurations, "Key_Display_Laboratorios", 2) ?? string.Empty;

                if (labKey != null && labKey.Length > 0)
                {
                    bool isLab = Convert.ToBoolean(labKey);

                    logos_labs_container.Visible = Convert.ToBoolean(labKey); 

                    if (Convert.ToBoolean(labKey))
                        GetLaboratorios();

                }


                GetInstituicoes();
                GetParceiros();



            }
            catch (Exception ex)
            {

                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP001", "UI.UC_Parceiros.Page_Load", ex.Message);
                HttpContext.Current.Response.Redirect(Url + IFL.Core.Constants.UrlsPages._pageError, false);
            }
        }


        protected void GetParceiros()
        {
            dt_P = LoadObjects.LoadAreaParceiros(Url, List, "P", 100);

            if (dt_P.Rows.Count > 0)
            {
                rptParceiros.DataSource = dt_P;
                rptParceiros.DataBind();
            }
        }

        protected void GetLaboratorios()
        {
            dt_L = LoadObjects.LoadAreaParceiros(Url, List, "L", 100);

            if (dt_L.Rows.Count > 0)
            {
                rptLaboratorios.DataSource = dt_L;
                rptLaboratorios.DataBind();
            }
        }
        protected void GetInstituicoes()
        {
            dt_I = LoadObjects.LoadAreaParceiros(Url, List, "I", 100);

            if (dt_I.Rows.Count > 0)
            {
                rptInstituicoes.DataSource = dt_I;
                rptInstituicoes.DataBind();
            }
        }


    }
}
