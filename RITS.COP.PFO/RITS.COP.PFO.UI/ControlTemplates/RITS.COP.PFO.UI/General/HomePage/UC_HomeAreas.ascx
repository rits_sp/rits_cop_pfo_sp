﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_HomeAreas.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.HomePage.UC_HomeAreas" %>
<section>
    <div class="head-description bg-gray-blue-dark">
        <div class="container">
            <h1 class="page-header">PortalTitle</h1>
            <p>
                TextProgramIntroduction
            </p>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
                <div class="col-lg-8">
                    <div id="card-activities" class="mdl-card wide mdl-shadow--2dp bg-gray-light featured">
                        <div class="mdl-card__title bg-red">
                            <h2 class="mdl-card__title-text">LblDestaques</h2>
                        </div>
                        <div class="mdl-card">
                            <div id="rev_slider_home_activities_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin: 0px auto; background-color: #ffffff; padding: 0px; margin-top: 0px; margin-bottom: 0px;">
                                <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
                                <div id="rev_slider_home_activities" class="rev_slider fullwidthabanner" style="display: none;" data-version="5.0.7">
                                    <ul>
                                        <li data-index="rs-270" data-transition="parallaxvertical" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="https://revolutioncdn-themepunchgbr.netdna-ssl.com/wp-content/uploads/revslider/news-gallery/newscarousel2.jpg" data-rotate="0" data-saveperformance="off" data-title="THE REAL DEAL" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="Every right implies a responsibility." class="tp-revslider-slidesli" style="width: 100%; height: 100%; overflow: hidden;">
 
                                            <div class="slotholder" style="position: absolute; top: 0px; left: 0px; z-index: 0; width: 100%; height: 100%; backface-visibility: hidden; transform: translate3d(0px, 0px, 0px);"><!--Runtime Modification - Img tag is Still Available for SEO Goals in Source - <img src="//revolutioncdn-themepunchgbr.netdna-ssl.com/wp-content/uploads/revslider/news-gallery/newscarousel2.jpg" alt="" title="newscarousel2.jpg" width="1920" height="1280" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg defaultimg" data-no-retina="">--><div class="tp-bgimg defaultimg" style="width: 100%; height: 100%; opacity: 0; background-image: url(&quot;//revolutioncdn-themepunchgbr.netdna-ssl.com/wp-content/uploads/revslider/news-gallery/newscarousel2.jpg&quot;); background-color: rgba(0, 0, 0, 0); background-size: cover; background-position: center center; background-repeat: no-repeat;" src="//revolutioncdn-themepunchgbr.netdna-ssl.com/wp-content/uploads/revslider/news-gallery/newscarousel2.jpg"></div></div>
                                            <div class="tp-parallax-wrap" style="position:absolute;visibility:hidden"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position:absolute"><div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme" id="slide-270-layer-3" data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']" data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']" data-width="full" data-height="full" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;" data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;" data-start="1000" data-basealign="slide" data-responsive_offset="on" style="z-index: 5; border-color: rgb(0, 0, 0); visibility: hidden; background-color: rgba(0, 0, 0, 0.34902);"> </div></div></div></div>
                                            <div class="tp-parallax-wrap" style="position:absolute;visibility:hidden"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position:absolute"><div class="tp-caption Newspaper-Title   tp-resizeme" id="slide-270-layer-1" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['165','135','105','130']" data-fontsize="['50','50','50','30']" data-lineheight="['55','55','55','35']" data-width="['600','600','600','420']" data-height="none" data-whitespace="normal" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal; visibility: hidden;">Every right implies a responsibility. </div></div></div></div>
                                            <div class="tp-parallax-wrap" style="position:absolute;visibility:hidden"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position:absolute"><div class="tp-caption Newspaper-Subtitle   tp-resizeme" id="slide-270-layer-2" data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']" data-y="['top','top','top','top']" data-voffset="['140','110','80','100']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-mask_out="x:0;y:0;s:inherit;e:inherit;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="z-index: 7; white-space: nowrap; visibility: hidden;">THE REAL DEAL </div></div></div></div>
                                            <div class="tp-parallax-wrap" style="position:absolute;visibility:hidden"><div class="tp-loop-wrap" style="position:absolute;"><div class="tp-mask-wrap" style="position:absolute"><div class="tp-caption Newspaper-Button rev-btn " id="slide-270-layer-5" data-x="['left','left','left','left']" data-hoffset="['53','53','53','30']" data-y="['top','top','top','top']" data-voffset="['361','331','301','245']" data-width="none" data-height="none" data-whitespace="nowrap" data-transform_idle="o:1;" data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;" data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);" data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;" data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;" data-mask_out="x:0;y:0;" data-start="1000" data-splitin="none" data-splitout="none" data-responsive_offset="on" data-responsive="off" style="z-index: 8; white-space: nowrap; outline: none; box-shadow: none; box-sizing: border-box; cursor: pointer; visibility: hidden;">READ MORE </div></div></div></div>
                                        </li>
                                        <%--<asp:Repeater ID="rptLastAct" runat="server">
                                        <ItemTemplate>
                                            <!-- SLIDE  -->
                                            <li data-index="rs-'<%# DataBinder.Eval(Container.DataItem, "Key") %>'" data-transition="parallaxvertical" data-slotamount="default" data-easein="default" data-easeout="default" data-masterspeed="default" data-thumb="<%# DataBinder.Eval(Container.DataItem, "LinkImage") %>" data-rotate="0" data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7" data-saveperformance="off" data-title="<%# DataBinder.Eval(Container.DataItem, "Title") %>" data-description="<%# DataBinder.Eval(Container.DataItem, "Description") %>">
                                                <!-- MAIN IMAGE -->
                                                <img src="<%# DataBinder.Eval(Container.DataItem, "LinkImage") %>" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="10" class="rev-slidebg" data-no-retina>
                                                <!-- LAYERS -->

                                                <!-- LAYER NR. 1 -->
                                                <div class="tp-caption tp-shape tp-shapewrapper   tp-resizeme rs-parallaxlevel-0"
                                                    id="slide-'<%# DataBinder.Eval(Container.DataItem, "Key") %>'-layer-3"
                                                    data-x="['center','center','center','center']" data-hoffset="['0','0','0','0']"
                                                    data-y="['middle','middle','middle','middle']" data-voffset="['0','0','0','0']"
                                                    data-width="full"
                                                    data-height="full"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                                                    data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;"
                                                    data-start="1000"
                                                    data-basealign="slide"
                                                    data-responsive_offset="on"
                                                    style="z-index: 5; background-color: rgba(0, 0, 0, 0.35); border-color: rgba(0, 0, 0, 1.00);">
                                                </div>

                                                <!-- LAYER NR. 2 -->
                                                <div class="tp-caption Newspaper-Title   tp-resizeme rs-parallaxlevel-0"
                                                    id="slide-'<%# DataBinder.Eval(Container.DataItem, "Key") %>'-layer-1"
                                                    data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']"
                                                    data-y="['top','top','top','top']" data-voffset="['165','135','105','130']"
                                                    data-fontsize="['50','50','50','30']"
                                                    data-lineheight="['55','55','55','35']"
                                                    data-width="['600','600','600','420']"
                                                    data-height="none"
                                                    data-whitespace="normal"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                                    data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                                    data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                                                    data-start="1000"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                    style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal;">
                                                    <%# DataBinder.Eval(Container.DataItem, "Description") %>
                                                </div>

                                                <!-- LAYER NR. 3 -->
                                                <div class="tp-caption Newspaper-Subtitle   tp-resizeme rs-parallaxlevel-0"
                                                    id="slide-'<%# DataBinder.Eval(Container.DataItem, "Key") %>'-layer-2"
                                                    data-x="['left','left','left','left']" data-hoffset="['50','50','50','30']"
                                                    data-y="['top','top','top','top']" data-voffset="['140','110','80','100']"
                                                    data-width="none"
                                                    data-height="none"
                                                    data-whitespace="nowrap"
                                                    data-transform_idle="o:1;"
                                                    data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                                    data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                                    data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                                    data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                                                    data-start="1000"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                    style="z-index: 7; white-space: nowrap;">
                                                    <%# DataBinder.Eval(Container.DataItem, "Title") %>
                                                </div>

                                                <!-- LAYER NR. 4 -->
                                                <div class="tp-caption Newspaper-Button rev-btn  rs-parallaxlevel-0"
                                                    id="slide-'<%# DataBinder.Eval(Container.DataItem, "Key") %>'-layer-5"
                                                    data-x="['left','left','left','left']" data-hoffset="['53','53','53','30']"
                                                    data-y="['top','top','top','top']" data-voffset="['361','331','301','245']"
                                                    data-width="none"
                                                    data-height="none"
                                                    data-whitespace="nowrap"
                                                    data-transform_idle="o:1;"
                                                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                                                    data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);cursor:pointer;"
                                                    data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                                    data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                                    data-mask_in="x:0px;y:0px;"
                                                    data-mask_out="x:0;y:0;"
                                                    data-start="1000"
                                                    data-splitin="none"
                                                    data-splitout="none"
                                                    data-responsive_offset="on"
                                                    data-responsive="off"
                                                    style="z-index: 8; white-space: nowrap; outline: none; box-shadow: none; box-sizing: border-box; -moz-box-sizing: border-box; -webkit-box-sizing: border-box;">
                                                    <a href="<%# DataBinder.Eval(Container.DataItem, "UrlAct") %>">Saber Mais</a>
                                                </div>
                                            </li>
                                        </ItemTemplate>
                                    </asp:Repeater>--%>
                                    </ul>
                                    <div class="tp-bannertimer tp-bottom" style="height: 5px; background-color: rgba(166, 216, 236, 1.00);"></div>
                                </div>
                            </div>
                            <!-- END REVOLUTION SLIDER -->
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                            <a href="" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Ver Todos</a>
                        </div>
                        <div class="mdl-card__menu">
                            <i class="fa fa-star" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                     <div id="card-activities" class="mdl-card wide mdl-shadow--2dp bg-gray-light featured">
                        <div class="mdl-card__title bg-green">
                            <h2 class="mdl-card__title-text">LblAgenda</h2>
                        </div>
                        <div class="mdl-card agenda">
                            <div class="tiva-events-calendar compact" data-source="json" data-start="monday" data-view="list" data-switch="hide"></div> 
                        </div>
                        <div class="mdl-card__actions mdl-card--border">
                            <a href="/Pages/Agenda.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Ver Todos</a>
                        </div>
                        <div class="mdl-card__menu">
                            <i class="fa fa-calendar" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
        </div>
        <div class="row">
                <div class="col-md-4">
                    <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                        <div class="mdl-card__title bg-blue-dark">
                            <h2 class="mdl-card__title-text">LblFormações</h2>
                        </div>
                        <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/IMG_7825.jp)'></div> 
                        <div class="mdl-card__actions mdl-card--border">
                            <a href="/Pages/Formacoes.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Saber mais</a>
                        </div>
                        <div class="mdl-card__menu">
                            <i class="fa fa-graduation-cap" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                        <div class="mdl-card__title bg-blue">
                            <h2 class="mdl-card__title-text">LblPublicações</h2>
                        </div>
                        <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/IMG_7825.jp)'></div> 
                        <div class="mdl-card__actions mdl-card--border">
                            <a href="/Pages/Publicacoes.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Saber mais</a>
                        </div>
                        <div class="mdl-card__menu">
                            <i class="zmdi zmdi-book"></i>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="mdl-card mdl-shadow--2dp bg-gray-light">
                        <div class="mdl-card__title bg-orange">
                            <h2 class="mdl-card__title-text">LblPrémios COP</h2>
                        </div>
                        <div class="mdl-card__image" style='background-image: url(../../../../../Style%20Library/cop/imgs/IMG_7825.jp)'></div> 
                        <div class="mdl-card__actions mdl-card--border">
                            <a href="/Pages/PremiosCOP.aspx" class="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">Saber mais</a>
                        </div>
                        <div class="mdl-card__menu">
                            <i class="fa fa-trophy" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
        </div>
    </div>
</section>