﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.TermsConditions
{
    public partial class UC_TermsConditions : UserControl
    {
        public string TextTerms { get; set; }
        protected string LBL_Terms;

        string _url = IFL.Core.Constants.Urls._formacao;
        string _list = IFL.Core.Constants.Lists._cop_pfo_Text;
        DataTable dt_menus = null;

       
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                string _menu = IFL.Core.Constants.Menus._menu_Header_Top;

                dt_menus = IFL.Core.LoadObjects.LoadMenu(_url, IFL.Core.Constants.Lists._cop_pfo_Navigation, _menu, 15);

                // HyperLink - inicio
                var btn_hyp_start = IFL.Core.LoadObjects.GetParameter("hyp_start", dt_menus);
          
                hyp_start.Text = btn_hyp_start.Value ?? string.Empty;
                hyp_start.NavigateUrl = btn_hyp_start.Link ?? @"/";
                hyp_start.Visible = Convert.ToBoolean(btn_hyp_start.Visible);
                hyp_start.Enabled = Convert.ToBoolean(btn_hyp_start.Enable);

                TextTerms = IFL.Core.LoadObjects.LoadTextPage(_url, _list, "txt_Terms", 2);

                LBL_Terms = ResourcesHelper.GetStringFromResource("PFO", "ConditionsTerms", "PT");

                //SEO
                string tl = "Termos e Condições - Formação Olímpica";
                string kw = "Programa de Formação Olímpica, Termos e Condições";
                string dc = "Termos e Condições do Programa de Formação Olímpica.";
                IFL.Core.TagsSEO.ApplyTagsCSharp(tl, kw, dc, this.Page);
                
            }

            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP001", "UI.UC_Terms.Page_Load", ex.Message);
                HttpContext.Current.Response.Redirect(_url + Constants.UrlsPages._pageError, false);

            }
        }
    }
}
