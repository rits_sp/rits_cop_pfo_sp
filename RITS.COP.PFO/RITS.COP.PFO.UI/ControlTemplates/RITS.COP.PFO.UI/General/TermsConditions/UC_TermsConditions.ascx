﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_TermsConditions.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.TermsConditions.UC_TermsConditions" %>


<div>
    <section>
      <div class="slider-container">
      <!-- Header Carousel -->
          <!-- START REVOLUTION SLIDER 5.0 -->
        <div class="rev_slider_wrapper">
         <div id="rev_slider_inside" class="rev_slider"  data-version="5.0">
          <ul>    
           <li data-transition="fade">      
             <!-- MAIN IMAGE -->
              <img src="../../../../_layouts/15/images/RITS.COP.PFO.BR/slidemapas.jpg"  alt="Comité Olímpico de Portugal" width="1920" height="1080" data-bgposition="center center" data-bgparallax="2" class="rev-slidebg" data-no-retina>            
             <!-- LAYER NR. 1 -->
             <div class="tp-caption News-Title"               
                data-x="left" data-hoffset="80" 
                data-y="bottom" data-voffset="150"               
                data-whitespace="normal"
                data-transform_idle="o:1;"      
                data-transform_in="o:0" 
                data-transform_out="o:0"               
                data-start="500"
                data-bgposition="center center">
                      <div class="insidepages">
                          <h1 class="page-header"><%=LBL_Terms%></h1>
                      </div>
                </div>
           </li>
          </ul>       
         </div><!-- END REVOLUTION SLIDER -->
        </div><!-- END OF SLIDER WRAPPER -->
      </div>
    </section>
    <section>
        <!-- Page Content -->
        <div class="container">
            <!-- Page Heading/Breadcrumbs -->
            <div class="row">
                <div class="col-lg-12">
                    <ol class="breadcrumb">
                        <li><asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                        <li class="active"><%=LBL_Terms%></li>
                    </ol>
                </div>
            </div>
        </div>
    </section>
    <!-- /.container -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-lg-8">
                    <h2><%=LBL_Terms%></h2>
                     <%=TextTerms%>
            </div>
        </div>
    </section>
</div>
<script>
    $(document).ready(function () {
        $('footer li a.terms').addClass('selected');
    });
</script>