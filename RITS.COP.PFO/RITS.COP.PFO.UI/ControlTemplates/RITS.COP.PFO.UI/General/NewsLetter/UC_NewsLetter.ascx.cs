﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;
using RITS.COP.PFO.MC;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.NewsLetter
{
    public partial class UC_NewsLetter : UserControl
    {


        private string _url = IFL.Core.Constants.Urls._formacao;
        private string _list = IFL.Core.Constants.Lists._cop_pfo_Text;

       
        private DataTable dt_menus = null;


        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                string _menu = IFL.Core.Constants.Menus._menu_Header_Top;
                dt_menus = IFL.Core.LoadObjects.LoadMenu(_url, IFL.Core.Constants.Lists._cop_pfo_Navigation, _menu, 15);

                // HyperLink - inicio
                var btn_hyp_start = IFL.Core.LoadObjects.GetParameter("hyp_start", dt_menus);
                hyp_start.Text = btn_hyp_start.Value ?? string.Empty;
                hyp_start.NavigateUrl = btn_hyp_start.Link ?? @"/";
                hyp_start.Visible = Convert.ToBoolean(btn_hyp_start.Visible);
                hyp_start.Enabled = Convert.ToBoolean(btn_hyp_start.Enable);
                
                //SEO
                string tl = "Newsletter - Formação Olímpica";
                string kw = "Newsletter, PEO, COP, Formação Olímpica, Notícias, Artigos, Novidades";
                string dc = "Inscreve-te para ficares a par de todas as novidades do Programa de Formação Olímpica!";
                IFL.Core.TagsSEO.ApplyTagsCSharp(tl, kw, dc, this.Page);
                
            }
            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP001", "UI.UC_Newsletter.Page_Load", ex.Message);
                HttpContext.Current.Response.Redirect(_url + IFL.Core.Constants.UrlsPages._pageError, false);
            }
        }

        protected void btn_submeteNewsletter_OnClick(object sender, EventArgs e)
        {
            try
            {
                SPSecurity.RunWithElevatedPrivileges(delegate()
                {

                    var name = Request.Form["nameNews"];
                    var email = Request.Form["emailNews"];


                    if (!String.IsNullOrEmpty(name) && !String.IsNullOrEmpty(email))
                    {
                        using (var site = new SPSite(IFL.Core.Constants.Urls._formacao))
                        {
                            using (var web = site.RootWeb)
                            {
                                var m = CRUDObjects.SaveNewsletter(web, IFL.Core.Constants.Lists._cop_pfo_Newsletter, name, email);

                                if (m)
                                {
                                    var isRegister = MailChimp.MailChimpRegister(name, email);

                                    if (isRegister)
                                    {
                                        lbl_Info.Text = @"Pedido de NewsLetter registado com sucesso!";
                                        lbl_Info.CssClass = "success";
                                    }
                                    else
                                    {
                                        lbl_Info.Text = @"Pedido de NewsLetter não foi registado!";
                                        lbl_Info.CssClass = "failed";
                                    }
                                }
                                else
                                {
                                    lbl_Info.Text = @"Pedido de NewsLetter não foi registado!";
                                    lbl_Info.CssClass = "failed";
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception ex)
            {

                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP002", "UI.UC_Newsletter.btn_submeteNewsletter_OnClick", ex.Message);
                HttpContext.Current.Response.Redirect(_url + Constants.UrlsPages._pageError, false);
            }
        }
    }
}
