﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_NewsLetter.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.NewsLetter.UC_NewsLetter" %>



<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                    <li class="active">Subscreva a nossa Newsletter</li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- /.container -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <h1 class="section-title wow fadeInDown" data-wow-delay="0.4s"><i class="zmdi zmdi-email"></i>Subscreva a nossa Newsletter</h1>
                </div>
                <div class="row comptext">
                    <h2>Inscreve-te para ficares a par de todas as novidades da Formação Olímpica!</h2>
                </div>
                <div class="row">
                    <div class="mdl-card mdl-shadow--2dp bg-gray-light login-container wow fadeInLeft">
                        <div class="card">
                            <div class="col-lg-12">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" type="text" id="name" name="nameNews" required>
                                    <label class="mdl-textfield__label" for="name">Nome</label>
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label">
                                    <input class="mdl-textfield__input" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$" type="email" name="emailNews" id="email" required>
                                    <label class="mdl-textfield__label" for="email">Email</label>
                                </div>
                            </div>
                            <div class="col-lg-12 box-buttons">
                                <div class="row">
                                    <input type="submit" value="Submeter" id="enviar" runat="server" onserverclick="btn_submeteNewsletter_OnClick" class="send-news mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent"/>
                                    <button id="cancel" onclick="window.history.back();" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect mdl-button--accent bg-gray">Cancelar</button>
                                </div>
                                <div class="alert fade in">
                                      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    <asp:Label runat="server" ID="lbl_Info" ClientIDMode="Static"></asp:Label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
        <!-- /card -->
    </div>
    <!-- /mdl-card -->
    </div>

</section>
<script>
    $('#aspnetForm').validate({
        debug: false,
        rules: {
            nameNews: {
                minlength: 3,
            }
        },
        submitHandler: function (form) {
            form.submit();
        }
    });
    
    $(document).ready(function () {
        $('footer .newsletter .container').hide();
        validateFields();
        $('#name, #email').change(validateFields);
    });
    function validateFields() {
        if ($('#name').val().length > 0 &&
            $('#email').val().length > 0) {
            $("#success button:not(#cancel)").prop("disabled", false);
        }
        else {
            $("#success button:not(#cancel)").prop("disabled", true);
        }
    }
    if ($('.alert span').hasClass('success')) {
        $('.alert').addClass('alert-success');
    } else {
        $('.alert').addClass('alert-warning');
    }
    if (!$.trim($('.alert span').html()).length) {
        $('.alert').remove();
    }
</script>