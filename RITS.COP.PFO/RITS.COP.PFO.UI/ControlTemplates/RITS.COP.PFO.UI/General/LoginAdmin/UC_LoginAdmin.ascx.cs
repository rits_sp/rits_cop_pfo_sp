﻿using System;
using System.Data;
using System.IdentityModel.Tokens;
using System.Text;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.SharePoint;
using Microsoft.SharePoint.Administration;
using Microsoft.SharePoint.Client;
using Microsoft.SharePoint.Utilities;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.LoginAdmin
{
    public partial class UC_LoginAdmin : UserControl
    {

        //private string _url = IFL.Core.Constants.Urls._program;
        //private string _list = IFL.Core.Constants.Lists._cop_Text;
        private string username;
        private string password;

        protected string LBL_LOGIN;
        protected string LBL_USERNAME;
        protected string LBL_PASSWORD;
        protected string LBL_LOGINADMIN;

        private DataTable dt_menus = null;
        public string classPopUp { get; set; }
        public string LblActivitie { get; set; }
        public string Url { get; set; }


        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                HttpRequest request = HttpContext.Current.Request;
                Url = IFL.Core.Constants.Urls._formacao;

                ////Labels da pagina
                LBL_LOGIN = ResourcesHelper.GetStringFromResource("PFO", "Login", "PT");
                LBL_LOGINADMIN = ResourcesHelper.GetStringFromResource("PFO", "LoginAdmin", "PT");
                LBL_USERNAME = ResourcesHelper.GetStringFromResource("PFO", "Username", "PT");
                LBL_PASSWORD = ResourcesHelper.GetStringFromResource("PFO", "Password", "PT");

                // HyperLink - inicio
                //var btn_hyp_start = IFL.Core.LoadObjects.GetParameter("hyp_start", dt_menus);
                //hyp_start.Text = btn_hyp_start.Value ?? string.Empty;
                //hyp_start.NavigateUrl = btn_hyp_start.Link ?? @"/";
                //hyp_start.Visible = Convert.ToBoolean(btn_hyp_start.Visible);
                //hyp_start.Enabled = Convert.ToBoolean(btn_hyp_start.Enable);
            }
            catch (Exception ex)
            {

                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP001", "UI.UC_login.Page_Load", ex.Message);
                HttpContext.Current.Response.Redirect(Url + IFL.Core.Constants.UrlsPages._pageError, false);

            }
        }

        protected void btn_Login_OnClick(object sender, EventArgs e)
        {
            try
            {
                bool isWindowsAuth = true;
                var username = Request.Form["usernameLogin"];
                var password = Request.Form["passwordLogin"];

                SPIisSettings iisSettings = SPContext.Current.Site.WebApplication.IisSettings[SPUrlZone.Default];

                if (isWindowsAuth)
                {
                    // Windows Authentication it is

                    if (null != iisSettings && iisSettings.UseWindowsClaimsAuthenticationProvider)
                    {
                        SPAuthenticationProvider provider = iisSettings.WindowsClaimsAuthenticationProvider;
                        RedirectToLoginPage(provider);
                    }
                }
            }

            catch (Exception ex)
            {
                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "COP002", "UI.UC_login.btn_Login_OnClick",
                    ex.Message);
                HttpContext.Current.Response.Redirect(Url + IFL.Core.Constants.UrlsPages._pageError, false);
            }
        }

        private void RedirectToSuccessUrl()
        {
            SPUtility.Redirect(IFL.Core.Constants.UrlsPages._pageHomePage, SPRedirectFlags.DoNotEncodeUrl, this.Context);
            HttpContext.Current.Response.End();
        }

        private void RedirectToLoginPage(SPAuthenticationProvider provider)
        {
            SPUtility.Redirect(IFL.Core.Constants.UrlsPages._pageLoginAdmin, SPRedirectFlags.DoNotEncodeUrl, this.Context);
            HttpContext.Current.Response.End();
        }

        private void EstablishSessionWithToken(SecurityToken securityToken)
        {
        }
    }
}
