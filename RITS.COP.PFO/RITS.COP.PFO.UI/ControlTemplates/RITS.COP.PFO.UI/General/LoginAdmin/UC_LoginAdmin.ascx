﻿<%@ Assembly Name="$SharePoint.Project.AssemblyFullName$" %>
<%@ Assembly Name="Microsoft.Web.CommandUI, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="SharePoint" Namespace="Microsoft.SharePoint.WebControls" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="Utilities" Namespace="Microsoft.SharePoint.Utilities" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Register Tagprefix="asp" Namespace="System.Web.UI" Assembly="System.Web.Extensions, Version=3.5.0.0, Culture=neutral, PublicKeyToken=31bf3856ad364e35" %>
<%@ Import Namespace="Microsoft.SharePoint" %> 
<%@ Register Tagprefix="WebPartPages" Namespace="Microsoft.SharePoint.WebPartPages" Assembly="Microsoft.SharePoint, Version=15.0.0.0, Culture=neutral, PublicKeyToken=71e9bce111e9429c" %>
<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UC_LoginAdmin.ascx.cs" Inherits="RITS.COP.PFO.UI.ControlTemplates.RITS.COP.PFO.UI.General.LoginAdmin.UC_LoginAdmin" %>

<section>
    <!-- Page Content -->
    <div class="container">
        <!-- Page Heading/Breadcrumbs -->
        <div class="row">
            <div class="col-lg-12">
                <ol class="breadcrumb">
                    <li>
                        <asp:HyperLink runat="server" ID="hyp_start"></asp:HyperLink></li>
                    <li class="active"> <%=LBL_LOGINADMIN%></li>
                </ol>
            </div>
        </div>
    </div>
</section>
<!-- /.container -->
<section>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="row">
                    <h3 class="section-title wow fadeInDown" data-wow-delay="0.4s"><i class="fa fa-lock"></i><%=LBL_LOGINADMIN%>  </h3>
                </div>
                <div class="row">
                    <div class="mdl-card mdl-shadow--2dp bg-gray-light login-container wow fadeInLeft">
                        <div class="card">
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label wow fadeInDown" data-wow-delay="0.5s">
                                <input class="mdl-textfield__input" type="text" id="username" name="usernameLogin">
                                <label class="mdl-textfield__label" for="username"><%=LBL_USERNAME%></label>
                            </div>
                            <div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label wow fadeInDown" data-wow-delay="0.6s">
                                <input class="mdl-textfield__input" type="password" id="password" name="passwordLogin">
                                <label class="mdl-textfield__label" for="password"><%=LBL_PASSWORD%></label>
                            </div>
                            <div class="box-buttons form__wrapper--submit wow fadeInLeft" data-wow-delay="0.7s">
                                <div class="mdl-card__actions mdl-card--border">
                                    <button id="bt_login" value="Login" type="submit" runat="server" clientidmode="Static" onserverclick="btn_Login_OnClick" class="mdl-button mdl-js-button">Login</button>
                                </div>
                            </div>
                            <asp:Label runat="server" ID="lbl_msg"></asp:Label>
                        </div>
                        <!-- /modal -->
                    </div>
                    <!-- /card -->
                </div>
                <!-- /mdl-card -->
            </div>
        </div>
    </div>
</section>
<script>
    $('#aspnetForm').validate({
        debug: false,
        rules: {
            passwordLogin: 'type_passw'
        },
        submitHandler: function (form) {
            form.submit();
        }
    });

    function validateFields() {
        if ($('#email').val().length > 0) {
            $(".form__wrapper--submit button#bt_login").prop("disabled", false);
        } else {
            $(".form__wrapper--submit button#bt_login").prop("disabled", true);
        }
    }

</script>
