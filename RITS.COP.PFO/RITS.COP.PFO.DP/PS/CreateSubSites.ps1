﻿#Caso corras com o powershell
Add-PsSnapin Microsoft.SharePoint.PowerShell

$url = "http://sp.ritslab.net"

$web = Get-SPWeb http://sp.ritslab.net
write-host "Web Template:" $web.WebTemplate " | Web Template ID:" $web.WebTemplateId
$web.Dispose()

$site = new-object microsoft.sharepoint.spsite($url);

	if ($site -ne $null)
	{
		$WebAppUrl = $url

		#->SubSite1<-
		$SubSiteTitle1 = "Atividades"
		$SubSite1Url = "Atividades"
	
		New-SPWeb –url $WebAppUrl"/"$SubSite1Url -name $SubSiteTitle1 -Template "STS#0" –UseParentTopNav
	
		Write-Host 'Create new SubSite:' $SubSiteTitle1 '-> Url:'$WebAppUrl"/"$SubSite1Url -foregroundcolor Green

		#->SubSite2<-
		$SubSiteTitle2 = "Valores"
		$SubSite2Url = "Valores"
	
		New-SPWeb –url $WebAppUrl"/"$SubSite2Url -name $SubSiteTitle2 -Template "STS#0" –UseParentTopNav
	
		Write-Host 'Create new SubSite:' $SubSiteTitle2 '-> Url:'$WebAppUrl"/"$SubSite2Url -foregroundcolor Green

		#->SubSite3<-
		$SubSiteTitle3 = "Conteudos"
		$SubSite3Url = "Conteudos"
	
		New-SPWeb –url $WebAppUrl"/"$SubSite3Url -name $SubSiteTitle3 -Template "STS#0" –UseParentTopNav
	
		Write-Host 'Create new SubSite:' $SubSiteTitle3 '-> Url:'$WebAppUrl"/"$SubSite3Url -foregroundcolor Green

		#->SubSite4<-
		$SubSiteTitle4 = "Desafios"
		$SubSite4Url = "Desafios"
	
		New-SPWeb –url $WebAppUrl"/"$SubSite4Url -name $SubSiteTitle4 -Template "STS#0" –UseParentTopNav
	
		Write-Host 'Create new SubSite:' $SubSiteTitle4 '-> Url:'$WebAppUrl"/"$SubSite4Url -foregroundcolor Green
	}
	else
	{
		 Write-Host "Web is Null!" -foregroundcolor Red
	}

if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null ) {
    Add-PSSnapin Microsoft.SharePoint.Powershell
}

