﻿

Add-PsSnapin Microsoft.SharePoint.PowerShell

#-------------------------VARIABLES START-----------------
#default Variables - Always needed
$CurrentDir= split-path -parent $MyInvocation.MyCommand.Definition
#WebApplications
$WebAppUrl_1="http://sp2013dev:46283/"

#Solucoes
$solutionName_1="EDIA.SharePoint.Layouts.wsp"
$solutionName_2="EDIA.SharePoint.Controls.wsp"
$solutionName_3="EDIA.Sharepoint.Dependencies.wsp"
#Directorias
$SolutionPath_1=$CurrentDir + "\"+$solutionName_1
$SolutionPath_2=$CurrentDir + "\"+$solutionName_2
$SolutionPath_3=$CurrentDir + "\"+$solutionName_3


#---------------------Deploy Script START-----------------

#UNINSTALL
Write-Host 'Going to uninstall solution'
#Comment one of the next 2 script lines
 #1 - For global WSP
Uninstall-SPSolution -identity $solutionName_1 -confirm:$false
#2 - For WebApp solutions, I.E. Webparts
Uninstall-SPSolution -identity $solutionName_2 -confirm:$false -WebApplication $WebAppUrl_1
Uninstall-SPSolution -identity $solutionName_3 -confirm:$false -WebApplication $WebAppUrl_1

#REMOVE
Write-Host 'Going to remove solution'
Remove-SPSolution –identity $solutionName_1 -confirm:$false
Remove-SPSolution –identity $solutionName_2 -confirm:$false
Remove-SPSolution –identity $solutionName_3 -confirm:$false

#ADD SOLUTIONS
Write-Host 'Going to add solution'
Add-SPSolution $SolutionPath_1
Add-SPSolution $SolutionPath_2
Add-SPSolution $SolutionPath_3

#INSTALL SOLUTIONS
Write-Host 'Going to install solution to all web applications'
#Comment one of the next 2 script lines
 #1 - For global WSP
Install-SPSolution –identity $solutionName_1 –GACDeployment -force

#For WebApp solutions, I.E. Webparts
Install-SPSolution –identity $solutionName_2 -WebApplication $WebAppUrl_1 –GACDeployment -force
#Install-SPSolution –identity $solutionName_2 -WebApplication $WebAppUrl_2 –GACDeployment -force
#Install-SPSolution –identity $solutionName_2 -WebApplication $WebAppUrl_3 –GACDeployment -force

Install-SPSolution –identity $solutionName_3 -WebApplication $WebAppUrl_1 –GACDeployment -force
#Install-SPSolution –identity $solutionName_3 -WebApplication $WebAppUrl_2 –GACDeployment -force
#Install-SPSolution –identity $solutionName_3 -WebApplication $WebAppUrl_3 –GACDeployment -force

Write-Host 'Waiting for job to finish'
WaitForJobToFinish

Remove-PsSnapin Microsoft.SharePoint.PowerShell

function WaitForJobToFinish([string]$SolutionFileName)
{ 
    $JobName = "*solution-deployment*$SolutionFileName*"
    $job = Get-SPTimerJob | ?{ $_.Name -like $JobName }
    if ($job -eq $null) 
    {
        Write-Host 'Timer job not found'
    }
    else
    {
        $JobFullName = $job.Name
        Write-Host -NoNewLine "Waiting to finish job $JobFullName"
        
        while ((Get-SPTimerJob $JobFullName) -ne $null) 
        {
            Write-Host -NoNewLine .
            Start-Sleep -Seconds 2
        }
        Write-Host "Finished waiting for job.."
    }
}
