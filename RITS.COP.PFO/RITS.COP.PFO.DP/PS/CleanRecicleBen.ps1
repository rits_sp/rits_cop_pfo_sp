﻿Add-PsSnapin Microsoft.SharePoint.PowerShell

$url = "http://sp.ritslab.net/"

$site = new-object microsoft.sharepoint.spsite($url);

for ($i=0;$i -lt $site.allwebs.count;$i++)
	{
	  write-host $site.allwebs[$i].url "...deleting" $site.allwebs[$i].recyclebin.count "item(s).";
	  $site.allwebs[$i].recyclebin.deleteall();
	}

write-host $site.url "...deleting" $site.recyclebin.count "item(s).";

$site.recyclebin.deleteall();
$site.dispose();

#Caso corras com o powershell
Start-Sleep -s 2

if ( (Get-PSSnapin -Name Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue) -eq $null ) {
    Add-PSSnapin Microsoft.SharePoint.Powershell
}