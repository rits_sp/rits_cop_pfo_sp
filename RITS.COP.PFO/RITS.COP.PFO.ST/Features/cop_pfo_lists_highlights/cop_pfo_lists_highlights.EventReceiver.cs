using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.ST.Features.cop_pfo_lists_highlights
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("b25cd28f-c6f9-48a4-9ca5-ac4c72553faf")]
    public class cop_pfo_lists_highlightsEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            #region Variables

            var webHighLights = IFL.Core.Constants.Areas.Highlights;

            #endregion

            using (var web = properties.Feature.Parent as SPWeb)
            {

                if (web != null && web.IsRootWeb)
                {
                    SPWebCollection webCollection = web.Webs;

                    foreach (SPWeb w in webCollection)
                    {
                        if (w.Title == webHighLights)
                        {
                            try
                            {
                                #region DESTAQUES PRINCIPAL LIST

                                if (!SpLists.Exists(w, "COP_PFO_DESTAQUES_PRINCIPAL"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_DESTAQUES_PRINCIPAL",
                                        "Lista de Destaques - Principal",
                                        SPListTemplateType.GenericList, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_highlights_main");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_description");
                                    view.ViewFields.Add("cop_pfo_tags_highlights");
                                    view.ViewFields.Add("cop_pfo_tags_Contents");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_isNews");
                                    view.ViewFields.Add("cop_pfo_enable");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region FORMA��ES IMAGENS LIST

                                if (!SpLists.Exists(w, "COP_PFO_DESTAQUES_IMAGENS"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_DESTAQUES_IMAGENS", "Lista de Forma��es - Imagens",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_highlights_images");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_imageFeatured");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_enable");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region FORMA��ES ANEXOS LIST

                                if (!SpLists.Exists(w, "COP_PFO_DESTAQUES_ANEXOS"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_DESTAQUES_ANEXOS", "Lista de Forma��es - Anexos",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_highlights_attachments");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region FORMA��ES LINKS LIST

                                if (!SpLists.Exists(w, "COP_PFO_DESTAQUES_LINKS"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_DESTAQUES_LINKS", "Lista de Forma��es - Links",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_highlights_externalLinks");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_link");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region FORMA��ES VIDEO LIST

                                if (!SpLists.Exists(w, "COP_PFO_DESTAQUES_VIDEO"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_DESTAQUES_VIDEO", "Lista de Forma��es - Videos",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_highlights_video");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_link");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_enable");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                            }
                            catch (Exception ex)
                            {
                                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "Erro ao criar fields",
                                    ex.Message);
                            }
                        }
                    }
                }
            }
        }

        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
