﻿<?xml version="1.0" encoding="utf-8"?>
<feature xmlns:dm0="http://schemas.microsoft.com/VisualStudio/2008/DslTools/Core" dslVersion="1.0.0.0" Id="e3176262-628c-4a45-a8d9-590642ce054c" description="COP PFO ContentTypes Main" featureId="e3176262-628c-4a45-a8d9-590642ce054c" imageUrl="" solutionId="00000000-0000-0000-0000-000000000000" title="COP PFO ContentTypes Main" version="" deploymentPath="$SharePoint.Project.FileNameWithoutExtension$_$SharePoint.Feature.FileNameWithoutExtension$" xmlns="http://schemas.microsoft.com/VisualStudio/2008/SharePointTools/FeatureModel">
  <projectItems>
    <projectItemReference itemId="a62d4929-3505-4985-a78d-2ac366868c51" />
    <projectItemReference itemId="a3c3b9d7-f17e-47ba-b92e-314d880b9939" />
    <projectItemReference itemId="64452767-f198-41a1-8335-c1f8673ff192" />
    <projectItemReference itemId="e87318d2-1c89-4c91-802c-fcca53c5a8fe" />
    <projectItemReference itemId="98f40aa5-53b2-4f7d-9ec6-a8f0e9260044" />
    <projectItemReference itemId="b7381e46-9beb-45d8-bc1e-eac06694a72a" />
    <projectItemReference itemId="6778b38b-8e92-4134-ba7e-e7790876230e" />
    <projectItemReference itemId="fb2cc584-2786-474f-bc20-551f12580a64" />
    <projectItemReference itemId="70e3d923-974d-423f-b70f-ec22510f8ced" />
    <projectItemReference itemId="1e761085-eefe-4cf2-9b72-4b131ddf890d" />
    <projectItemReference itemId="a8692546-deae-418e-9ae9-6d12318ecde9" />
    <projectItemReference itemId="3a1678ce-0970-4983-983f-424df4ad36c4" />
    <projectItemReference itemId="70050d2a-acda-43ed-bc33-f091e3341a64" />
    <projectItemReference itemId="11e51ea7-0a08-4cee-bca6-85cbf8ce4861" />
    <projectItemReference itemId="301256e7-f667-4a10-af41-5ed79ba76f4d" />
    <projectItemReference itemId="e96e303b-2920-4901-b817-5ca483ce764d" />
    <projectItemReference itemId="c710f165-f960-47f7-a713-027db7d8c362" />
    <projectItemReference itemId="16047fbc-c0ae-4181-ab5b-0a20da064d63" />
    <projectItemReference itemId="1723163a-8326-47e9-9d27-2058d4bb9fba" />
    <projectItemReference itemId="15f0a555-76b1-493c-8e4d-5f82698668c9" />
    <projectItemReference itemId="e7eea64d-c542-48ff-aa33-d14a0e063fd5" />
    <projectItemReference itemId="e60fd569-2d9b-4129-97d4-180a4ad40056" />
    <projectItemReference itemId="b51fcc75-bf05-4809-a8ac-c3e241e370e7" />
    <projectItemReference itemId="bcc8e693-a712-4a31-832a-960ccffb5374" />
    <projectItemReference itemId="261ac16a-aa08-45a0-8f1d-b030790beb05" />
    <projectItemReference itemId="b1220e15-09c6-4f29-b8ce-01926bfc2e10" />
    <projectItemReference itemId="40b5242a-3486-46d5-8cea-66451d2a2e71" />
    <projectItemReference itemId="04f94da5-c38a-49e3-9fe6-1c3ac6548305" />
    <projectItemReference itemId="39a89dcc-467a-4e04-9ee8-db92976e834c" />
    <projectItemReference itemId="6acc8531-fab3-4948-adfb-3ca9c2c8d42c" />
    <projectItemReference itemId="b877ae2d-2dce-4ebd-9db4-caa9fc9e1f5b" />
  </projectItems>
</feature>