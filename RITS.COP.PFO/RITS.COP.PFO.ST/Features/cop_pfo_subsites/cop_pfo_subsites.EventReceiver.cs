using System;
using System.Collections;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;

namespace RITS.COP.PFO.ST.Features.cop_pfo_subsites
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("2c534707-ac57-4fc5-9b34-a7446d35b5e1")]
    public class cop_pfo_subsitesEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            try
            {
                using (var web = properties.Feature.Parent as SPWeb)
                {
                    if (web != null && web.IsRootWeb)
                    {
                        var alist = new ArrayList();
                        alist.Add(IFL.Core.Constants.Areas.Formations);
                        alist.Add(IFL.Core.Constants.Areas.Highlights);
                        alist.Add(IFL.Core.Constants.Areas.Publications);
                        alist.Add(IFL.Core.Constants.Areas.COPAwards);

                        foreach (string newWeb in alist)
                        {
                            using (SPSite spSite = new SPSite(web.Url))
                            {
                                if (!isWebExists(spSite, newWeb))
                                {
                                    web.Webs.Add(newWeb, newWeb, newWeb, 1033, "BLANKINTERNET#0", false, false);
                                }
                            }
                        }
                    }
                }
            }
            catch(Exception ex)
            {
                
            }
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}

        private static bool isWebExists(SPSite spSite, string webRelativeUrl)
        {
            var returnVal = false;
            using (SPWeb currentWeb = spSite.OpenWeb(webRelativeUrl))
            {
                try
                {
                  
                    var siteTitle = currentWeb.Title;
                    returnVal = true;
                }
                catch
                {
                    //Do nothing
                }
            }
            return returnVal;
        }
    }
}
