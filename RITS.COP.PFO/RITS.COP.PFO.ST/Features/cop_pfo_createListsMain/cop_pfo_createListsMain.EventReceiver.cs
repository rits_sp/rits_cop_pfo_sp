using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.ST.Features.cop_pfo_createListsMain
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("110836d7-06c9-474c-bd62-a449879d160b")]
    public class cop_pfo_createListsMainEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            #region Variables

            var webroot = IFL.Core.Constants.Areas.FormacaoOlimpica;

            #endregion

            using (var web = properties.Feature.Parent as SPWeb)
            {
                if (web == null) return;


                if (web.Title == webroot)
                {
                    try
                    {
                        #region Configurations List
                        if (!SpLists.Exists(web, "COP_PFO_Configuracoes"))
                        {
                            SPList list;

                            SpLists.CreateList(web, "COP_PFO_Configuracoes", "Lista de Configurações", SPListTemplateType.GenericList, out list);

                            SpLists.AddContentTypeToList(web, list.Title, "cop_pfo_configuration");

                            var view = list.DefaultView;

                            view.ViewFields.Add("cop_pfo_key");
                            view.ViewFields.Add("cop_pfo_value");

                            view.Update();

                            list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                            list.Update();

                        }
                        #endregion

                        #region FAQ List
                        if (!SpLists.Exists(web, "COP_PFO_FAQS"))
                        {
                            SPList list;

                            SpLists.CreateList(web, "COP_PFO_FAQS", "Lista de FAQS", SPListTemplateType.GenericList, out list);

                            SpLists.AddContentTypeToList(web, list.Title, "cop_pfo_FAQS");

                            var view = list.DefaultView;

                            view.ViewFields.Add("cop_pfo_id");
                            view.ViewFields.Add("cop_pfo_question");
                            view.ViewFields.Add("cop_pfo_answer");


                            view.Update();


                            list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                            list.Update();


                        }

                        #endregion

                        #region Navigation
                        if (!SpLists.Exists(web, "COP_PFO_Navegacao"))
                        {
                            SPList list;

                            SpLists.CreateList(web, "COP_PFO_Navegacao", "Lista de Navegação", SPListTemplateType.GenericList, out list);

                            SpLists.AddContentTypeToList(web, list.Title, "cop_pfo_navigation");

                            var view = list.DefaultView;

                            view.ViewFields.Add("cop_pfo_key");
                            view.ViewFields.Add("cop_pfo_value");
                            view.ViewFields.Add("cop_pfo_menu");
                            view.ViewFields.Add("cop_pfo_link");
                            view.ViewFields.Add("cop_pfo_visible");
                            view.ViewFields.Add("cop_pfo_enable");

                            view.Update();

                            list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                            list.Update();

                        }
                        #endregion

                        #region Textos
                        if (!SpLists.Exists(web, "COP_PFO_Texto"))
                        {
                            SPList list;

                            SpLists.CreateList(web, "COP_PFO_Texto", "Lista de Textos das Páginas", SPListTemplateType.GenericList, out list);

                            SpLists.AddContentTypeToList(web, list.Title, "cop_pfo_text");

                            var view = list.DefaultView;

                            view.ViewFields.Add("cop_pfo_key");
                            view.ViewFields.Add("cop_pfo_richText");

                            view.Update();

                            list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                            list.Update();

                        }
                        #endregion

                        #region NewsLetter
                        if (!SpLists.Exists(web, "COP_PFO_NewsLetter"))
                        {
                            SPList list;

                            SpLists.CreateList(web, "COP_PFO_NewsLetter", "Lista de utilizadores da NewsLetter", SPListTemplateType.GenericList, out list);

                            SpLists.AddContentTypeToList(web, list.Title, "cop_pfo_newsletter");

                            var view = list.DefaultView;

                            view.ViewFields.Add("cop_pfo_name");
                            view.ViewFields.Add("cop_pfo_email");
                            view.ViewFields.Add("cop_pfo_enable");
                            view.ViewFields.Add("cop_pfo_subscriptionDate");
                            view.ViewFields.Add("cop_pfo_deactivationDate");

                            view.Update();

                            list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                            list.Update();

                        }
                        #endregion

                        #region Contacts List
                        if (!SpLists.Exists(web, "COP_PFO_Contactos"))
                        {
                            SPList list;

                            SpLists.CreateList(web, "COP_PFO_Contactos", "Lista de Pedidos Contactos", SPListTemplateType.GenericList, out list);

                            SpLists.AddContentTypeToList(web, list.Title, "cop_pfo_contacts");

                            var view = list.DefaultView;

                            view.ViewFields.Add("cop_pfo_key");
                            view.ViewFields.Add("cop_pfo_name");
                            view.ViewFields.Add("cop_pfo_email");
                            view.ViewFields.Add("cop_pfo_phone");
                            view.ViewFields.Add("cop_pfo_entity");
                            view.ViewFields.Add("cop_pfo_subject");
                            view.ViewFields.Add("cop_pfo_message");
                            view.ViewFields.Add("cop_pfo_date");
                            view.ViewFields.Add("cop_pfo_read");

                            view.Update();

                            list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                            list.Update();
                        }

                        #endregion

                        #region Partners List
                        if (!SpLists.Exists(web, "COP_PFO_Parceiros"))
                        {
                            SPList list;

                            SpLists.CreateList(web, "COP_PFO_Parceiros", "Lista de Parceiros COP", SPListTemplateType.DocumentLibrary, out list);

                            SpLists.AddContentTypeToList(web, list.Title, "cop_pfo_partners");

                            var view = list.DefaultView;

                            view.ViewFields.Add("cop_pfo_name");
                            view.ViewFields.Add("cop_pfo_link");
                            view.ViewFields.Add("cop_pfo_partnerType");
                            view.ViewFields.Add("cop_pfo_visible");
         
                            view.Update();

                            list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                            list.Update();
                        }

                        #endregion
                    
                        #region Revisores List
                        if (!SpLists.Exists(web, "COP_PFO_Revisores"))
                        {
                            SPList list;

                            SpLists.CreateList(web, "COP_PFO_Revisores", "Lista de Revisores COP", SPListTemplateType.DocumentLibrary, out list);

                            SpLists.AddContentTypeToList(web, list.Title, "cop_pfo_revisores");

                            var view = list.DefaultView;

                            view.ViewFields.Add("cop_pfo_name");
                            view.ViewFields.Add("cop_pfo_institute");
                            view.ViewFields.Add("cop_pfo_areaType");
                            view.ViewFields.Add("cop_pfo_enable");
         
                            view.Update();

                            list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                            list.Update();
                        }

                        #endregion

                        #region Imagens List
                        if (!SpLists.Exists(web, "COP_PFO_Images"))
                        {
                            SPList list;

                            SpLists.CreateList(web, "COP_PFO_Images", "Lista de Imagens COP", SPListTemplateType.DocumentLibrary, out list);

                            SpLists.AddContentTypeToList(web, list.Title, "cop_pfo_images");

                            var view = list.DefaultView;

                            view.ViewFields.Add("cop_pfo_key");
                            view.ViewFields.Add("cop_pfo_description");

                            view.Update();

                            list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                            list.Update();
                        }

                        #endregion
                    
                    }
                    catch (Exception ex)
                    {
                        ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "Erro ao criar fields", ex.Message);
                    }
                }
            }
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
