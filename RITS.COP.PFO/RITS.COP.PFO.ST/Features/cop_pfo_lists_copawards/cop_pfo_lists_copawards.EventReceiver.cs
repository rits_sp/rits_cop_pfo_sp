using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.ST.Features.cop_pfo_lists_copawards
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("881bba6f-31c6-49ee-b818-8556321ca088")]
    public class cop_pfo_lists_copawardsEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            #region Variables

            var webroot = IFL.Core.Constants.Areas.FormacaoOlimpica;
            var webCOPAW = IFL.Core.Constants.Areas.COPAwards;

            #endregion

            using (var web = properties.Feature.Parent as SPWeb)
            {

             if (web != null && web.IsRootWeb)
                {
                    SPWebCollection webCollection = web.Webs;

                    foreach (SPWeb w in webCollection)
                    {
                        if (w.Title == webCOPAW)
                        {
                            try
                            {
                                #region TRABALHOS PREMIADOS LIST

                                if (!SpLists.Exists(w, "COP_PFO_TP"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_TP", "Lista de Trabalhos Premiados",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_tp");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_author");
                                    view.ViewFields.Add("cop_pfo_institute");
                                    view.ViewFields.Add("cop_pfo_tags_TP");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");
                            
                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region MEN��ES HONROSAS LIST

                                if (!SpLists.Exists(w, "COP_PFO_MH"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_MH", "Lista de Men��es Honrosas",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_mh");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_author");
                                    view.ViewFields.Add("cop_pfo_institute");
                                    view.ViewFields.Add("cop_pfo_tags_MH");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region MEN��ES HONROSAS LIST

                                if (!SpLists.Exists(w, "COP_PFO_TS"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_TS", "Lista de Trabalhos Submetidos",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_ts");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_author");
                                    view.ViewFields.Add("cop_pfo_institute");
                                    view.ViewFields.Add("cop_pfo_tags_TS");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion
                            }
                            catch (Exception ex)
                            {
                                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "Erro ao criar fields",
                                    ex.Message);
                            }
                        }
                    }
                }
            }

            
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
