using System;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.ST.Features.cop_pfo_navigationXML
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("c6ab0cff-ca0e-4597-af54-42064eb633dd")]
    public class cop_pfo_navigationXMLEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.
        public string Url { get; set; }

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            //List<object> listBottom = new List<object>();
            var dt = new DataTable();
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                try
                {
                    using (var web = properties.Feature.Parent as SPWeb)
                    {
                        if (web != null && web.IsRootWeb)
                        {
                            SPList _list = web.Lists.TryGetList("Style Library");
                            if (_list != null)
                            {
                                SPListItemCollection _listItemCollection = _list.Items;

                                if (_listItemCollection.Count > 0)
                                {
                                    foreach (
                                        SPListItem item in
                                            _listItemCollection.Cast<SPListItem>()
                                                .Where(
                                                    item =>
                                                        item["LinkFilename"].ToString()
                                                            .Contains("navigationList")))
                                    {
                                        Url = web.Url + "/" + item.Url;
                                    }
                                }
                            }

                            if (!string.IsNullOrEmpty(Url))
                            {
                                SPList list = web.Lists.TryGetList("COP_PFO_Navegacao");

                                dt = IFL.Core.ReadXmlNavigation.ReadXml(Url);

                                for (int i = 0; i <= dt.Rows.Count; i++)
                                {
                                    SPListItem item = list.Items.Add();

                                    item["Key"] = dt.Rows[i]["key"].ToString() ?? string.Empty;
                                    item["Value"] = dt.Rows[i]["value"].ToString() ?? string.Empty;
                                    item["Menu"] = dt.Rows[i]["menu"].ToString() ?? string.Empty;
                                    item["Link"] = dt.Rows[i]["link"].ToString() ?? string.Empty;
                                    item["Title"] = dt.Rows[i]["title"].ToString() ?? string.Empty;

                                    item.Update();
                                }

                                list.Update();
                            }
                        }
                    }
                }

                catch (Exception ex)
                {
                    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "RITS.COP.ST.Features.cop_xml.FeatureActivated", ex.Message);
                }

            });
        }

        // Uncomment the method below to handle the event raised before a feature is deactivated.

        public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        {
            SPSecurity.RunWithElevatedPrivileges(delegate()
            {
                try
                {
                    using (var web = properties.Feature.Parent as SPWeb)
                    {
                        if (web != null && web.IsRootWeb)
                        {
                            web.AllowUnsafeUpdates = true;

                            SPList list = web.Lists.TryGetList("COP_PFO_Navegacao");

                            if (list != null)
                            {
                                SPListItemCollection listcollection = list.Items;
                                int itemCount = listcollection.Count;
                                if (listcollection.Count > 0)
                                {
                                    for (int intIndex = listcollection.Count - 1; intIndex > -1; intIndex--)
                                    {
                                        listcollection.Delete(intIndex);
                                    }


                                    list.Update();
                                }
                            }
                            web.AllowUnsafeUpdates = false;

                        }
                    }

                }

                catch (Exception ex)
                {
                    ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "RITS.COP.ST.Features.cop_xml.FeatureDeactivating", ex.Message);
                }

            });
        }


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
