using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.ST.Features.cop_pfo_lists_publications
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("6512ccdf-3242-4ea3-b460-083a660d4ce1")]
    public class cop_pfo_lists_publicationsEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {

            #region Variables

            var webPub = IFL.Core.Constants.Areas.Publications;

            #endregion

            using (var web = properties.Feature.Parent as SPWeb)
            {
                if (web != null && web.IsRootWeb)
                {
                    SPWebCollection webCollection = web.Webs;

                    foreach (SPWeb w in webCollection)
                    {
                        if (w.Title == webPub)
                        {
                            try
                            {
                                #region REVISTA OLIMPO LIST

                                if (!SpLists.Exists(w, "COP_PFO_RO"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_RO", "Lista de Revistas Olimpicas",
                                        SPListTemplateType.GenericList, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_ro");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_tags_RO");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();

                                }

                                #endregion

                                #region VALORIZAR SOCIALMENTE O DESPORTO LIST

                                if (!SpLists.Exists(w, "COP_PFO_VSD"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_VSD", "Lista de Valorizar Socialmente o Desporto",
                                        SPListTemplateType.GenericList, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_vsd");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_tags_VSD");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();

                                }

                                #endregion

                                #region PUBLICACOES PERIODICAS LIST

                                if (!SpLists.Exists(w, "COP_PFO_PP"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_PP", "Lista de Publica��es Peri�dicas",
                                        SPListTemplateType.GenericList, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_pp");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_tags_PP");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();

                                }

                                #endregion


                                #region TREINO DESPORTIVO LIST

                                if (!SpLists.Exists(w, "COP_PFO_TD"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_TD", "Lista de Treino Desportivo",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_td");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_author");
                                    view.ViewFields.Add("cop_pfo_institute");
                                    view.ViewFields.Add("cop_pfo_tags_documentType");
                                    view.ViewFields.Add("cop_pfo_tags_TD");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();

                                }

                                #endregion

                                #region PSICOLOGIA E PEDAGOGIA DO DESPORTO LIST

                                if (!SpLists.Exists(w, "COP_PFO_PPD"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_PPD", "Lista de Psicologia e Pedagogia do Desporto",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_ppd");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_author");
                                    view.ViewFields.Add("cop_pfo_institute");
                                    view.ViewFields.Add("cop_pfo_tags_documentType");
                                    view.ViewFields.Add("cop_pfo_tags_PPD");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();

                                }

                                #endregion

                                #region MEDICINA DO DESPORTO LIST

                                if (!SpLists.Exists(w, "COP_PFO_MD"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_MD", "Lista de Medicina do Desporto",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_md");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_author");
                                    view.ViewFields.Add("cop_pfo_institute");
                                    view.ViewFields.Add("cop_pfo_tags_documentType");
                                    view.ViewFields.Add("cop_pfo_tags_MD");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();

                                }

                                #endregion

                                #region FISIOLOGIA E BIOMECANICA DO DESPORTO LIST

                                if (!SpLists.Exists(w, "COP_PFO_FBD"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_FBD", "Lista de Fisiologia e Biomec�nica do Desporto",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_fbd");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_author");
                                    view.ViewFields.Add("cop_pfo_institute");
                                    view.ViewFields.Add("cop_pfo_tags_documentType");
                                    view.ViewFields.Add("cop_pfo_tags_FBD");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();

                                }

                                #endregion

                                #region ECONOMIA,DIREITO E GESTAO DO DESPORTO LIST

                                if (!SpLists.Exists(w, "COP_PFO_EDGD"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_EDGD", "Lista de Economia, Direito e Gest�o do Desporto",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_edgd");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_author");
                                    view.ViewFields.Add("cop_pfo_institute");
                                    view.ViewFields.Add("cop_pfo_tags_documentType");
                                    view.ViewFields.Add("cop_pfo_tags_EDGD");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();

                                }

                                #endregion

                                #region SOCIOLOGIA E HISTORIA DO DESPORTO LIST

                                if (!SpLists.Exists(w, "COP_PFO_SHD"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_SHD", "Lista de Sociologia e Hist�ria do Desporto",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_shd");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_author");
                                    view.ViewFields.Add("cop_pfo_institute");
                                    view.ViewFields.Add("cop_pfo_tags_documentType");
                                    view.ViewFields.Add("cop_pfo_tags_SHD");
                                    view.ViewFields.Add("cop_pfo_shortLink");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();

                                }

                                #endregion

                            }
                            catch (Exception ex)
                            {
                                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "Erro ao criar fields",
                                    ex.Message);
                            }
                        }
                    }
                }
            }
        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
