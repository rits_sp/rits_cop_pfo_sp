using System;
using System.Runtime.InteropServices;
using System.Security.Permissions;
using Microsoft.SharePoint;
using RITS.COP.PFO.IFL.Core;
using RITS.COP.PFO.IFL.Logging;

namespace RITS.COP.PFO.ST.Features.cop_pfo_lists_formations
{
    /// <summary>
    /// This class handles events raised during feature activation, deactivation, installation, uninstallation, and upgrade.
    /// </summary>
    /// <remarks>
    /// The GUID attached to this class may be used during packaging and should not be modified.
    /// </remarks>

    [Guid("9a847f39-25ee-43bb-bfec-044fc6cc0939")]
    public class cop_pfo_lists_formationsEventReceiver : SPFeatureReceiver
    {
        // Uncomment the method below to handle the event raised after a feature has been activated.

        public override void FeatureActivated(SPFeatureReceiverProperties properties)
        {
            #region Variables

            var webFormations = IFL.Core.Constants.Areas.Formations;

            #endregion

            using (var web = properties.Feature.Parent as SPWeb)
            {

                if (web != null && web.IsRootWeb)
                {
                    SPWebCollection webCollection = web.Webs;

                    foreach (SPWeb w in webCollection)
                    {
                        if (w.Title == webFormations)
                        {
                            try
                            {
                                #region FORMA��ES PRINCIPAL LIST

                                if (!SpLists.Exists(w, "COP_PFO_FORMACOES_PRINCIPAL"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_FORMACOES_PRINCIPAL", "Lista de Forma��es - Principal",
                                        SPListTemplateType.GenericList, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_formations_main");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_description");
                                    view.ViewFields.Add("cop_pfo_tags_formations");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_isNews");
                                    view.ViewFields.Add("cop_pfo_enable");
                                    view.ViewFields.Add("cop_pfo_visible");
                        
                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region FORMA��ES IMAGENS LIST

                                if (!SpLists.Exists(w, "COP_PFO_FORMACOES_IMAGENS"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_FORMACOES_IMAGENS", "Lista de Forma��es - Imagens",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_formations_images");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_imageFeatured");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_enable");
                 
                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region FORMA��ES ANEXOS LIST

                                if (!SpLists.Exists(w, "COP_PFO_FORMACOES_ANEXOS"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_FORMACOES_ANEXOS", "Lista de Forma��es - Anexos",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_formations_attachments");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region FORMA��ES LINKS LIST

                                if (!SpLists.Exists(w, "COP_PFO_FORMACOES_LINKS"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_FORMACOES_LINKS", "Lista de Forma��es - Links",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_formations_externalLinks");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_link");
                                    view.ViewFields.Add("cop_pfo_visible");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                                #region FORMA��ES VIDEO LIST

                                if (!SpLists.Exists(w, "COP_PFO_FORMACOES_VIDEO"))
                                {
                                    SPList list;

                                    SpLists.CreateList(w, "COP_PFO_FORMACOES_VIDEO", "Lista de Forma��es - Videos",
                                        SPListTemplateType.DocumentLibrary, out list);

                                    SpLists.AddContentTypeToList(w, list.Title, "cop_pfo_formations_video");

                                    var view = list.DefaultView;

                                    view.ViewFields.Add("cop_pfo_key");
                                    view.ViewFields.Add("cop_pfo_title");
                                    view.ViewFields.Add("cop_pfo_link");
                                     view.ViewFields.Add("cop_pfo_date");
                                    view.ViewFields.Add("cop_pfo_enable");

                                    view.Update();

                                    list.ImageUrl = web.ServerRelativeUrl + "Style Library/cop/imgs/I_peo-iconList.png";
                                    list.Update();
                                }

                                #endregion

                            }
                            catch (Exception ex)
                            {
                                ULSLogging.WriteLog(ULSLogging.Category.Unexpected, "001", "Erro ao criar fields",
                                    ex.Message);
                            }
                        }
                    }
                }
            }



        }


        // Uncomment the method below to handle the event raised before a feature is deactivated.

        //public override void FeatureDeactivating(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised after a feature has been installed.

        //public override void FeatureInstalled(SPFeatureReceiverProperties properties)
        //{
        //}


        // Uncomment the method below to handle the event raised before a feature is uninstalled.

        //public override void FeatureUninstalling(SPFeatureReceiverProperties properties)
        //{
        //}

        // Uncomment the method below to handle the event raised when a feature is upgrading.

        //public override void FeatureUpgrading(SPFeatureReceiverProperties properties, string upgradeActionName, System.Collections.Generic.IDictionary<string, string> parameters)
        //{
        //}
    }
}
